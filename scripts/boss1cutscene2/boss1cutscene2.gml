/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 01302D90
/// @DnDArgument : "obj" "obj_textbox"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
var l01302D90_0 = false;
l01302D90_0 = instance_exists(obj_textbox);
if(!l01302D90_0)
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 4710B77B
	/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
	/// @DnDParent : 01302D90
	with(obj_camera) {
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 4ED4328F
		/// @DnDParent : 4710B77B
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "175"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "objectid" "obj_textbox"
		/// @DnDArgument : "layer" ""text""
		/// @DnDSaveInfo : "objectid" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
		instance_create_layer(x + 0, y + 175, "text", obj_textbox);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2CABE1E9
		/// @DnDParent : 4710B77B
		/// @DnDArgument : "expr" "obj_clonebird"
		/// @DnDArgument : "var" "camerafollow"
		camerafollow = obj_clonebird;
	}
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 39D84804
/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
with(obj_textbox) {
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 30D67DB8
	/// @DnDParent : 39D84804
	/// @DnDArgument : "expr" "2"
	/// @DnDArgument : "var" "current_text"
	current_text = 2;

	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 11D68B36
	/// @DnDParent : 39D84804
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 03890E6E
	/// @DnDParent : 39D84804
	/// @DnDArgument : "expr" ""Clone Bird:""
	/// @DnDArgument : "var" "speaker"
	speaker = "Clone Bird:";

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 308A0432
	/// @DnDParent : 39D84804
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "1"
	if(textphase == 1)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3B7CA24E
		/// @DnDParent : 308A0432
		/// @DnDArgument : "expr" ""\nWhat? You thought you killed me?""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nWhat? You thought you killed me?";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 10516F36
	/// @DnDParent : 39D84804
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "2"
	if(textphase == 2)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 679FFB5F
		/// @DnDParent : 10516F36
		/// @DnDArgument : "expr" ""\nHAHAHAHA""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nHAHAHAHA";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6CCFBAFF
	/// @DnDParent : 39D84804
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "3"
	if(textphase == 3)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6762390F
		/// @DnDParent : 6CCFBAFF
		/// @DnDArgument : "expr" ""\nI AM ONE OF THOUSANDS OF CLONES!!!""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nI AM ONE OF THOUSANDS OF CLONES!!!";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 649089EB
	/// @DnDParent : 39D84804
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "4"
	if(textphase == 4)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 7F8F0975
		/// @DnDParent : 649089EB
		/// @DnDArgument : "expr" ""\nI AM IMMORTAL!!! HAHAHA!!!""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nI AM IMMORTAL!!! HAHAHA!!!";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 0AC1D91A
	/// @DnDParent : 39D84804
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "5"
	if(textphase == 5)
	{
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 64CFC408
		/// @DnDParent : 0AC1D91A
		instance_destroy();
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 3D5876A6
		/// @DnDApplyTo : b8f2740a-f816-4e35-bfa4-5741586c667f
		/// @DnDParent : 0AC1D91A
		with(obj_cutscene1) {
			/// @DnDAction : YoYo Games.Instances.Destroy_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 36871FDA
			/// @DnDParent : 3D5876A6
			instance_destroy();
		}
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 52E55FE2
		/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
		/// @DnDParent : 0AC1D91A
		with(obj_camera) {
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 6D250FFB
			/// @DnDParent : 52E55FE2
			/// @DnDArgument : "expr" "obj_player"
			/// @DnDArgument : "var" "camerafollow"
			camerafollow = obj_player;
		}
	}
}