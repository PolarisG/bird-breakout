/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0CFD805C
/// @DnDArgument : "var" "on_ground"
/// @DnDArgument : "value" "true"
if(on_ground == true)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2A1D4C65
	/// @DnDParent : 0CFD805C
	/// @DnDArgument : "var" "hsp"
	/// @DnDArgument : "not" "1"
	if(!(hsp == 0))
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 7F334986
		/// @DnDParent : 2A1D4C65
		/// @DnDArgument : "expr" "spr_walk"
		/// @DnDArgument : "var" "sprite_index"
		sprite_index = spr_walk;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 4A409331
	/// @DnDParent : 0CFD805C
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1C428627
		/// @DnDParent : 4A409331
		/// @DnDArgument : "expr" "spr_idle"
		/// @DnDArgument : "var" "sprite_index"
		sprite_index = spr_idle;
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 53BB2A68
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 631E598C
	/// @DnDParent : 53BB2A68
	/// @DnDArgument : "expr" "spr_air"
	/// @DnDArgument : "var" "sprite_index"
	sprite_index = spr_air;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 67E788D0
/// @DnDArgument : "var" "hsp"
/// @DnDArgument : "not" "1"
if(!(hsp == 0))
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 046ACF78
	/// @DnDParent : 67E788D0
	/// @DnDArgument : "var" "hsp"
	/// @DnDArgument : "op" "2"
	if(hsp > 0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0FEBD58D
		/// @DnDParent : 046ACF78
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "facing"
		facing = 1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3900B84B
		/// @DnDApplyTo : 8d33e9c5-4529-45cb-91b9-0b206f882847
		/// @DnDParent : 046ACF78
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "facing"
		with(obj_laserleft) {
		facing = 1;
		
		}
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 59DD16DF
	/// @DnDParent : 67E788D0
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 219A7039
		/// @DnDParent : 59DD16DF
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "facing"
		facing = -1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2B6502F1
		/// @DnDApplyTo : 8d33e9c5-4529-45cb-91b9-0b206f882847
		/// @DnDParent : 59DD16DF
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "facing"
		with(obj_laserleft) {
		facing = -1;
		
		}
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 668339E8
/// @DnDArgument : "var" "crouching"
/// @DnDArgument : "value" "true"
if(crouching == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 58FCD77A
	/// @DnDParent : 668339E8
	/// @DnDArgument : "expr" "spr_crouch"
	/// @DnDArgument : "var" "sprite_index"
	sprite_index = spr_crouch;
}