/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 675EA046
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "jump"
jump = false;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 17BB5555
/// @DnDArgument : "var" "controllock"
/// @DnDArgument : "value" "false"
if(controllock == false)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2EFEAC42
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "var" "crouching"
	/// @DnDArgument : "value" "false"
	if(crouching == false)
	{
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 1672BAE0
		/// @DnDParent : 2EFEAC42
		/// @DnDArgument : "key" "vk_left"
		var l1672BAE0_0;
		l1672BAE0_0 = keyboard_check(vk_left);
		if (l1672BAE0_0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 4CBD5B23
			/// @DnDParent : 1672BAE0
			/// @DnDArgument : "expr" "leftwalk_spd"
			/// @DnDArgument : "var" "hsp"
			hsp = leftwalk_spd;
		}
	
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 1C4AAC35
		/// @DnDParent : 2EFEAC42
		/// @DnDArgument : "key" "vk_right"
		var l1C4AAC35_0;
		l1C4AAC35_0 = keyboard_check(vk_right);
		if (l1C4AAC35_0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 6BD838E6
			/// @DnDParent : 1C4AAC35
			/// @DnDArgument : "expr" "rightwalk_spd"
			/// @DnDArgument : "var" "hsp"
			hsp = rightwalk_spd;
		}
	
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 41CFD9B0
		/// @DnDParent : 2EFEAC42
		/// @DnDArgument : "key" "ord("D")"
		var l41CFD9B0_0;
		l41CFD9B0_0 = keyboard_check(ord("D"));
		if (l41CFD9B0_0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 32E7777B
			/// @DnDParent : 41CFD9B0
			/// @DnDArgument : "expr" "rightwalk_spd"
			/// @DnDArgument : "var" "hsp"
			hsp = rightwalk_spd;
		}
	
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 5FB29EB4
		/// @DnDParent : 2EFEAC42
		/// @DnDArgument : "key" "ord("A")"
		var l5FB29EB4_0;
		l5FB29EB4_0 = keyboard_check(ord("A"));
		if (l5FB29EB4_0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 27DA64F2
			/// @DnDParent : 5FB29EB4
			/// @DnDArgument : "expr" "leftwalk_spd"
			/// @DnDArgument : "var" "hsp"
			hsp = leftwalk_spd;
		}
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
	/// @DnDVersion : 1
	/// @DnDHash : 5F0C21C5
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "vk_up"
	var l5F0C21C5_0;
	l5F0C21C5_0 = keyboard_check_pressed(vk_up);
	if (l5F0C21C5_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 605B09C0
		/// @DnDParent : 5F0C21C5
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "jump"
		jump = true;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
	/// @DnDVersion : 1
	/// @DnDHash : 4DEECBC1
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "ord("W")"
	var l4DEECBC1_0;
	l4DEECBC1_0 = keyboard_check_pressed(ord("W"));
	if (l4DEECBC1_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 407A23D4
		/// @DnDParent : 4DEECBC1
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "jump"
		jump = true;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 43FB02DB
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "vk_down"
	var l43FB02DB_0;
	l43FB02DB_0 = keyboard_check(vk_down);
	if (l43FB02DB_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 3550BDD7
		/// @DnDParent : 43FB02DB
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "crouching"
		crouching = true;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 7703D254
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "ord("S")"
	var l7703D254_0;
	l7703D254_0 = keyboard_check(ord("S"));
	if (l7703D254_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 7D534154
		/// @DnDParent : 7703D254
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "crouching"
		crouching = true;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 6D93792D
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "ord("S")"
	/// @DnDArgument : "not" "1"
	var l6D93792D_0;
	l6D93792D_0 = keyboard_check(ord("S"));
	if (!l6D93792D_0)
	{
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 7F1684F8
		/// @DnDParent : 6D93792D
		/// @DnDArgument : "key" "vk_down"
		/// @DnDArgument : "not" "1"
		var l7F1684F8_0;
		l7F1684F8_0 = keyboard_check(vk_down);
		if (!l7F1684F8_0)
		{
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 1C176B76
			/// @DnDParent : 7F1684F8
			/// @DnDArgument : "expr" "false"
			/// @DnDArgument : "var" "crouching"
			crouching = false;
		}
	}
}