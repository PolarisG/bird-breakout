/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 3F10A562
/// @DnDArgument : "obj" "obj_textbox"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
var l3F10A562_0 = false;
l3F10A562_0 = instance_exists(obj_textbox);
if(!l3F10A562_0)
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 12C76B4F
	/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
	/// @DnDParent : 3F10A562
	with(obj_camera) {
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 4A750176
		/// @DnDParent : 12C76B4F
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "175"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "objectid" "obj_textbox"
		/// @DnDArgument : "layer" ""text""
		/// @DnDSaveInfo : "objectid" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
		instance_create_layer(x + 0, y + 175, "text", obj_textbox);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 55C0A7C7
		/// @DnDParent : 12C76B4F
		/// @DnDArgument : "expr" "obj_clonebird"
		/// @DnDArgument : "var" "camerafollow"
		camerafollow = obj_clonebird;
	}
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 1FE6C7C3
/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
with(obj_textbox) {
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1BEBFE2E
	/// @DnDParent : 1FE6C7C3
	/// @DnDArgument : "expr" "2"
	/// @DnDArgument : "var" "current_text"
	current_text = 2;

	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 2F418D7B
	/// @DnDParent : 1FE6C7C3
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 71E304C7
	/// @DnDParent : 1FE6C7C3
	/// @DnDArgument : "expr" ""Clone Bird:""
	/// @DnDArgument : "var" "speaker"
	speaker = "Clone Bird:";

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3A2A8D84
	/// @DnDParent : 1FE6C7C3
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "1"
	if(textphase == 1)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 74666603
		/// @DnDParent : 3A2A8D84
		/// @DnDArgument : "expr" ""\nSpikes don't kill me! HAHAHA!!""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nSpikes don't kill me! HAHAHA!!";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 167B3CB9
	/// @DnDParent : 1FE6C7C3
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "2"
	if(textphase == 2)
	{
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 5DBBF06C
		/// @DnDParent : 167B3CB9
		instance_destroy();
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 2AF7408D
		/// @DnDApplyTo : b8f2740a-f816-4e35-bfa4-5741586c667f
		/// @DnDParent : 167B3CB9
		with(obj_cutscene1) {
			/// @DnDAction : YoYo Games.Instances.Destroy_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 4D0645BD
			/// @DnDParent : 2AF7408D
			instance_destroy();
		}
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 4771D1D3
		/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
		/// @DnDParent : 167B3CB9
		with(obj_camera) {
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 4FD23EFB
			/// @DnDParent : 4771D1D3
			/// @DnDArgument : "expr" "obj_player"
			/// @DnDArgument : "var" "camerafollow"
			camerafollow = obj_player;
		}
	}
}