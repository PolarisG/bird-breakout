/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 6D947107
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_windleft"
/// @DnDSaveInfo : "object" "770fb874-4b2d-480a-b21b-128305c211e9"
var l6D947107_0 = instance_place(x + 0, y + 0, obj_windleft);
if ((l6D947107_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3131BB21
	/// @DnDParent : 6D947107
	/// @DnDArgument : "var" "on_ground"
	/// @DnDArgument : "value" "true"
	if(on_ground == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 388CA19A
		/// @DnDParent : 3131BB21
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "rightwalk_spd"
		rightwalk_spd = 1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 57CBECB3
		/// @DnDParent : 3131BB21
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "hsp"
		hsp = -1;
	
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
		/// @DnDVersion : 1
		/// @DnDHash : 532E213D
		/// @DnDParent : 3131BB21
		/// @DnDArgument : "key" "vk_left"
		/// @DnDArgument : "not" "1"
		var l532E213D_0;
		l532E213D_0 = keyboard_check(vk_left);
		if (!l532E213D_0)
		{
			/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
			/// @DnDVersion : 1
			/// @DnDHash : 43F9343D
			/// @DnDParent : 532E213D
			/// @DnDArgument : "key" "vk_right"
			/// @DnDArgument : "not" "1"
			var l43F9343D_0;
			l43F9343D_0 = keyboard_check(vk_right);
			if (!l43F9343D_0)
			{
				/// @DnDAction : YoYo Games.Common.Variable
				/// @DnDVersion : 1
				/// @DnDHash : 2FB4BB99
				/// @DnDParent : 43F9343D
				/// @DnDArgument : "expr" "spr_blown"
				/// @DnDArgument : "var" "spr_index"
				spr_index = spr_blown;
			}
		}
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 3A92696C
	/// @DnDParent : 6D947107
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 50927F71
		/// @DnDParent : 3A92696C
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "rightwalk_spd"
		rightwalk_spd = -1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 4EABA600
		/// @DnDParent : 3A92696C
		/// @DnDArgument : "expr" "-2"
		/// @DnDArgument : "var" "hsp"
		hsp = -2;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 260DBCCC
	/// @DnDParent : 6D947107
	/// @DnDArgument : "key" "vk_left"
	var l260DBCCC_0;
	l260DBCCC_0 = keyboard_check(vk_left);
	if (l260DBCCC_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 79C88A27
		/// @DnDParent : 260DBCCC
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "facing"
		facing = -1;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 26558E80
	/// @DnDParent : 6D947107
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0682B4D3
		/// @DnDParent : 26558E80
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "facing "
		facing  = 1;
	}
}

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 2BC390BF
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_windright"
/// @DnDSaveInfo : "object" "99bec057-5bb2-4860-bdb0-244ff1ffcda8"
var l2BC390BF_0 = instance_place(x + 0, y + 0, obj_windright);
if ((l2BC390BF_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 19B0CABA
	/// @DnDParent : 2BC390BF
	/// @DnDArgument : "var" "on_ground"
	/// @DnDArgument : "value" "true"
	if(on_ground == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 40EB8DB6
		/// @DnDParent : 19B0CABA
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "lefttwalk_spd"
		lefttwalk_spd = -1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 69A7F724
		/// @DnDParent : 19B0CABA
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "hsp"
		hsp = 1;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 0173C0F3
	/// @DnDParent : 2BC390BF
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 297519B3
		/// @DnDParent : 0173C0F3
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "leftwalk_spd"
		leftwalk_spd = 1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 35953F8E
		/// @DnDParent : 0173C0F3
		/// @DnDArgument : "expr" "2"
		/// @DnDArgument : "var" "hsp"
		hsp = 2;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 66FCF96D
	/// @DnDParent : 2BC390BF
	/// @DnDArgument : "key" "vk_left"
	var l66FCF96D_0;
	l66FCF96D_0 = keyboard_check(vk_left);
	if (l66FCF96D_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 482494CB
		/// @DnDParent : 66FCF96D
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "var" "facing"
		facing = -1;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 524053B0
	/// @DnDParent : 2BC390BF
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1626F906
		/// @DnDParent : 524053B0
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "facing "
		facing  = 1;
	}
}

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 231F7C67
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_windup"
/// @DnDSaveInfo : "object" "cfca85e2-e9b3-46bc-9902-b6a142e350bb"
var l231F7C67_0 = instance_place(x + 0, y + 0, obj_windup);
if ((l231F7C67_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4F49E58D
	/// @DnDParent : 231F7C67
	/// @DnDArgument : "expr" "-3"
	/// @DnDArgument : "var" "vsp"
	vsp = -3;
}