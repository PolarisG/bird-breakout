/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 7111908D
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
with(obj_player) {
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4BBE7FF2
	/// @DnDParent : 7111908D
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	controllock = true;
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 77F960AE
/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
with(obj_textbox) {
	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 4068850F
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 39ABC890
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "expr" ""Mysterious Voice:""
	/// @DnDArgument : "var" "speaker"
	speaker = "Mysterious Voice:";

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 17F9C2AF
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "1"
	if(textphase == 1)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1E2AFBDF
		/// @DnDParent : 17F9C2AF
		/// @DnDArgument : "expr" ""\nHello.""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nHello.";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 5B646133
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "2"
	if(textphase == 2)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 10E1FC29
		/// @DnDParent : 5B646133
		/// @DnDArgument : "expr" ""\nI can help you escape.""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nI can help you escape.";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6967C2B6
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "3"
	if(textphase == 3)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 19F84D73
		/// @DnDParent : 6967C2B6
		/// @DnDArgument : "expr" ""\nGo through the portal I have \nopened for you. Enter the shadow realm.""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nGo through the portal I have \nopened for you. Enter the shadow realm.";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7EFFC92B
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "4"
	if(textphase == 4)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 4CAF491B
		/// @DnDParent : 7EFFC92B
		/// @DnDArgument : "expr" """"
		/// @DnDArgument : "var" "speaker"
		speaker = "";
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 58DB3FD9
		/// @DnDParent : 7EFFC92B
		/// @DnDArgument : "expr" ""(Use the arrow keys to move)""
		/// @DnDArgument : "var" "textstring"
		textstring = "(Use the arrow keys to move)";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4C66DBAA
	/// @DnDParent : 77F960AE
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "5"
	if(textphase == 5)
	{
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 1417348A
		/// @DnDParent : 4C66DBAA
		instance_destroy();
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 71ED48E7
		/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
		/// @DnDParent : 4C66DBAA
		with(obj_player) {
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 01FF173A
			/// @DnDParent : 71ED48E7
			/// @DnDArgument : "expr" "false"
			/// @DnDArgument : "var" "controllock"
			controllock = false;
		}
	}
}