/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 731F993A
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_trap"
/// @DnDSaveInfo : "object" "a3ffabe8-e15b-413a-9184-2f9f55cbdde8"
var l731F993A_0 = instance_place(x + 0, y + 0, obj_trap);
if ((l731F993A_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 7AAB193C
	/// @DnDApplyTo : a3ffabe8-e15b-413a-9184-2f9f55cbdde8
	/// @DnDParent : 731F993A
	with(obj_trap) {
		/// @DnDAction : YoYo Games.Instances.Set_Sprite
		/// @DnDVersion : 1
		/// @DnDHash : 7C10E711
		/// @DnDParent : 7AAB193C
		/// @DnDArgument : "imageind" "1"
		/// @DnDArgument : "spriteind" "spr_trapbutton"
		/// @DnDSaveInfo : "spriteind" "e4112751-69b9-49b7-bb61-0b3a6e16bee1"
		sprite_index = spr_trapbutton;
		image_index = 1;
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 53AED04E
else
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 796DE336
	/// @DnDApplyTo : a3ffabe8-e15b-413a-9184-2f9f55cbdde8
	/// @DnDParent : 53AED04E
	with(obj_trap) {
		/// @DnDAction : YoYo Games.Instances.Set_Sprite
		/// @DnDVersion : 1
		/// @DnDHash : 46876B99
		/// @DnDParent : 796DE336
		/// @DnDArgument : "imageind" "2"
		/// @DnDArgument : "spriteind" "spr_trapbutton"
		/// @DnDSaveInfo : "spriteind" "e4112751-69b9-49b7-bb61-0b3a6e16bee1"
		sprite_index = spr_trapbutton;
		image_index = 2;
	}
}