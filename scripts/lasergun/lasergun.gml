/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7D348FCF
/// @DnDArgument : "var" "facing"
/// @DnDArgument : "value" "-1"
if(facing == -1)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 25C1EBB3
	/// @DnDParent : 7D348FCF
	/// @DnDArgument : "var" "hasgun"
	/// @DnDArgument : "value" "true"
	if(hasgun == true)
	{
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 17F96360
		/// @DnDParent : 25C1EBB3
		var l17F96360_0;
		l17F96360_0 = keyboard_check_pressed(vk_space);
		if (l17F96360_0)
		{
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 36758653
			/// @DnDParent : 17F96360
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "objectid" "obj_laserleft"
			/// @DnDSaveInfo : "objectid" "8d33e9c5-4529-45cb-91b9-0b206f882847"
			instance_create_layer(x + 0, y + 0, "Instances", obj_laserleft);
		}
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 36ACB6C3
/// @DnDArgument : "var" "facing"
/// @DnDArgument : "value" "1"
if(facing == 1)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2A1452D2
	/// @DnDParent : 36ACB6C3
	/// @DnDArgument : "var" "hasgun"
	/// @DnDArgument : "value" "true"
	if(hasgun == true)
	{
		/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
		/// @DnDVersion : 1
		/// @DnDHash : 3BEE1832
		/// @DnDParent : 2A1452D2
		var l3BEE1832_0;
		l3BEE1832_0 = keyboard_check_pressed(vk_space);
		if (l3BEE1832_0)
		{
			/// @DnDAction : YoYo Games.Instances.Create_Instance
			/// @DnDVersion : 1
			/// @DnDHash : 70974501
			/// @DnDParent : 3BEE1832
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			/// @DnDArgument : "objectid" "obj_laserright"
			/// @DnDSaveInfo : "objectid" "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
			instance_create_layer(x + 0, y + 0, "Instances", obj_laserright);
		}
	}
}