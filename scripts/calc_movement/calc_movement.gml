/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 0EBBFAC3
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_windleft"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "object" "770fb874-4b2d-480a-b21b-128305c211e9"
var l0EBBFAC3_0 = instance_place(x + 0, y + 0, obj_windleft);
if (!(l0EBBFAC3_0 > 0))
{
	/// @DnDAction : YoYo Games.Collisions.If_Object_At
	/// @DnDVersion : 1.1
	/// @DnDHash : 4595D5FD
	/// @DnDParent : 0EBBFAC3
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "object" "obj_windright"
	/// @DnDArgument : "not" "1"
	/// @DnDSaveInfo : "object" "99bec057-5bb2-4860-bdb0-244ff1ffcda8"
	var l4595D5FD_0 = instance_place(x + 0, y + 0, obj_windright);
	if (!(l4595D5FD_0 > 0))
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 45EAD9B7
		/// @DnDParent : 4595D5FD
		/// @DnDArgument : "expr" "-3"
		/// @DnDArgument : "var" "leftwalk_spd"
		leftwalk_spd = -3;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 427AB1E6
		/// @DnDParent : 4595D5FD
		/// @DnDArgument : "expr" "3"
		/// @DnDArgument : "var" "rightwalk_spd"
		rightwalk_spd = 3;
	}
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3F1A3F6B
/// @DnDArgument : "expr" "hsp*drag"
/// @DnDArgument : "var" "hsp"
hsp = hsp*drag;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 74F1F446
/// @DnDArgument : "var" "crouching"
/// @DnDArgument : "value" "true"
if(crouching == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7B48D5E4
	/// @DnDParent : 74F1F446
	/// @DnDArgument : "expr" "2 * global.grav"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "vsp"
	vsp += 2 * global.grav;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 5DAED285
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 00275D9F
	/// @DnDParent : 5DAED285
	/// @DnDArgument : "expr" "global.grav"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "vsp"
	vsp += global.grav;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 427E8A22
/// @DnDArgument : "var" "abs(hsp)"
/// @DnDArgument : "op" "1"
/// @DnDArgument : "value" "0.2"
if(abs(hsp) < 0.2)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4E4011FE
	/// @DnDParent : 427E8A22
	/// @DnDArgument : "var" "hsp"
	hsp = 0;
}