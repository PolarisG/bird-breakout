/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 675EA046
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "jump"
jump = false;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 17BB5555
/// @DnDArgument : "var" "controllock"
/// @DnDArgument : "value" "false"
if(controllock == false)
{
	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 1C4AAC35
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "vk_right"
	var l1C4AAC35_0;
	l1C4AAC35_0 = keyboard_check(vk_right);
	if (l1C4AAC35_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6BD838E6
		/// @DnDParent : 1C4AAC35
		/// @DnDArgument : "expr" "leftwalk_spd"
		/// @DnDArgument : "var" "hsp"
		hsp = leftwalk_spd;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 7616EF54
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "vk_left"
	var l7616EF54_0;
	l7616EF54_0 = keyboard_check(vk_left);
	if (l7616EF54_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 4CF385F8
		/// @DnDParent : 7616EF54
		/// @DnDArgument : "expr" "rightwalk_spd"
		/// @DnDArgument : "var" "hsp"
		hsp = rightwalk_spd;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
	/// @DnDVersion : 1
	/// @DnDHash : 5F0C21C5
	/// @DnDParent : 17BB5555
	/// @DnDArgument : "key" "vk_up"
	var l5F0C21C5_0;
	l5F0C21C5_0 = keyboard_check_pressed(vk_up);
	if (l5F0C21C5_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 605B09C0
		/// @DnDParent : 5F0C21C5
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "jump"
		jump = true;
	}
}