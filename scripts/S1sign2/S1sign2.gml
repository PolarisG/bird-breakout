/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 0953CB74
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
with(obj_player) {
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3B4A8FC8
	/// @DnDParent : 0953CB74
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	controllock = true;
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 1888BD05
/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
with(obj_textbox) {
	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 124F917D
	/// @DnDParent : 1888BD05
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 086E5F00
	/// @DnDParent : 1888BD05
	/// @DnDArgument : "expr" ""Sign:""
	/// @DnDArgument : "var" "speaker"
	speaker = "Sign:";

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 605CC4BF
	/// @DnDParent : 1888BD05
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "1"
	if(textphase == 1)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 5D2C56CF
		/// @DnDParent : 605CC4BF
		/// @DnDArgument : "expr" ""\nYou can also double jump""
		/// @DnDArgument : "var" "textstring"
		textstring = "\nYou can also double jump";
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 55C5466A
	/// @DnDParent : 1888BD05
	/// @DnDArgument : "var" "textphase"
	/// @DnDArgument : "value" "2"
	if(textphase == 2)
	{
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 634A390E
		/// @DnDParent : 55C5466A
		instance_destroy();
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 10E7A5F9
		/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
		/// @DnDParent : 55C5466A
		with(obj_player) {
			/// @DnDAction : YoYo Games.Common.Variable
			/// @DnDVersion : 1
			/// @DnDHash : 77F3A4AC
			/// @DnDParent : 10E7A5F9
			/// @DnDArgument : "expr" "false"
			/// @DnDArgument : "var" "controllock"
			controllock = false;
		}
	}
}