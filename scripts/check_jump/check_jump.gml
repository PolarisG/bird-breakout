/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5E18C0EB
/// @DnDArgument : "var" "on_ground"
/// @DnDArgument : "value" "true"
if(on_ground == true)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7673FB18
	/// @DnDParent : 5E18C0EB
	/// @DnDArgument : "var" "triplejump"
	/// @DnDArgument : "value" "true"
	if(triplejump == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 0A6962A5
		/// @DnDParent : 7673FB18
		/// @DnDArgument : "expr" "2"
		/// @DnDArgument : "var" "extrajump_count"
		extrajump_count = 2;
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 629D7943
	/// @DnDParent : 5E18C0EB
	else
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2B7977BB
		/// @DnDParent : 629D7943
		/// @DnDArgument : "expr" "1"
		/// @DnDArgument : "var" "extrajump_count"
		extrajump_count = 1;
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 11CBFA23
/// @DnDArgument : "var" "extrajump_count"
/// @DnDArgument : "op" "4"
if(extrajump_count >= 0)
{
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 397C5D5D
	/// @DnDParent : 11CBFA23
	/// @DnDArgument : "var" "jump"
	/// @DnDArgument : "value" "true"
	if(jump == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 1465A615
		/// @DnDParent : 397C5D5D
		/// @DnDArgument : "expr" "-1"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "extrajump_count"
		extrajump_count += -1;
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 46694E75
		/// @DnDParent : 397C5D5D
		/// @DnDArgument : "expr" "-jump_spd"
		/// @DnDArgument : "var" "vsp"
		vsp = -jump_spd;
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55B1DC59
/// @DnDArgument : "var" "global.currentlevel"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "10"
if(global.currentlevel > 10)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7EFD04B2
	/// @DnDParent : 55B1DC59
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "triplejump"
	triplejump = true;
}