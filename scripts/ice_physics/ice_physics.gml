/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B03253C
/// @DnDArgument : "var" "on_ice"
/// @DnDArgument : "value" "true"
if(on_ice == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 70898B39
	/// @DnDParent : 4B03253C
	/// @DnDArgument : "expr" "3.5"
	/// @DnDArgument : "var" "walk_spd"
	walk_spd = 3.5;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 27BC8E8B
	/// @DnDParent : 4B03253C
	/// @DnDArgument : "expr" "0.99"
	/// @DnDArgument : "var" "drag"
	drag = 0.99;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 32DF8CCD
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 5EA79476
	/// @DnDParent : 32DF8CCD
	/// @DnDArgument : "expr" "0.9"
	/// @DnDArgument : "var" "drag"
	drag = 0.9;

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3E5A2F3D
	/// @DnDParent : 32DF8CCD
	/// @DnDArgument : "expr" "3"
	/// @DnDArgument : "var" "walk_spd"
	walk_spd = 3;
}

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 0DD0514A
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "global.grav"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_ice"
/// @DnDSaveInfo : "object" "ec2afcfd-a37a-450d-9880-bd7dc2b26546"
var l0DD0514A_0 = instance_place(x + 0, y + global.grav, obj_ice);
if ((l0DD0514A_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 703EC583
	/// @DnDParent : 0DD0514A
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "on_ice"
	on_ice = true;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 2F4AEFF3
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7E988A68
	/// @DnDParent : 2F4AEFF3
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "on_ice"
	on_ice = false;
}