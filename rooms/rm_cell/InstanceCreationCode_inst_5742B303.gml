/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 0D5A7944
/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
with(obj_textbox) {
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 0D585C3D
	/// @DnDParent : 0D5A7944
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "var" "current_text"
	current_text = 1;
}

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 204BB91D
/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
with(obj_camera) {
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7A051017
	/// @DnDParent : 204BB91D
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos" "175"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_textbox"
	/// @DnDArgument : "layer" ""text""
	/// @DnDSaveInfo : "objectid" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
	instance_create_layer(x + 0, y + 175, "text", obj_textbox);
}