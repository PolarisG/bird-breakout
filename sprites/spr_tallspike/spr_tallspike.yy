{
    "id": "254d3ba7-1ace-4073-9032-adcc35c7a450",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tallspike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2fb3e54-1f85-49c5-9595-2e62bcc3f5a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "254d3ba7-1ace-4073-9032-adcc35c7a450",
            "compositeImage": {
                "id": "878e1503-fbad-40cb-aa31-abac23a4da15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fb3e54-1f85-49c5-9595-2e62bcc3f5a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5fbf4b-e2f9-4334-8333-83792268f264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fb3e54-1f85-49c5-9595-2e62bcc3f5a9",
                    "LayerId": "9a199515-b4d9-40f9-8a19-41a5ec380167"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9a199515-b4d9-40f9-8a19-41a5ec380167",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "254d3ba7-1ace-4073-9032-adcc35c7a450",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}