{
    "id": "6279e598-0a78-4fd9-b108-fd86266eb2a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spikeball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34fea67e-f829-434a-946a-e7da0ac697f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6279e598-0a78-4fd9-b108-fd86266eb2a0",
            "compositeImage": {
                "id": "630c95ff-f0dd-47dd-bd46-11417a6b2ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fea67e-f829-434a-946a-e7da0ac697f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774498fb-3f17-47ec-9700-740df500fa76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fea67e-f829-434a-946a-e7da0ac697f3",
                    "LayerId": "89ed1e44-afb2-47dc-92d5-8d9629849739"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "89ed1e44-afb2-47dc-92d5-8d9629849739",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6279e598-0a78-4fd9-b108-fd86266eb2a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}