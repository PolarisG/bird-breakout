{
    "id": "afb48b22-976e-442b-8f85-cfc422a376fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portal_in",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 128,
    "bbox_left": 0,
    "bbox_right": 96,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa096edc-36b2-414d-a525-4d40ad212f93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb48b22-976e-442b-8f85-cfc422a376fe",
            "compositeImage": {
                "id": "1f4fd93a-a6aa-4e2d-80a3-459cca8e202f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa096edc-36b2-414d-a525-4d40ad212f93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6d72e8-1b23-47e7-8a45-b14b6b5bd927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa096edc-36b2-414d-a525-4d40ad212f93",
                    "LayerId": "8b87bd7d-7cd4-436b-a268-e6929f4dbd19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8b87bd7d-7cd4-436b-a268-e6929f4dbd19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afb48b22-976e-442b-8f85-cfc422a376fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 64
}