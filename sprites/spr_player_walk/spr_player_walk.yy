{
    "id": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fe82cf4-f231-4630-acef-2ae3ad097d3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "aa1fa7ca-58b4-41e1-85f6-ac9ee12d81f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe82cf4-f231-4630-acef-2ae3ad097d3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2673328e-100b-40c4-982e-935fa2f0a494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe82cf4-f231-4630-acef-2ae3ad097d3f",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        },
        {
            "id": "ed1f565a-5325-4f95-9967-9db867633762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "9a5bb3b1-e988-4f74-a01a-bd9093cc1b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1f565a-5325-4f95-9967-9db867633762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97e946b-6dba-477f-bf32-90af110f8831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1f565a-5325-4f95-9967-9db867633762",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        },
        {
            "id": "6fa98971-1cc4-46b2-b996-ac5cb35b180b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "2fc7f395-c738-447e-8c31-3b46c6401b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa98971-1cc4-46b2-b996-ac5cb35b180b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1616ec1-913e-4e93-b860-576fc7e85567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa98971-1cc4-46b2-b996-ac5cb35b180b",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        },
        {
            "id": "6954bcae-0fae-4b42-b250-8d2aa3e31ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "a60c70d1-d295-4a45-9a64-58cd4fb6db33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6954bcae-0fae-4b42-b250-8d2aa3e31ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4823753e-fa02-4e21-89e8-6f129c33672c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6954bcae-0fae-4b42-b250-8d2aa3e31ca4",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        },
        {
            "id": "75f65188-ce7a-4f74-a71f-c76e724444f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "749b5004-4eed-43b9-b9d6-8fc6663b5374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f65188-ce7a-4f74-a71f-c76e724444f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e14e61-d428-4a22-aa37-c74b61eb86d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f65188-ce7a-4f74-a71f-c76e724444f0",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        },
        {
            "id": "aaf0d3e7-c882-41e9-89be-5da1be3c8ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "compositeImage": {
                "id": "2d583e13-9f12-41f8-ad7a-873bdadd7473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf0d3e7-c882-41e9-89be-5da1be3c8ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670a8a44-6aa9-4c62-b3c8-e8f2eaf30fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf0d3e7-c882-41e9-89be-5da1be3c8ced",
                    "LayerId": "8e4bb888-b8e8-469d-8483-5cfae990dced"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8e4bb888-b8e8-469d-8483-5cfae990dced",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2ec75ac-4e77-46b8-a48a-d33fd9af7042",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}