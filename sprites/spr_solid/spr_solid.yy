{
    "id": "25ea8889-ec38-434e-a780-9ef53ee4f794",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "967abbec-f44d-4e22-9348-cba53a098973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25ea8889-ec38-434e-a780-9ef53ee4f794",
            "compositeImage": {
                "id": "75b0a050-64eb-4599-825e-d6a5d8e45d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "967abbec-f44d-4e22-9348-cba53a098973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bca7761-0b77-41e3-9193-c26c132fa307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "967abbec-f44d-4e22-9348-cba53a098973",
                    "LayerId": "0438c657-c959-4db2-b251-4c6062fe5a52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0438c657-c959-4db2-b251-4c6062fe5a52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25ea8889-ec38-434e-a780-9ef53ee4f794",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}