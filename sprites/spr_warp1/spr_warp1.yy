{
    "id": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31a63319-66b8-4096-8b2f-ce03bcab3a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "b8600b66-a7ab-4615-9cba-767001bc9163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a63319-66b8-4096-8b2f-ce03bcab3a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85e688c-e02e-4a5b-b1ca-039328d46e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a63319-66b8-4096-8b2f-ce03bcab3a2a",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "d1264de4-eb79-4581-ae32-a6d936ab6a08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "eaff99a6-18cc-4db5-b7e6-4531c31063cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1264de4-eb79-4581-ae32-a6d936ab6a08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193f0817-5d79-47d0-a552-3f9a2532d9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1264de4-eb79-4581-ae32-a6d936ab6a08",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "d663ed4b-11f5-4cfe-bcfd-8bc1724af27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "88e4b977-3db9-4cde-bdcd-5aa7fa34f41a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d663ed4b-11f5-4cfe-bcfd-8bc1724af27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6df8f72-d4fb-4a82-990b-2e1b7dca3509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d663ed4b-11f5-4cfe-bcfd-8bc1724af27f",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "43c1bfee-78e2-4d83-8b1a-8a2db972f127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "13ed0a92-2236-41f1-ac30-c99ae68bb49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c1bfee-78e2-4d83-8b1a-8a2db972f127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea2b6286-02f7-466d-ab5d-c60504664d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c1bfee-78e2-4d83-8b1a-8a2db972f127",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "b2fc6b7f-f5bb-4a4e-9484-c39cb3ae86e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "feb8bac9-6db3-40ee-964c-896a80594ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2fc6b7f-f5bb-4a4e-9484-c39cb3ae86e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afaeca5d-e80d-4d4b-8c44-42a0df5ac9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2fc6b7f-f5bb-4a4e-9484-c39cb3ae86e3",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "0902f088-f62d-4490-a4a8-c210c51a8495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "c535e16d-0f92-43c3-869b-9f3e49a97b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0902f088-f62d-4490-a4a8-c210c51a8495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b9fe6c-a860-448e-b4e8-21e650f3a4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0902f088-f62d-4490-a4a8-c210c51a8495",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "2e04cef9-6c9e-4a89-8f01-9f2327f59dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "b7ea9376-a3d0-4383-ba45-75027dcc9c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e04cef9-6c9e-4a89-8f01-9f2327f59dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab12ca9-853a-4650-870b-182071a534b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e04cef9-6c9e-4a89-8f01-9f2327f59dfd",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "f71fec58-ec15-40ff-978b-6010e482c976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "b198a15f-ddbb-4aa5-9603-9bf866c98381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71fec58-ec15-40ff-978b-6010e482c976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d07f952-d3ac-446a-ac9b-5477c27cf6fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71fec58-ec15-40ff-978b-6010e482c976",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "dbae0c4b-c681-4622-b5d9-0b397b5ba389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "6d8d1ced-74e7-485b-9475-fa370e68a27b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbae0c4b-c681-4622-b5d9-0b397b5ba389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dadba25-53d0-4453-9555-5cefd3951ef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbae0c4b-c681-4622-b5d9-0b397b5ba389",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "02f5fa58-b9e6-46f0-80c3-9cc071a92f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "556590af-45a6-400b-9bc5-fe0f31c5cd3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f5fa58-b9e6-46f0-80c3-9cc071a92f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb7958b-34aa-4f3d-b151-16142c3df0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f5fa58-b9e6-46f0-80c3-9cc071a92f97",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "2feaf68d-86c2-4862-b986-74ff5f3e2c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "dad62f74-84ae-4080-8cff-adcdae510511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2feaf68d-86c2-4862-b986-74ff5f3e2c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0579bf-4e13-4231-ba16-7c45740cd06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2feaf68d-86c2-4862-b986-74ff5f3e2c18",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "b3bee228-5d19-4954-a0bd-7985a0f577b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "c07f4408-1102-47d1-801a-b285d8eaf9ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3bee228-5d19-4954-a0bd-7985a0f577b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569912c9-4a33-47ff-8b81-46016977833d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3bee228-5d19-4954-a0bd-7985a0f577b6",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "5204ea4d-ee03-4d57-ba4b-c1a00f008e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "470a308b-4d53-4c05-aeb7-6e432a3b5b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5204ea4d-ee03-4d57-ba4b-c1a00f008e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7698c76d-6f46-4887-ba8e-5dfc48e78e7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5204ea4d-ee03-4d57-ba4b-c1a00f008e81",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "c6f0166e-2b4f-4a66-a4f9-702cc9a9bffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "b2bedd32-3ba5-42f8-a6e7-1d4d6de4fb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f0166e-2b4f-4a66-a4f9-702cc9a9bffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1a6ebc-cfc8-4cf4-98c2-4ab1301e8602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f0166e-2b4f-4a66-a4f9-702cc9a9bffd",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        },
        {
            "id": "a2e5af60-f3fb-48ba-80e8-f6ea68070c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "compositeImage": {
                "id": "b40eef23-90f0-4103-ab42-88f92431abc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e5af60-f3fb-48ba-80e8-f6ea68070c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e88d9930-cc18-4897-8fea-38fe858fcd9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e5af60-f3fb-48ba-80e8-f6ea68070c00",
                    "LayerId": "a1cb0083-2960-4661-a3c6-bc2623e862e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a1cb0083-2960-4661-a3c6-bc2623e862e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}