{
    "id": "b3bdd63f-029c-4f64-ab63-67c120d0666b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite33",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76bd308f-62ba-46c3-babe-b9a90ce0bb1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3bdd63f-029c-4f64-ab63-67c120d0666b",
            "compositeImage": {
                "id": "fc142fc2-78ad-46ef-ac3e-90c2b3b668d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76bd308f-62ba-46c3-babe-b9a90ce0bb1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365d4926-1afd-47e9-9217-631bd09f1bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76bd308f-62ba-46c3-babe-b9a90ce0bb1e",
                    "LayerId": "37a537c3-c47d-48b5-871f-d50da1d22c58"
                }
            ]
        },
        {
            "id": "a5878760-a2be-4dac-bc75-1706ae5cacd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3bdd63f-029c-4f64-ab63-67c120d0666b",
            "compositeImage": {
                "id": "e2fe29eb-367a-4366-b899-25cf9c5e9133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5878760-a2be-4dac-bc75-1706ae5cacd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bda0b5-ef06-46be-98b1-e263cf17c858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5878760-a2be-4dac-bc75-1706ae5cacd0",
                    "LayerId": "37a537c3-c47d-48b5-871f-d50da1d22c58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "37a537c3-c47d-48b5-871f-d50da1d22c58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3bdd63f-029c-4f64-ab63-67c120d0666b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}