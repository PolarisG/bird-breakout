{
    "id": "176d468f-ee41-49c1-9971-c0694c71c944",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 2,
    "bbox_right": 55,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6db4bb25-eee8-431c-94bc-0ce173243032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "176d468f-ee41-49c1-9971-c0694c71c944",
            "compositeImage": {
                "id": "a2edadaf-0e87-4986-90d1-722d93075a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db4bb25-eee8-431c-94bc-0ce173243032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1827756-5a6c-42ea-8c00-5d5bb38006d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db4bb25-eee8-431c-94bc-0ce173243032",
                    "LayerId": "8d57b34e-b678-4221-a0bf-3f71ba3fc18a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8d57b34e-b678-4221-a0bf-3f71ba3fc18a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "176d468f-ee41-49c1-9971-c0694c71c944",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}