{
    "id": "e4112751-69b9-49b7-bb61-0b3a6e16bee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trapbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbe3a6ca-882e-40fc-a723-fdb9416ee5dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4112751-69b9-49b7-bb61-0b3a6e16bee1",
            "compositeImage": {
                "id": "853bdf8d-9295-4222-a6e1-8a0730ad2a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe3a6ca-882e-40fc-a723-fdb9416ee5dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126e4282-ada9-46e6-8516-52eff07ff9cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe3a6ca-882e-40fc-a723-fdb9416ee5dc",
                    "LayerId": "8ef2a8b3-fbd4-481e-80fa-c4e64c6083cb"
                }
            ]
        },
        {
            "id": "ff67a023-db1a-43d1-a41e-f574e3c1b29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4112751-69b9-49b7-bb61-0b3a6e16bee1",
            "compositeImage": {
                "id": "a679ee85-850b-4912-b5cd-7b2854982265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff67a023-db1a-43d1-a41e-f574e3c1b29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89711f4d-e19f-4e52-b4c3-0baaf67c6c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff67a023-db1a-43d1-a41e-f574e3c1b29d",
                    "LayerId": "8ef2a8b3-fbd4-481e-80fa-c4e64c6083cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8ef2a8b3-fbd4-481e-80fa-c4e64c6083cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4112751-69b9-49b7-bb61-0b3a6e16bee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}