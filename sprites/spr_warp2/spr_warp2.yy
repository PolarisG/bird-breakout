{
    "id": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41ee60c3-3e25-47c7-93f2-ea2b6399e4ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "7cc16325-5806-427e-94b3-9f4673513dfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ee60c3-3e25-47c7-93f2-ea2b6399e4ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69471d80-3282-4e10-bac6-c5aba165b296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ee60c3-3e25-47c7-93f2-ea2b6399e4ab",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "71c97a79-29a9-4914-88a2-07eb12112a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "4c6aa8d7-6dd2-4639-9398-084fc155daf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c97a79-29a9-4914-88a2-07eb12112a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027a327d-2f14-4f60-bdb8-29f73a3eeb04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c97a79-29a9-4914-88a2-07eb12112a93",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "4918f257-15f4-4556-9d44-407898d2d290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "4ab7b83f-ed6b-4885-b001-965be6e3e9d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4918f257-15f4-4556-9d44-407898d2d290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a9c49b-4559-4eaf-89c7-35016988b5f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4918f257-15f4-4556-9d44-407898d2d290",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "5dd8f1ec-d60e-46e0-a2ff-3d3632d4b7c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "57eed299-1d7a-4b1f-aa33-838db0f817d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd8f1ec-d60e-46e0-a2ff-3d3632d4b7c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5740ddcc-a3f2-4861-a5db-fc780ac1cec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd8f1ec-d60e-46e0-a2ff-3d3632d4b7c9",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "b7f8e0f5-d337-43b4-8bd9-db0b125caa66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "e22b0a36-7830-43ff-acbf-13b7274398e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f8e0f5-d337-43b4-8bd9-db0b125caa66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207e8809-25a4-4a15-a1c7-77041a46f525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f8e0f5-d337-43b4-8bd9-db0b125caa66",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "f29da84c-685d-4ca7-9543-2272e13e6b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "94a994e7-f65e-443e-8045-a4943761ca90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f29da84c-685d-4ca7-9543-2272e13e6b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad53d9c-4e36-4735-9571-c0cea678d84d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f29da84c-685d-4ca7-9543-2272e13e6b20",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "feb81f93-e16e-423b-bad1-869843373136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "d6e4d398-c2c5-484c-8f27-82759ce3a274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb81f93-e16e-423b-bad1-869843373136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96b90f0-8b03-4ff9-8d63-61c497f0c3e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb81f93-e16e-423b-bad1-869843373136",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "0b8b7892-2e1d-48e2-ab93-b60ae0f186f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "4d81ecc8-0281-41b1-898e-8faae04c3d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8b7892-2e1d-48e2-ab93-b60ae0f186f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6800792f-2e7c-400b-b7e5-d35d622c0eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8b7892-2e1d-48e2-ab93-b60ae0f186f2",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "17ec9aeb-25e1-42d7-8108-1fc5a51ce2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "3ad72862-1999-4924-bf28-c70569685a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17ec9aeb-25e1-42d7-8108-1fc5a51ce2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe5df0f-e020-4942-9cb1-675756a565ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17ec9aeb-25e1-42d7-8108-1fc5a51ce2f1",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "be647891-2a14-4d37-aec1-19bc1d19c388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "c1395f91-d47c-411e-b663-4dff73841b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be647891-2a14-4d37-aec1-19bc1d19c388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0721b8e-9b88-4cf6-a4af-a73344ee3d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be647891-2a14-4d37-aec1-19bc1d19c388",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "57a1b003-e25b-4233-b457-46988654f77f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "f35a6ced-8651-4b3f-9f7a-eef627b6fe30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a1b003-e25b-4233-b457-46988654f77f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f3c8486-13e9-4b92-b79c-542c70b3f264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a1b003-e25b-4233-b457-46988654f77f",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "6d342f02-0336-499e-9ebe-699166ffc79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "2f628f0b-07d4-4a95-8f31-5548c1ed1f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d342f02-0336-499e-9ebe-699166ffc79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9e8fd0-4315-4796-9d8b-fac658077e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d342f02-0336-499e-9ebe-699166ffc79e",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "1138fa09-269d-4873-8fe5-e38314bb4f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "f2509cc7-6053-42c4-a5cf-2ec9d474abfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1138fa09-269d-4873-8fe5-e38314bb4f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146c00c1-6822-4598-94a6-60e011ce5195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1138fa09-269d-4873-8fe5-e38314bb4f7c",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "15487a50-99c0-4669-9768-1ebd7a71b244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "ffd43c6e-757f-4979-8fa1-f1ba64fb6727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15487a50-99c0-4669-9768-1ebd7a71b244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f26aa2-6a50-4bd2-8b47-7162022cdfa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15487a50-99c0-4669-9768-1ebd7a71b244",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        },
        {
            "id": "033130c5-7d2a-4b3d-ac21-d47ad7ef810b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "compositeImage": {
                "id": "525fbd1b-6874-4fd0-9813-cab317f38a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033130c5-7d2a-4b3d-ac21-d47ad7ef810b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80e8b0a-ddb7-434d-abc2-0939e3815240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033130c5-7d2a-4b3d-ac21-d47ad7ef810b",
                    "LayerId": "f5c51571-24a7-45e5-aa06-201a1bba6428"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f5c51571-24a7-45e5-aa06-201a1bba6428",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}