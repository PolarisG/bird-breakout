{
    "id": "eac3dc59-0274-462b-8635-c3526609a0a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dropgenerator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2e2d798-f433-4d35-8073-5a644c89da8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eac3dc59-0274-462b-8635-c3526609a0a1",
            "compositeImage": {
                "id": "df821541-8e2e-4b47-8825-7de6ae09a362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e2d798-f433-4d35-8073-5a644c89da8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca29b3bb-d676-40b5-85cc-833ad6fd0e11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e2d798-f433-4d35-8073-5a644c89da8c",
                    "LayerId": "96de7cef-61fa-4152-bfa9-0f53e92e78d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "96de7cef-61fa-4152-bfa9-0f53e92e78d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eac3dc59-0274-462b-8635-c3526609a0a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 1,
    "yorig": 1
}