{
    "id": "c95cd48f-dec3-40a7-a617-f352691fe802",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portal_out",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 128,
    "bbox_left": 0,
    "bbox_right": 96,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15ec1145-21dd-4d02-9426-d51fb916f373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95cd48f-dec3-40a7-a617-f352691fe802",
            "compositeImage": {
                "id": "224932fb-f42b-4880-9fb4-744be2e57e58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ec1145-21dd-4d02-9426-d51fb916f373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5960b8-642f-41ad-917e-8f0bf8cda134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ec1145-21dd-4d02-9426-d51fb916f373",
                    "LayerId": "d39860a8-31c2-4349-9045-13b5b50cc6b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d39860a8-31c2-4349-9045-13b5b50cc6b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c95cd48f-dec3-40a7-a617-f352691fe802",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 64
}