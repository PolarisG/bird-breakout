{
    "id": "b77218a9-a681-4762-8642-4dcc3841131c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_doormedium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2c2215d-302c-4893-b58e-49abd8523448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77218a9-a681-4762-8642-4dcc3841131c",
            "compositeImage": {
                "id": "07638df2-3758-4bef-b851-f64947d29d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c2215d-302c-4893-b58e-49abd8523448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a173273e-8913-4d67-9207-5fb9fdb30c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c2215d-302c-4893-b58e-49abd8523448",
                    "LayerId": "f49800ef-065f-41d2-b0d1-7fb5114dc3f1"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 32,
    "height": 128,
    "layers": [
        {
            "id": "f49800ef-065f-41d2-b0d1-7fb5114dc3f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b77218a9-a681-4762-8642-4dcc3841131c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 64
}