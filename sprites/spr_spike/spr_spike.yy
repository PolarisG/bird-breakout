{
    "id": "15875281-d4c3-4f2d-a6d9-5b9262a0f141",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91713e14-d918-40bf-9467-f1f44eb9b705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15875281-d4c3-4f2d-a6d9-5b9262a0f141",
            "compositeImage": {
                "id": "59d4e901-ce01-4d91-b20b-a79c992d8183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91713e14-d918-40bf-9467-f1f44eb9b705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82a16824-c371-4d66-86b0-fb1eeabeca67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91713e14-d918-40bf-9467-f1f44eb9b705",
                    "LayerId": "d5f14038-450c-4e07-ae42-03541ecc93f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5f14038-450c-4e07-ae42-03541ecc93f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15875281-d4c3-4f2d-a6d9-5b9262a0f141",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}