{
    "id": "38efe206-356a-4043-a174-fbf0f90922d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nothing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30a92281-412e-488b-af69-3fc78f01519b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38efe206-356a-4043-a174-fbf0f90922d5",
            "compositeImage": {
                "id": "5597a985-5b8b-4d29-947e-ddeb847361e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a92281-412e-488b-af69-3fc78f01519b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8117089d-a5ab-4b23-a334-6f4a22089152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a92281-412e-488b-af69-3fc78f01519b",
                    "LayerId": "73421e77-6864-44b5-9a98-1f67077482f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "73421e77-6864-44b5-9a98-1f67077482f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38efe206-356a-4043-a174-fbf0f90922d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}