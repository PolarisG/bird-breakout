{
    "id": "894350fa-c44b-4595-98de-e375abfd6e52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadowmonsterwalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 57,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "732a560c-0815-44e2-8ac5-7060cbfd3f22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "894350fa-c44b-4595-98de-e375abfd6e52",
            "compositeImage": {
                "id": "56e7aa71-509c-4443-b570-445d202ffdbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "732a560c-0815-44e2-8ac5-7060cbfd3f22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b274e2c-b457-49b7-a4f2-bdf1a56fbb89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732a560c-0815-44e2-8ac5-7060cbfd3f22",
                    "LayerId": "889ee7f2-0ef4-4d8e-b7db-c03846b18ee6"
                }
            ]
        },
        {
            "id": "a963392f-cb3d-4c71-b2d1-28a3a3ef61e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "894350fa-c44b-4595-98de-e375abfd6e52",
            "compositeImage": {
                "id": "a7f3ebb9-7cab-4313-863e-b7b42ef7790a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a963392f-cb3d-4c71-b2d1-28a3a3ef61e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f42089-b772-4cf6-b929-569a3c5b6fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a963392f-cb3d-4c71-b2d1-28a3a3ef61e0",
                    "LayerId": "889ee7f2-0ef4-4d8e-b7db-c03846b18ee6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "889ee7f2-0ef4-4d8e-b7db-c03846b18ee6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "894350fa-c44b-4595-98de-e375abfd6e52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}