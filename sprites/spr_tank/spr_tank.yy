{
    "id": "50116910-14b4-492c-b3ff-6b50674b825a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94362013-a952-413c-9c86-4e3120e3d496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50116910-14b4-492c-b3ff-6b50674b825a",
            "compositeImage": {
                "id": "5e197c4a-a378-4a96-83a8-6791005e1e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94362013-a952-413c-9c86-4e3120e3d496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34432e28-cda0-4e82-b9ce-2f0786fbac5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94362013-a952-413c-9c86-4e3120e3d496",
                    "LayerId": "8d55a17b-61ae-4afa-94d0-da7e35d2d22a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8d55a17b-61ae-4afa-94d0-da7e35d2d22a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50116910-14b4-492c-b3ff-6b50674b825a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}