{
    "id": "17595f86-e7f6-445a-9ad8-96d749ed24ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60b05ad2-674f-45ee-b197-0f2a52a06aa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17595f86-e7f6-445a-9ad8-96d749ed24ce",
            "compositeImage": {
                "id": "3abeead7-a307-4625-8c71-e029756bd6ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b05ad2-674f-45ee-b197-0f2a52a06aa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f509f366-ccf1-4086-9a21-60eaadb5b22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b05ad2-674f-45ee-b197-0f2a52a06aa1",
                    "LayerId": "c9768691-eae3-431d-8369-91bac41f95c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c9768691-eae3-431d-8369-91bac41f95c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17595f86-e7f6-445a-9ad8-96d749ed24ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}