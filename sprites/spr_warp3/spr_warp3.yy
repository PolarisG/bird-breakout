{
    "id": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warp3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23cc1eed-0621-470d-b4f0-48a41377c0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "042a7b34-d3f6-48df-b86b-53be4d290bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23cc1eed-0621-470d-b4f0-48a41377c0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f2f17b-b970-4086-b775-e277fef00698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23cc1eed-0621-470d-b4f0-48a41377c0eb",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "693cc35d-3a72-4fbb-8588-c0d4150f7675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "c048d512-b3aa-4095-b008-f24d1e5ae0ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693cc35d-3a72-4fbb-8588-c0d4150f7675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643f7246-f621-4541-85f9-774498c6c252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693cc35d-3a72-4fbb-8588-c0d4150f7675",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "786f4dc2-9cc9-4b98-a95f-7f15df5c1463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "bd00d2f7-e8b4-4031-86f1-d098f04d5e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786f4dc2-9cc9-4b98-a95f-7f15df5c1463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a399a3d8-03ac-48cd-ac40-d019620c4e1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786f4dc2-9cc9-4b98-a95f-7f15df5c1463",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "bbc47069-dade-45b8-a266-b40763aa3465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "d0194fc3-4b1e-4278-bd56-a0ca4580a02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc47069-dade-45b8-a266-b40763aa3465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9809bcf-cdc4-4b03-a970-9e1ac19ff1fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc47069-dade-45b8-a266-b40763aa3465",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "359df322-2d66-42d9-aef8-12415fbd1735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "9bf66752-9b94-473c-8a05-a5438b8d079b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "359df322-2d66-42d9-aef8-12415fbd1735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ca80e5-70d7-42e7-8fb5-0b8f0479606e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "359df322-2d66-42d9-aef8-12415fbd1735",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "abaf4a57-09c7-46ba-bda5-1b71b11598f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "98335bca-193e-4f2a-9092-c0bbe8056e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abaf4a57-09c7-46ba-bda5-1b71b11598f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d596a4d-a507-40f0-9d64-8bd5858c9baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abaf4a57-09c7-46ba-bda5-1b71b11598f5",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "5beb3c43-0f39-41ff-85c6-47205b8c0816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "d023a091-51c6-4bbc-871a-f42bce7be424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5beb3c43-0f39-41ff-85c6-47205b8c0816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44004f2-3249-4088-ba86-d04efe1d67b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5beb3c43-0f39-41ff-85c6-47205b8c0816",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "d09c73db-3548-4232-95b7-cc8c9292103b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "19947495-584f-4a39-b8af-180d386eb1fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09c73db-3548-4232-95b7-cc8c9292103b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e01076-87f3-4999-a340-d3d24272c996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09c73db-3548-4232-95b7-cc8c9292103b",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "ba7c1854-da30-413d-87d3-7a0022030cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "49b5957d-da2a-4825-b677-3f6715bc4d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba7c1854-da30-413d-87d3-7a0022030cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8aca06b-f90f-4056-9f7a-c6ba821d173f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba7c1854-da30-413d-87d3-7a0022030cd6",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "07bd96d3-5278-438c-82b1-3e75a965eaa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "e02bf3ce-8fee-42a3-a64d-845c750f60bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07bd96d3-5278-438c-82b1-3e75a965eaa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f114fa4-aea9-4b6e-a408-7c37ca794552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07bd96d3-5278-438c-82b1-3e75a965eaa1",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "3e684c2f-37b0-4622-8b59-fd77f78a264a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "a21bbd5a-82ba-4a9a-ba3d-84647a9757c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e684c2f-37b0-4622-8b59-fd77f78a264a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0dfce2f-3959-4acd-adfb-2f62add0057f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e684c2f-37b0-4622-8b59-fd77f78a264a",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "929def4c-dbff-4d23-ab6e-4a737cf8682a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "8f2161c8-58bf-4202-8482-5d6c5347e624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929def4c-dbff-4d23-ab6e-4a737cf8682a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df68c65-d9c7-43d8-af98-1b21dd28a29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929def4c-dbff-4d23-ab6e-4a737cf8682a",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "dd77b251-a99e-4747-808b-e8bd14e45365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "49e33f73-e102-4245-a1d3-42ce968b8776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd77b251-a99e-4747-808b-e8bd14e45365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05c3173b-2ff9-4355-a24e-ba2502b2835f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd77b251-a99e-4747-808b-e8bd14e45365",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        },
        {
            "id": "a663567e-53c6-476b-8012-dd8909887203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "compositeImage": {
                "id": "a0314d6a-efb5-4edf-a495-bc29d711fecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a663567e-53c6-476b-8012-dd8909887203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c6fa0e-1bc2-4e69-aa2c-97d966a45a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a663567e-53c6-476b-8012-dd8909887203",
                    "LayerId": "8fb90408-93f9-488e-9875-aab383cbc15e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8fb90408-93f9-488e-9875-aab383cbc15e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}