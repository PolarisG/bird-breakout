{
    "id": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wind",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f091d0ee-d7ee-4cfe-b9bf-586d2b49212d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "23b75c4b-8139-4aaf-8e36-d699b69bec6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f091d0ee-d7ee-4cfe-b9bf-586d2b49212d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281d8883-2288-4ac5-96a3-ba29a4c6aa95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f091d0ee-d7ee-4cfe-b9bf-586d2b49212d",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "f0c44b67-7c71-4e30-8e5e-43220713469e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "1378963c-ef97-4d10-9e7a-bb782b1cd556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c44b67-7c71-4e30-8e5e-43220713469e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1712178-1ca2-4fee-ae7b-60355b498557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c44b67-7c71-4e30-8e5e-43220713469e",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "e8d877bf-c923-4cab-a05e-740aee2656c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "35531216-40d8-413b-93a8-ca4f36f04cd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d877bf-c923-4cab-a05e-740aee2656c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d709122e-ad7c-4a01-b3cb-936ac57b6732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d877bf-c923-4cab-a05e-740aee2656c3",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "cb620488-f935-4a7c-8d01-a6e88045815a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "4ccb9fad-5a0e-4d83-a196-c2e9a5b59561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb620488-f935-4a7c-8d01-a6e88045815a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cdfb485-2938-4bff-9ce1-626ebc74bab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb620488-f935-4a7c-8d01-a6e88045815a",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "659379f5-b4a3-405f-a26e-e90ee2920178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "5a1cf8fc-61a8-4f3f-8470-a92daadd02c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659379f5-b4a3-405f-a26e-e90ee2920178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2e4bd3-1cd8-47fc-b9d3-43122e84d4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659379f5-b4a3-405f-a26e-e90ee2920178",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "27310706-e0c1-4e73-b802-07db26179238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "57df5b6e-35a6-4dd7-b59e-3b22cedef986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27310706-e0c1-4e73-b802-07db26179238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c7c3ef-fb41-471a-980e-4027d6ab9533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27310706-e0c1-4e73-b802-07db26179238",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "f458260a-336e-4236-b696-241387ce9741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "8bfac3c2-95dd-4f1c-855f-ba4b58f40807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f458260a-336e-4236-b696-241387ce9741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed0a3030-3867-4556-89f5-b4628cd96c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f458260a-336e-4236-b696-241387ce9741",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "7fa66d62-14e8-4e63-a7a1-bc2954e3c98b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "602d7a25-230f-4644-b652-e39bdfcc292d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa66d62-14e8-4e63-a7a1-bc2954e3c98b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564067d1-2523-4dbe-9448-3b4cbb355454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa66d62-14e8-4e63-a7a1-bc2954e3c98b",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "ad4cf857-ec4a-4302-a4dd-cfe37188e190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "9ba51121-423b-42ba-af5f-25047d54b1d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad4cf857-ec4a-4302-a4dd-cfe37188e190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dad4499-dd8f-4c1b-8d59-7e3b449b5de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad4cf857-ec4a-4302-a4dd-cfe37188e190",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "dffb06f1-f89c-4167-9154-cb44accdad2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "a7b18f66-082f-4d8a-b7ec-782ec3bb6031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dffb06f1-f89c-4167-9154-cb44accdad2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc6d01c-02c3-48e2-a555-a9ad6ab203e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dffb06f1-f89c-4167-9154-cb44accdad2f",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "9198bfc4-715e-47a6-a119-606a627eb8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "bb62683e-3eb8-40d8-953c-354c047b41fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9198bfc4-715e-47a6-a119-606a627eb8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6758dc55-9393-41a8-bd77-5c09a3bae646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9198bfc4-715e-47a6-a119-606a627eb8f7",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "015d96c4-ecfe-4864-b8f5-c8e0baa91622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "05f5cdf6-d9c8-4c55-a4eb-f75e6ec501c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015d96c4-ecfe-4864-b8f5-c8e0baa91622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f39c3b2c-3963-4060-a7f5-0b0f95ad67ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015d96c4-ecfe-4864-b8f5-c8e0baa91622",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "20288e74-1bac-4a77-919e-f981ac6609a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "7fb7962f-0ad4-4215-b516-6af14d7b5e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20288e74-1bac-4a77-919e-f981ac6609a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90bbfd68-4ba0-4951-ba7a-d14c5c657651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20288e74-1bac-4a77-919e-f981ac6609a4",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "16d6c44a-8a73-4b57-8001-326e275f02b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "d9048b87-e620-4a6d-ae78-4ec0e6985753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d6c44a-8a73-4b57-8001-326e275f02b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7266b850-1f51-4a57-a5d5-9c6f0e07a83e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d6c44a-8a73-4b57-8001-326e275f02b2",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "47f23d70-2b37-4921-8248-b0ed9fd5bed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "d577e03e-47d1-4730-aea1-a799475c7820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f23d70-2b37-4921-8248-b0ed9fd5bed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d8551a-588b-4073-8739-861d3716643a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f23d70-2b37-4921-8248-b0ed9fd5bed4",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "ef4cbcd3-55cf-4278-90b3-c3cb6ae8ba3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "23a6740f-c43b-444a-9043-da74bd840cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4cbcd3-55cf-4278-90b3-c3cb6ae8ba3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef40bad1-f8b1-49c5-86a4-1afa58afa718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4cbcd3-55cf-4278-90b3-c3cb6ae8ba3f",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "64ea7173-c92c-4cc5-8351-ffda60e82c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "d75f1b7c-e1c6-49d8-851f-77f041165f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ea7173-c92c-4cc5-8351-ffda60e82c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9da55e-867e-4534-a2b1-ebd92e3f665f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ea7173-c92c-4cc5-8351-ffda60e82c30",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "288a677d-9ff8-4e68-9b8e-8c909cdc63d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "198441f9-8e95-4e62-a9cf-02a140324da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "288a677d-9ff8-4e68-9b8e-8c909cdc63d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d91544a-babe-4de3-bef0-b12d3528a192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "288a677d-9ff8-4e68-9b8e-8c909cdc63d5",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "a4710e11-3766-474d-bbf2-c54aff9a4dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "dda539b4-28c0-4231-bec5-8c0740b7f435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4710e11-3766-474d-bbf2-c54aff9a4dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7ff63d-efec-48ce-8216-87c3b6dda6b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4710e11-3766-474d-bbf2-c54aff9a4dd4",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "c2bf01b7-bb73-4134-812d-87a5bbaaae76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "539c099e-b0d6-4dcc-852c-89243968ba54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bf01b7-bb73-4134-812d-87a5bbaaae76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de690d9-a438-4448-96c0-bec6d7e2799a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bf01b7-bb73-4134-812d-87a5bbaaae76",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "c7d2d31f-adce-42df-bf4c-9ca991ff46ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "02e480f1-f94e-4e08-a86c-d0c21c591b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d2d31f-adce-42df-bf4c-9ca991ff46ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5cc9c21-c3f3-47d7-93f1-64c054bd83c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d2d31f-adce-42df-bf4c-9ca991ff46ad",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "2d44cb20-b454-4eb4-af0f-4dd82e2ffc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "cc16145e-08b7-4134-94b4-d2c34c51c49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d44cb20-b454-4eb4-af0f-4dd82e2ffc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0126f20-42b4-4276-9ffe-f1bfe7fb2af9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d44cb20-b454-4eb4-af0f-4dd82e2ffc08",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "baed0ece-ebb8-4e3e-ae2d-ab9b8631601b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "fb8ca1be-bac6-4f82-8286-d7c0a8a6446c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baed0ece-ebb8-4e3e-ae2d-ab9b8631601b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e852457-e824-42cf-a667-8b3f83e1ee81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baed0ece-ebb8-4e3e-ae2d-ab9b8631601b",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "caf15fdf-d881-451d-92c7-11305d15913b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "71c7dbfd-78e2-4868-858b-07b17f3c4b59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf15fdf-d881-451d-92c7-11305d15913b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd881def-8150-495a-a802-d48571fdf5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf15fdf-d881-451d-92c7-11305d15913b",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "48f3f949-9960-4ebd-ae63-e96b355395d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "131221a8-0e8c-40b7-a304-0a8c678d875d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f3f949-9960-4ebd-ae63-e96b355395d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7f2f52-a2e1-4935-a529-6185062fce8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f3f949-9960-4ebd-ae63-e96b355395d1",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "7d5ba9fa-5dd9-428b-a906-92c79296c9a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "e5d90a3f-5836-472e-b0a4-09f79e28168a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5ba9fa-5dd9-428b-a906-92c79296c9a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9632b1d9-c48b-49cf-8840-04e7b93e6cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5ba9fa-5dd9-428b-a906-92c79296c9a9",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "10cb446a-5833-4275-9fe0-e514496db6bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "3a9713fc-1311-4668-8b4a-f4dd90380707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10cb446a-5833-4275-9fe0-e514496db6bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98932ee8-7ac3-4a31-aaf1-4aacc2c32549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10cb446a-5833-4275-9fe0-e514496db6bd",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "d1419389-4178-41c6-a033-7ea160178c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "b831f68c-335b-4ba9-b3e7-842760045038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1419389-4178-41c6-a033-7ea160178c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da736315-12e5-4f7c-ad66-0cc0e5a5c5bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1419389-4178-41c6-a033-7ea160178c4f",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "8b3dffee-efa4-4728-8174-618b3345f4fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "e1401cb2-f151-4845-b98d-65c3243f4d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3dffee-efa4-4728-8174-618b3345f4fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84890f99-f35b-45c3-a37e-3032bf37de79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3dffee-efa4-4728-8174-618b3345f4fc",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "8b82287f-81f6-4b1e-97c7-fea6ddccea54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "9b4a827e-be97-4193-83d6-0140c7c4faca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b82287f-81f6-4b1e-97c7-fea6ddccea54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a359016a-3a07-4c9c-a6cd-c7e394d875c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b82287f-81f6-4b1e-97c7-fea6ddccea54",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "7fd8d0b1-2a39-4060-b715-40cb96ce54ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "171825f3-8a22-4b92-851e-9d42644b4da8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd8d0b1-2a39-4060-b715-40cb96ce54ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8570cf-2adc-4cfa-b19f-3e9ab07ad61b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd8d0b1-2a39-4060-b715-40cb96ce54ea",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "2a56b692-ba87-4874-99f8-c4d9e532aae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "e9fa4f49-b8d8-4bb6-b8b7-ebaba854f9f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a56b692-ba87-4874-99f8-c4d9e532aae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c79e0feb-2c89-4f49-8488-646a01c163b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a56b692-ba87-4874-99f8-c4d9e532aae7",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "31ac5a7b-ba72-451b-9474-1da9647ebe7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "cf254656-722d-4701-be41-cb9d3122ef51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ac5a7b-ba72-451b-9474-1da9647ebe7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a422913-eb8d-4999-b764-f46d4164275e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ac5a7b-ba72-451b-9474-1da9647ebe7d",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "816a73ea-502d-40b6-93b4-6a85f14a2d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "c5e148e5-be2b-490f-8a4b-df8cdb198946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "816a73ea-502d-40b6-93b4-6a85f14a2d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d5316eb-c2b8-4942-938c-0254eb10447f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816a73ea-502d-40b6-93b4-6a85f14a2d2f",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "6e1dbb0f-200b-4368-8ad0-164f96c01e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "efc618ec-601b-4ed7-b0db-e1e21676679a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e1dbb0f-200b-4368-8ad0-164f96c01e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c90c17-58ef-45df-ae10-0c65f9623728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e1dbb0f-200b-4368-8ad0-164f96c01e06",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        },
        {
            "id": "e39c3817-0a62-46f7-8869-a7291ec02e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "compositeImage": {
                "id": "718d7cb9-9aff-4f95-9ecf-7b93bf0320b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39c3817-0a62-46f7-8869-a7291ec02e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ece1db6-2df1-42fe-935d-2e4c53f3acbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39c3817-0a62-46f7-8869-a7291ec02e23",
                    "LayerId": "ef47d6bb-7774-449a-a2fa-d301356c555b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef47d6bb-7774-449a-a2fa-d301356c555b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3b78384-4e4c-40eb-80c6-76ab431b38de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}