{
    "id": "12c7a1c0-25c6-471c-ac3a-eb17e48aa3a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tilesetshadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1000,
    "bbox_left": 0,
    "bbox_right": 1063,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea27495d-8bef-4b43-8a51-d13c3a8a3ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12c7a1c0-25c6-471c-ac3a-eb17e48aa3a5",
            "compositeImage": {
                "id": "030186a8-4403-48be-b142-273f92777f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea27495d-8bef-4b43-8a51-d13c3a8a3ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b02acccd-3239-4c05-a0c0-5dc2561ae1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea27495d-8bef-4b43-8a51-d13c3a8a3ae8",
                    "LayerId": "0e844a6f-a6b0-4732-bb7c-24ebf0c85143"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 2000,
    "layers": [
        {
            "id": "0e844a6f-a6b0-4732-bb7c-24ebf0c85143",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12c7a1c0-25c6-471c-ac3a-eb17e48aa3a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}