{
    "id": "3bc47cd1-7e26-45e1-8112-047e49f06d83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f78804c4-0707-4591-a40c-827366fe0497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bc47cd1-7e26-45e1-8112-047e49f06d83",
            "compositeImage": {
                "id": "e2f33c1d-6543-498b-b890-532f11a99a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78804c4-0707-4591-a40c-827366fe0497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a333281-6d81-48f8-8490-07735a69f543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78804c4-0707-4591-a40c-827366fe0497",
                    "LayerId": "7408a03a-2ac1-4a28-8f76-8eb2d997f97b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7408a03a-2ac1-4a28-8f76-8eb2d997f97b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bc47cd1-7e26-45e1-8112-047e49f06d83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}