{
    "id": "07fe81dd-8630-4ff4-983f-184b27a393c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_doublespike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3199bbf2-9042-4045-8938-c5798d590d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07fe81dd-8630-4ff4-983f-184b27a393c6",
            "compositeImage": {
                "id": "a5ea38c1-d81f-439e-93c9-a628334ef0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3199bbf2-9042-4045-8938-c5798d590d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46635a36-d67c-4e16-89b5-1aad6304e4cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3199bbf2-9042-4045-8938-c5798d590d5a",
                    "LayerId": "47823d81-e501-46b9-8b15-78d50a5cdd4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47823d81-e501-46b9-8b15-78d50a5cdd4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07fe81dd-8630-4ff4-983f-184b27a393c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}