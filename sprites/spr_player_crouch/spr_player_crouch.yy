{
    "id": "57d1c197-c4b4-4b97-82ee-4b94389561b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30f935b9-47bf-4286-a78d-acfb026d3c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d1c197-c4b4-4b97-82ee-4b94389561b8",
            "compositeImage": {
                "id": "ecd5c637-9616-4f95-af63-897cb273cef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f935b9-47bf-4286-a78d-acfb026d3c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c23c0f-b448-42c3-85a8-e272b6a5b9f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f935b9-47bf-4286-a78d-acfb026d3c96",
                    "LayerId": "36f13087-0bf5-41b2-a3e0-91b6d16e6900"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "36f13087-0bf5-41b2-a3e0-91b6d16e6900",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d1c197-c4b4-4b97-82ee-4b94389561b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}