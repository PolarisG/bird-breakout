{
    "id": "6ec238ce-2353-4f9d-afce-b85c9b63e5d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_doorlarge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "306ed903-77fb-4546-aa48-5a4fc5082bf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ec238ce-2353-4f9d-afce-b85c9b63e5d0",
            "compositeImage": {
                "id": "17a60013-ceb2-4c4a-8ea8-17ddc34f37ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306ed903-77fb-4546-aa48-5a4fc5082bf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee322a18-c9aa-459e-9066-54afbb34ac28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306ed903-77fb-4546-aa48-5a4fc5082bf4",
                    "LayerId": "699e49d6-fe4c-48d4-95d4-c38bad85e027"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 160,
    "layers": [
        {
            "id": "699e49d6-fe4c-48d4-95d4-c38bad85e027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ec238ce-2353-4f9d-afce-b85c9b63e5d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 80
}