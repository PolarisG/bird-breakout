{
    "id": "55395709-3ed4-49b2-9dac-3e853fd4b1d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 13,
    "bbox_right": 29,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba7dcf31-6ddd-4a21-a51f-9c70f1ef1df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55395709-3ed4-49b2-9dac-3e853fd4b1d9",
            "compositeImage": {
                "id": "332e0c7e-1e51-4efb-93c4-3be59dafd324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba7dcf31-6ddd-4a21-a51f-9c70f1ef1df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35302109-daac-4ef8-92d6-b39770dabda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba7dcf31-6ddd-4a21-a51f-9c70f1ef1df5",
                    "LayerId": "48476c70-10b7-4dbf-880c-c647f40d4292"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "48476c70-10b7-4dbf-880c-c647f40d4292",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55395709-3ed4-49b2-9dac-3e853fd4b1d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}