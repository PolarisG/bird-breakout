{
    "id": "10fa6ee8-7e02-498d-b04b-6907c54f06d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_doublejump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 2,
    "bbox_right": 36,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f04d2ace-656e-4f7d-8597-54e440ee0b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10fa6ee8-7e02-498d-b04b-6907c54f06d1",
            "compositeImage": {
                "id": "dc868084-f1b2-4507-9144-7c3dfda86379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f04d2ace-656e-4f7d-8597-54e440ee0b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d204f51-b5e7-4dbd-9c98-43e345df0f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f04d2ace-656e-4f7d-8597-54e440ee0b92",
                    "LayerId": "cf749cc3-767e-4e0b-bf3b-f48f4a6dc4d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "cf749cc3-767e-4e0b-bf3b-f48f4a6dc4d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10fa6ee8-7e02-498d-b04b-6907c54f06d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}