{
    "id": "1c4f35a2-0a28-42df-afec-e74490b68717",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaffb5fe-d106-42d4-bcf5-8da536309de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c4f35a2-0a28-42df-afec-e74490b68717",
            "compositeImage": {
                "id": "42b1a990-b36f-4996-acae-dc0d293c3944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaffb5fe-d106-42d4-bcf5-8da536309de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0f79b3-d51c-43e5-a139-2b0f1c2f14d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaffb5fe-d106-42d4-bcf5-8da536309de9",
                    "LayerId": "8b6eb20f-f12f-4f08-a49b-a30033f8e240"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "8b6eb20f-f12f-4f08-a49b-a30033f8e240",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c4f35a2-0a28-42df-afec-e74490b68717",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 400,
    "yorig": 100
}