{
    "id": "2aab302a-4f92-43d2-b231-6f71e481303c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_doorsmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e26defc-fdbf-4ce6-a927-aed978f0d093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2aab302a-4f92-43d2-b231-6f71e481303c",
            "compositeImage": {
                "id": "f980ed51-bdff-442d-8adc-f53b1ab71464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e26defc-fdbf-4ce6-a927-aed978f0d093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c58a8eaa-5bf9-460a-b8d8-574acc975014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e26defc-fdbf-4ce6-a927-aed978f0d093",
                    "LayerId": "5da2a6c4-bec7-4d92-8fa4-7323c8bdd317"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 96,
    "layers": [
        {
            "id": "5da2a6c4-bec7-4d92-8fa4-7323c8bdd317",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2aab302a-4f92-43d2-b231-6f71e481303c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 48
}