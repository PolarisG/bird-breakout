{
    "id": "67821102-3be3-42e1-9904-d4c0bb0b2989",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a65092a-af67-4b52-8f7d-83a928c76d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67821102-3be3-42e1-9904-d4c0bb0b2989",
            "compositeImage": {
                "id": "7f4d4402-b911-4841-8da1-8cfe6b5cc89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a65092a-af67-4b52-8f7d-83a928c76d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ffb53ed-5fd9-40e9-8455-6e9103614b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a65092a-af67-4b52-8f7d-83a928c76d93",
                    "LayerId": "81fbdf1f-cae6-4dc5-ae67-0d4524dd2c1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "81fbdf1f-cae6-4dc5-ae67-0d4524dd2c1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67821102-3be3-42e1-9904-d4c0bb0b2989",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}