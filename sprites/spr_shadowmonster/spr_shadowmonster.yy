{
    "id": "6f913770-76e3-43de-a8b9-d942436cb9eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadowmonster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 2,
    "bbox_right": 55,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b06851c9-2f93-4cdc-8226-cd8af0f7d5a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f913770-76e3-43de-a8b9-d942436cb9eb",
            "compositeImage": {
                "id": "70c4f5bc-8c10-4b9d-9a31-0e70aa12a364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06851c9-2f93-4cdc-8226-cd8af0f7d5a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5d1616-79d8-4389-9249-5a2c2e20e30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06851c9-2f93-4cdc-8226-cd8af0f7d5a6",
                    "LayerId": "eebede6c-c2bb-491d-b156-29e1b2005963"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eebede6c-c2bb-491d-b156-29e1b2005963",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f913770-76e3-43de-a8b9-d942436cb9eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}