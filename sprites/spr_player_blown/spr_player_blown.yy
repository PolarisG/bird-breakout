{
    "id": "b387541d-a618-4860-9bc2-e24f5dfe9c82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_blown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 4,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1758aa74-c388-4a6b-bc17-1b40281f6ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b387541d-a618-4860-9bc2-e24f5dfe9c82",
            "compositeImage": {
                "id": "5fb868f3-aa3e-48d5-928d-90463cf0ee5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1758aa74-c388-4a6b-bc17-1b40281f6ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1d5d5f-1549-4ab3-a90a-98895bd4cd21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1758aa74-c388-4a6b-bc17-1b40281f6ed8",
                    "LayerId": "bf40dd45-7c64-4a19-8c10-588382faeb57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "bf40dd45-7c64-4a19-8c10-588382faeb57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b387541d-a618-4860-9bc2-e24f5dfe9c82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}