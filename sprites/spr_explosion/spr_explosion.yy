{
    "id": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 33,
    "bbox_right": 56,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45d4e69f-d46e-4b21-acde-108b8b90d52f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "14c475cf-440c-4624-8925-bcd61a44a1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d4e69f-d46e-4b21-acde-108b8b90d52f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eddc80a-7eb0-4569-8ebd-c796e14734a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d4e69f-d46e-4b21-acde-108b8b90d52f",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "68e90193-d3e9-4f5b-803f-3bb453d94824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "72281e19-530b-475d-8c85-5f345d67847e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e90193-d3e9-4f5b-803f-3bb453d94824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362d5c74-acc5-48db-94f2-a28f216079f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e90193-d3e9-4f5b-803f-3bb453d94824",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "0fde3a76-c50b-4e52-bc80-7c6f39ae2ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "7b2f7bdb-0928-4dc2-b49a-84ba01e508d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fde3a76-c50b-4e52-bc80-7c6f39ae2ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5452b8b3-5fee-448e-a163-45a1fc3dfc06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fde3a76-c50b-4e52-bc80-7c6f39ae2ceb",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "50d87153-c3fd-4c81-9d10-47fc8af201ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "567a464c-3484-4974-8226-da685ddda13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d87153-c3fd-4c81-9d10-47fc8af201ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a7a3e5-837c-4fdf-9753-667d6ee37115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d87153-c3fd-4c81-9d10-47fc8af201ab",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "8975e95c-ab92-4c1c-a3a9-befd468ec3e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "12f0eb6d-80ce-45d1-9321-fd4706287186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8975e95c-ab92-4c1c-a3a9-befd468ec3e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64dede1b-508a-4c4f-b0db-531e9d06c3cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8975e95c-ab92-4c1c-a3a9-befd468ec3e4",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "39736b0b-9cbe-484a-bebd-9e37a13ff46a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "ba7c0a26-c297-466e-af73-1ea207f78f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39736b0b-9cbe-484a-bebd-9e37a13ff46a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd2a1cd-48fe-4c97-a8bb-e188f922974f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39736b0b-9cbe-484a-bebd-9e37a13ff46a",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "2fa054ac-26e0-4790-861c-05b2db644cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "37a1dcc3-7dd7-4196-b9f5-51d88287cd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa054ac-26e0-4790-861c-05b2db644cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934f3b4e-b907-4c07-a2e6-a5939b0b8f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa054ac-26e0-4790-861c-05b2db644cb2",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "35860063-8f29-45f3-b379-041897a6fa26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "f8995997-efa1-43bc-8de6-b72b3b7e279e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35860063-8f29-45f3-b379-041897a6fa26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05858828-7b0f-402b-8f99-b80b71b1acbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35860063-8f29-45f3-b379-041897a6fa26",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "d6248aad-784b-4597-aea4-de408218fa25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "e914f147-ba71-41a2-a731-c438b4f4ac8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6248aad-784b-4597-aea4-de408218fa25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b8d816-4d83-4265-85a7-9f1e450502f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6248aad-784b-4597-aea4-de408218fa25",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "5205d301-dae1-40c9-b4ed-2ea1bed56b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "02205d81-a9fc-46fd-9faf-a3df44b95c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5205d301-dae1-40c9-b4ed-2ea1bed56b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c93c4ad-f743-4aab-94c2-7a297260daf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5205d301-dae1-40c9-b4ed-2ea1bed56b1f",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "7d7fdfac-b32c-41ba-abca-df792864d99c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "5144f794-8066-418e-892e-0a7809e0c862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d7fdfac-b32c-41ba-abca-df792864d99c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e034df35-0709-4a94-8c9a-eea2e2e2fe43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d7fdfac-b32c-41ba-abca-df792864d99c",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "59c6de53-f259-424c-bac3-f358aafab0cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "6a336184-8de9-459a-bf50-04060690c97f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c6de53-f259-424c-bac3-f358aafab0cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b525454-234b-433d-bfc0-e9c85d07b75f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c6de53-f259-424c-bac3-f358aafab0cf",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "cc9801a1-f409-4e9b-9ca7-ca8e3d7f54d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "3e00e101-c35e-454b-968b-e2f8a7b15996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9801a1-f409-4e9b-9ca7-ca8e3d7f54d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd185fbe-85dc-448d-be05-72f2158b4d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9801a1-f409-4e9b-9ca7-ca8e3d7f54d0",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        },
        {
            "id": "1e84309b-583e-4905-ab6f-bc1ffb62e998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "compositeImage": {
                "id": "0bf08019-1f73-4d72-b64b-67f9990ec82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e84309b-583e-4905-ab6f-bc1ffb62e998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d8210e-cc31-47b1-8e9b-d2f40da84bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e84309b-583e-4905-ab6f-bc1ffb62e998",
                    "LayerId": "340f724c-0d75-4e75-9a1f-0651c294b9a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "340f724c-0d75-4e75-9a1f-0651c294b9a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "047f34b0-b020-43ed-8a97-4afbc7532cfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 44,
    "yorig": 41
}