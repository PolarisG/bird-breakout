{
    "id": "54867d74-7557-4d3f-b185-4763e2e57ca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_acid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79005611-b5b1-41cf-9a7a-afb579d16850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "82ac6150-f653-4b8b-9eef-6011f9355bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79005611-b5b1-41cf-9a7a-afb579d16850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f189753d-13db-443e-add3-02cb36e58009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79005611-b5b1-41cf-9a7a-afb579d16850",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "5cf9c6ae-d7b7-42fd-bab4-1c2d650f6401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "f0bcc1ca-9c1d-4957-82b2-6f87f7111448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf9c6ae-d7b7-42fd-bab4-1c2d650f6401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706c7abf-e2d2-48d0-a3c8-67c2c0074cb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf9c6ae-d7b7-42fd-bab4-1c2d650f6401",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "9e79a9f7-9f35-4e6c-bc9b-951bd3ebb4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "92888bac-0f8f-41d4-86a5-399ba1f5d241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e79a9f7-9f35-4e6c-bc9b-951bd3ebb4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40dac46a-f1e8-4518-a5e6-3588de835995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e79a9f7-9f35-4e6c-bc9b-951bd3ebb4ad",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "cb4235ca-cca7-44cd-bb41-3da689ef71f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "fbee442f-9ff7-4b57-b884-ac6209a91929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb4235ca-cca7-44cd-bb41-3da689ef71f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46e67f9d-57f3-4aa7-8625-49989e0b39bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb4235ca-cca7-44cd-bb41-3da689ef71f0",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "1d9b9dc5-5425-4fd6-95b5-88e731ee6212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "75c94cdc-a9b7-46f1-94ab-a8e51725610e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d9b9dc5-5425-4fd6-95b5-88e731ee6212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de242b66-68c0-4bc7-8b1b-31aa45fae764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d9b9dc5-5425-4fd6-95b5-88e731ee6212",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "82c6b20c-4e6b-45ce-8488-4d94da2578bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "cc1ab843-3d5b-4417-bea8-a47bc3425971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c6b20c-4e6b-45ce-8488-4d94da2578bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177224b4-8dd6-498f-a108-550bc98e55b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c6b20c-4e6b-45ce-8488-4d94da2578bb",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "b6cee952-53c0-4674-b724-b9ce77ea0c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "3aeb8321-9012-45f9-bd06-067190dcc756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6cee952-53c0-4674-b724-b9ce77ea0c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e12914-cd5a-4be5-bad2-a560ef7037b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6cee952-53c0-4674-b724-b9ce77ea0c1f",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "7419fc9f-6214-475f-b4ba-b855c2926e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "441136e5-3807-4a34-9c41-0de7e030dbcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7419fc9f-6214-475f-b4ba-b855c2926e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a654bc32-9fc1-4ca1-9250-51a597f45557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7419fc9f-6214-475f-b4ba-b855c2926e3f",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "6856e3ea-a68d-47b6-9a23-4fe6d20f96c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "149e9b92-f31f-4028-a7f8-2f3f341d7a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6856e3ea-a68d-47b6-9a23-4fe6d20f96c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b350adfd-40d9-4e57-a983-9d9e939ce835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6856e3ea-a68d-47b6-9a23-4fe6d20f96c2",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "f0cde14b-a4e0-4cfc-b51f-ecea3cb523a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "4ffe2be2-fe3d-490b-a267-d32af18e531e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cde14b-a4e0-4cfc-b51f-ecea3cb523a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "029fcddd-bf12-4306-ae7c-f8a42de8ac32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cde14b-a4e0-4cfc-b51f-ecea3cb523a6",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "64635718-19f6-4658-b993-4d7b637de586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "def47099-7559-4c7e-a93e-6533646b7c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64635718-19f6-4658-b993-4d7b637de586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b9ee5d-fc9d-43d5-a8f9-ead3de14a6e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64635718-19f6-4658-b993-4d7b637de586",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "2b280988-3d6e-451b-8a54-06e2b12a6840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "e89c8ed8-2220-44bb-9cdb-4b4073e35488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b280988-3d6e-451b-8a54-06e2b12a6840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f7b1da-d065-4972-9e3f-2e9fa67bfc87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b280988-3d6e-451b-8a54-06e2b12a6840",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "d1c85474-ebd0-4cc9-b7af-87572c28fbe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "92eb5424-af45-4a55-ab93-2b4891041063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c85474-ebd0-4cc9-b7af-87572c28fbe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be0e925-559e-463d-822b-edd2d5f023df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c85474-ebd0-4cc9-b7af-87572c28fbe9",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "2ea3358d-a35b-451d-ba43-d0b9abe7830f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "65cc5bb2-74ab-4ecd-8bcd-d8cb6f5b5c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea3358d-a35b-451d-ba43-d0b9abe7830f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc569a22-0833-477a-ad9d-29a4f158cb3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea3358d-a35b-451d-ba43-d0b9abe7830f",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "23382561-4540-437a-b028-291c50fb179a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "51e462fa-6d8b-4363-98ce-b0912f7198f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23382561-4540-437a-b028-291c50fb179a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85376e40-9536-41b0-b6e7-7fb39ad52985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23382561-4540-437a-b028-291c50fb179a",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "b30f9b01-a3c3-46e5-9f33-73f92dd012e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "2e224c58-75fe-4c77-8fd9-8794830cfac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30f9b01-a3c3-46e5-9f33-73f92dd012e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8394456f-f976-4f43-9820-d6a2917b6989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30f9b01-a3c3-46e5-9f33-73f92dd012e5",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "4d8f5b02-2b5e-4043-8188-bbe679e0e2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "e97f9bfb-91c3-49cb-8914-c4ee6ba16046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8f5b02-2b5e-4043-8188-bbe679e0e2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2618264-c965-4c3a-b1e0-b4444ef85404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8f5b02-2b5e-4043-8188-bbe679e0e2e3",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "e2d3eef2-5d35-4e25-a327-8d94bc88660d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "9ae9c651-b149-4a97-8d0f-ec986dca1f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d3eef2-5d35-4e25-a327-8d94bc88660d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f19089-261e-4df7-b7c5-d7a95aa83851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d3eef2-5d35-4e25-a327-8d94bc88660d",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "ec50c749-3a38-4972-85c3-a6e0afba081c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "8e9f41c4-ec5a-4c1e-9f75-03b70a8072bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec50c749-3a38-4972-85c3-a6e0afba081c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6a8f6c0-2ddf-452a-85cb-5ad38d1951fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec50c749-3a38-4972-85c3-a6e0afba081c",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "287f3e04-96a4-4914-8d01-e181c263ee41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "57aff402-50dd-474a-a2d3-586cafc374a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287f3e04-96a4-4914-8d01-e181c263ee41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e205e81-1b49-45b0-8a6d-fd2dab79d746",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287f3e04-96a4-4914-8d01-e181c263ee41",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "85e5b28d-88db-4ffc-b1f1-6ef1cf3f8fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "f5ed71ca-e80e-4663-9f7f-0746c6a3d2cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e5b28d-88db-4ffc-b1f1-6ef1cf3f8fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d7182b-6cd7-48f8-9a7f-31377995c829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e5b28d-88db-4ffc-b1f1-6ef1cf3f8fe6",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "f053edad-1627-488b-86e5-a0999ae771d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "feca595e-4be0-404f-bc3e-357c631906e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f053edad-1627-488b-86e5-a0999ae771d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cdf70db-b67e-4500-bad7-534d595ac6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f053edad-1627-488b-86e5-a0999ae771d0",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "4a99cc7b-be5b-4f9b-a333-ef5f6137affd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "377cb939-63ac-47a6-b80c-9c185d5d3511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a99cc7b-be5b-4f9b-a333-ef5f6137affd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc64aec-70e2-4a82-859a-f6026acffb20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a99cc7b-be5b-4f9b-a333-ef5f6137affd",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "5840803a-3291-4718-a5dc-d3707c124a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "7b218a99-6b34-424e-8e46-6557caf49660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5840803a-3291-4718-a5dc-d3707c124a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25aefdc1-7396-41ea-97b6-2be26c9ea1ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5840803a-3291-4718-a5dc-d3707c124a3c",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "317f2ab5-d725-4b39-9ab4-be603d49238c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "6a7d4fea-439a-405f-8436-d3ab43a998f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317f2ab5-d725-4b39-9ab4-be603d49238c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c45a414-b8f8-4d42-9cc4-23dcf58a82c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317f2ab5-d725-4b39-9ab4-be603d49238c",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "15cfddcd-abc7-4cbf-ab81-abc070d55299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "a51f36e6-fd63-459f-b343-588f5dd9b7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15cfddcd-abc7-4cbf-ab81-abc070d55299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09bcbbaa-76fc-4c24-b4e8-433547715c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15cfddcd-abc7-4cbf-ab81-abc070d55299",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "1b82fc7d-525e-49d9-aa2d-f24b9e70e39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "cf0e9145-def0-4d08-ad56-c3d7f9e8111b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b82fc7d-525e-49d9-aa2d-f24b9e70e39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28901a1-d08a-45ac-b3cd-c0e44abe1d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b82fc7d-525e-49d9-aa2d-f24b9e70e39a",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "488cdd11-db57-4022-a47c-f59826e1f581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "9fa98d75-cd5e-46fd-86d1-2de30eefa44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488cdd11-db57-4022-a47c-f59826e1f581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9160bfc9-37c5-4151-99d8-c13368451480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488cdd11-db57-4022-a47c-f59826e1f581",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "494d2c1f-5b9d-4413-b624-806d02ca128c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "697a7b35-891a-428e-93f4-64d3dede20cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494d2c1f-5b9d-4413-b624-806d02ca128c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1497bce-b03e-4e2e-8e7f-1d7305b94f17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494d2c1f-5b9d-4413-b624-806d02ca128c",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "b58b7d9e-8a69-4ba9-8a2a-1c33936d8874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "cb774825-353e-430a-92a5-786f17bc23c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58b7d9e-8a69-4ba9-8a2a-1c33936d8874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afe4de4-54d5-40bd-b992-6f14a82442a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58b7d9e-8a69-4ba9-8a2a-1c33936d8874",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "edf95972-ab35-48a7-9814-b130b34d2fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "cc2a5372-e402-4ade-9a8c-71c433e227e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf95972-ab35-48a7-9814-b130b34d2fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46abbba-28b9-44e3-8df7-a06c5699b2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf95972-ab35-48a7-9814-b130b34d2fb4",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "7f4f34e0-2205-4521-af09-197b8b5d0061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "ba8bc16b-6110-4610-8198-f50a0a618715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4f34e0-2205-4521-af09-197b8b5d0061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ef9107-289f-46fa-8ffc-b8b4ac2bf098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4f34e0-2205-4521-af09-197b8b5d0061",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "3fec5575-331a-49e5-96a4-fe11e74d36d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "4e3b885d-a052-451f-a51b-1508a253b39c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fec5575-331a-49e5-96a4-fe11e74d36d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50e9367e-8126-410d-9e70-85b82ee1b70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fec5575-331a-49e5-96a4-fe11e74d36d3",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "5a38c45c-cbe0-453f-9b0f-e863ae480726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "9cd33822-0490-456e-a207-32c4ad3839c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a38c45c-cbe0-453f-9b0f-e863ae480726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d549c7a-c5df-4f76-8b8a-63391f5f972d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a38c45c-cbe0-453f-9b0f-e863ae480726",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "76530199-a969-43a8-86a7-a709aa922d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "35359483-8991-4bea-a118-276895e485c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76530199-a969-43a8-86a7-a709aa922d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "484ad7d0-ada1-4eda-b453-403ae8c7d144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76530199-a969-43a8-86a7-a709aa922d60",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "d8176899-7d22-4459-9f97-a165e45bc087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "7187ae1a-9f33-4033-886a-8eac0be43c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8176899-7d22-4459-9f97-a165e45bc087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55730c8-e622-485a-8cdf-ff5e4080768d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8176899-7d22-4459-9f97-a165e45bc087",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "00cfc89f-1b2a-47cf-8d7d-969053bfb765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "acea78cf-692a-4683-8ad0-d12eb0331641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00cfc89f-1b2a-47cf-8d7d-969053bfb765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab87ab46-c4db-4432-88d5-f4b6121357a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00cfc89f-1b2a-47cf-8d7d-969053bfb765",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "d2252259-0d72-4834-98a7-e852124e3d7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "8c90fc0a-53fd-4d84-a4a0-3ce518bca3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2252259-0d72-4834-98a7-e852124e3d7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3e37cc-0fc9-4d09-a1d8-336681d30efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2252259-0d72-4834-98a7-e852124e3d7f",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        },
        {
            "id": "c1a0778f-6914-4d0e-b7f8-1a7b6b6c5c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "compositeImage": {
                "id": "7d3ac468-f739-420d-a559-1b2376b02e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a0778f-6914-4d0e-b7f8-1a7b6b6c5c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bc1cc43-fbbc-4f92-96f0-ee78ec8621a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a0778f-6914-4d0e-b7f8-1a7b6b6c5c68",
                    "LayerId": "4e8282da-9c92-48d6-9747-fd26adf69089"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e8282da-9c92-48d6-9747-fd26adf69089",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54867d74-7557-4d3f-b185-4763e2e57ca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}