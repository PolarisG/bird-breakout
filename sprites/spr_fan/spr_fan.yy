{
    "id": "963210d0-53e9-4647-bed2-863767565365",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25e74f38-df9d-40c1-9e49-1f1b134222a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "82f57a78-1ae5-45e9-9f2e-78d5e79a1e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e74f38-df9d-40c1-9e49-1f1b134222a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6d84b2-c09b-42c6-b6a3-b8aa2ec2f407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e74f38-df9d-40c1-9e49-1f1b134222a1",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "694935c6-2e0e-49ca-95ad-36394b01aa6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "b783fc77-fcd4-43cb-b122-657f51a6f2d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "694935c6-2e0e-49ca-95ad-36394b01aa6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0675f246-07eb-4762-9514-f341384175b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "694935c6-2e0e-49ca-95ad-36394b01aa6c",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "21fcedd8-cf16-44a6-b1fc-918f9f4fcb0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "72847369-f8db-4cf0-a634-98f3d7440b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fcedd8-cf16-44a6-b1fc-918f9f4fcb0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc832649-7ecc-4990-b828-ac5a29f10e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fcedd8-cf16-44a6-b1fc-918f9f4fcb0c",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "e5fdf4f3-47e7-40d8-a667-a613450a31aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "7e11167f-f85d-4c02-9459-978f32d0ee37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5fdf4f3-47e7-40d8-a667-a613450a31aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bb2b04-bd08-4ffa-93db-fd15532d8315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5fdf4f3-47e7-40d8-a667-a613450a31aa",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "e6cef87b-17b2-4482-ab54-eee8909c3e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "04f2983b-3c32-4f5d-a4c1-1306cd5aad65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6cef87b-17b2-4482-ab54-eee8909c3e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16dd6963-8d64-4442-971f-a62fee00487d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6cef87b-17b2-4482-ab54-eee8909c3e6c",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "371d303c-66de-43db-bcdd-eba68ca9dae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "3522ded1-0df0-49fb-8192-c74689b5efd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "371d303c-66de-43db-bcdd-eba68ca9dae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d55f46-9861-40ea-a131-9a9221ad8d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "371d303c-66de-43db-bcdd-eba68ca9dae7",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "6dc402d7-b1f2-43a6-9699-8e3f533b9960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "277eb4d9-3c02-48d0-afa7-9543395a344c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc402d7-b1f2-43a6-9699-8e3f533b9960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a5839d-a60c-4f93-b406-aedb7caeb535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc402d7-b1f2-43a6-9699-8e3f533b9960",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "f012f7e6-c203-4037-8880-888df78c26de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "4ef5fc85-ceb0-4ba7-979d-1ceeb47bd8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f012f7e6-c203-4037-8880-888df78c26de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "860bca51-d190-47f0-9b2f-9cf7a9a33fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f012f7e6-c203-4037-8880-888df78c26de",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "af72f876-2a4b-45f1-a945-731e3d588244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "37d9581c-63c1-48c2-8565-ac1101e59a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af72f876-2a4b-45f1-a945-731e3d588244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bddb4a-1a7f-4580-8ed1-edee8c669cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af72f876-2a4b-45f1-a945-731e3d588244",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "d90c7b9c-52b6-4e45-b66b-245a3677ac1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "00dba9b5-958c-48d6-87cb-f5c83ee7cf45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90c7b9c-52b6-4e45-b66b-245a3677ac1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94043cce-4cfb-4fa2-8f82-1ba23d96aa56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90c7b9c-52b6-4e45-b66b-245a3677ac1c",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "e8e5f021-ec23-4805-b183-0eea91103a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "8182ac84-4ae2-4fe8-8509-1166d2b43bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e5f021-ec23-4805-b183-0eea91103a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9693717-fa25-445a-8b4b-f6c3b37b6cb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e5f021-ec23-4805-b183-0eea91103a5a",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        },
        {
            "id": "8418a614-a5f3-4c32-b210-e6360709e174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "compositeImage": {
                "id": "cbb71f1b-2b91-4a34-89cb-de1c34310852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8418a614-a5f3-4c32-b210-e6360709e174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d708ba-739b-4820-b1f7-ea00f0de9de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8418a614-a5f3-4c32-b210-e6360709e174",
                    "LayerId": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68b8ca44-bc7f-49e3-9aa8-5aa9c2274bbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "963210d0-53e9-4647-bed2-863767565365",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}