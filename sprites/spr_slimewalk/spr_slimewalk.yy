{
    "id": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slimewalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 2,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "476d5640-38fd-4fa1-862b-c1af776f5e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
            "compositeImage": {
                "id": "c26c7995-ea46-47a3-a336-9883d08e7e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476d5640-38fd-4fa1-862b-c1af776f5e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532d630b-6473-4e13-99b0-b5841bd68a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476d5640-38fd-4fa1-862b-c1af776f5e1b",
                    "LayerId": "a5adbead-e175-45a1-af72-a7c578934d28"
                }
            ]
        },
        {
            "id": "8d5c845c-8e90-4b33-b46a-73c55317a167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
            "compositeImage": {
                "id": "7004abb8-b1f6-42fa-9dcf-c9eb485caeff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5c845c-8e90-4b33-b46a-73c55317a167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c925f3d9-1c6c-4719-9c97-f2759e7b7eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5c845c-8e90-4b33-b46a-73c55317a167",
                    "LayerId": "a5adbead-e175-45a1-af72-a7c578934d28"
                }
            ]
        },
        {
            "id": "13084433-deed-4b77-a008-4a0ecc5e2425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
            "compositeImage": {
                "id": "e81c94f9-6d56-4978-bc0e-89e6ddcb8c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13084433-deed-4b77-a008-4a0ecc5e2425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7470aa5-f7d4-4cf9-a403-1d4f70e96a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13084433-deed-4b77-a008-4a0ecc5e2425",
                    "LayerId": "a5adbead-e175-45a1-af72-a7c578934d28"
                }
            ]
        },
        {
            "id": "bf225a63-0c48-4b96-81f6-e77e86a20f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
            "compositeImage": {
                "id": "cce9d4a8-b291-4fc4-a40f-0bfe9d25cec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf225a63-0c48-4b96-81f6-e77e86a20f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a7556f-4267-41ee-b38b-efe04ca3d6dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf225a63-0c48-4b96-81f6-e77e86a20f33",
                    "LayerId": "a5adbead-e175-45a1-af72-a7c578934d28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a5adbead-e175-45a1-af72-a7c578934d28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf5aff7-1ef2-4651-aa40-185cc657bd59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}