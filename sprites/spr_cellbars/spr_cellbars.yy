{
    "id": "1a8aff99-dcb4-4354-bdcd-22fd7581eac4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellbars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec60c1b0-e07a-4afc-ac9c-c0b850a474d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8aff99-dcb4-4354-bdcd-22fd7581eac4",
            "compositeImage": {
                "id": "768649cc-5708-4d4f-b8f7-fb76e97dca67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec60c1b0-e07a-4afc-ac9c-c0b850a474d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1743781f-bb56-4b00-95e7-0718999b318a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec60c1b0-e07a-4afc-ac9c-c0b850a474d7",
                    "LayerId": "579a8624-1084-4a4d-afb6-fee9303c3d8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 476,
    "layers": [
        {
            "id": "579a8624-1084-4a4d-afb6-fee9303c3d8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a8aff99-dcb4-4354-bdcd-22fd7581eac4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 896,
    "xorig": 0,
    "yorig": 0
}