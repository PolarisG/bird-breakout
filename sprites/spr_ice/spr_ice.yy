{
    "id": "07edc611-8b88-42db-8f0c-f31600937ef8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15139cae-2972-4bcc-862e-a74e0666b3ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07edc611-8b88-42db-8f0c-f31600937ef8",
            "compositeImage": {
                "id": "4881517c-a455-41a2-acfc-8404d81fa91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15139cae-2972-4bcc-862e-a74e0666b3ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b676faa9-821f-4489-b3f6-2caa3cc82840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15139cae-2972-4bcc-862e-a74e0666b3ae",
                    "LayerId": "97a3a604-79be-4896-b715-af426234e1e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "97a3a604-79be-4896-b715-af426234e1e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07edc611-8b88-42db-8f0c-f31600937ef8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}