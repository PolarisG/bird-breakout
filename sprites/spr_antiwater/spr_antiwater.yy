{
    "id": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_antiwater",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "119468e4-8f61-4b68-8671-7bf25e0c9377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "a0c13383-74b6-4494-9bd0-8d1000120ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "119468e4-8f61-4b68-8671-7bf25e0c9377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "108e8ab2-9d4d-4d55-907c-317dddb1b4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "119468e4-8f61-4b68-8671-7bf25e0c9377",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "a00ca9f0-9f0e-4b51-bef7-a2b082dbb8fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "1e59ca26-0d6f-474c-8a2e-89ea1b21f96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00ca9f0-9f0e-4b51-bef7-a2b082dbb8fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94506339-149f-4efc-8e50-ebba72055b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00ca9f0-9f0e-4b51-bef7-a2b082dbb8fb",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "108bd75b-5f9a-46d2-a6fa-6e9641a7d1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "9b212bc3-3a2d-4b6f-a167-47602106f3ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108bd75b-5f9a-46d2-a6fa-6e9641a7d1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b96619-c6b4-4918-b791-43e6193fe22e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108bd75b-5f9a-46d2-a6fa-6e9641a7d1d8",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "0bbe65b0-c9c6-47db-a5e6-c53e0ee9656c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "e2a1b86b-ee63-4dc5-bcec-b0bd21ea9a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbe65b0-c9c6-47db-a5e6-c53e0ee9656c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54352cc5-eb2e-458b-9651-3d1a396ddac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbe65b0-c9c6-47db-a5e6-c53e0ee9656c",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "9488a5ea-c132-4997-8146-a9e6e0c81b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "cd28453a-e377-421b-8ad9-3d691640c29c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9488a5ea-c132-4997-8146-a9e6e0c81b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52210ba5-4ffe-4c6d-b767-9c22b678953c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9488a5ea-c132-4997-8146-a9e6e0c81b6e",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "38f03819-b8e5-481c-ab8f-6b7a9a1d6951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "87df3f0c-93fb-4c0f-86fa-e780cb6ce478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f03819-b8e5-481c-ab8f-6b7a9a1d6951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fbaed62-486a-4967-ad41-7dcd4ac07d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f03819-b8e5-481c-ab8f-6b7a9a1d6951",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "3e2c19c9-19c1-4a31-a530-be09c14ba7a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "0dcd7b11-b0cd-4016-9f13-c639654020b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2c19c9-19c1-4a31-a530-be09c14ba7a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bf10fb0-422a-4b8e-98c8-c30e199738cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2c19c9-19c1-4a31-a530-be09c14ba7a0",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "3fc10966-5ebb-47f9-8c0d-00034b2e1e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "b2fc102a-b2bd-44a9-b484-6546efa2d7bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc10966-5ebb-47f9-8c0d-00034b2e1e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a643af6-e0db-44e2-ac44-8158255b3f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc10966-5ebb-47f9-8c0d-00034b2e1e79",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "0873a6f6-7257-467e-ac95-967162dbc4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "cd36510d-ee09-46ad-8441-bca1ba99c5cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0873a6f6-7257-467e-ac95-967162dbc4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644af2ae-9795-42a1-b063-946c7ea9f3b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0873a6f6-7257-467e-ac95-967162dbc4bc",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "19fffc8e-a364-40a0-9d46-84cff49ec321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "e4a507b7-7720-4d91-9992-9cdb0350c0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fffc8e-a364-40a0-9d46-84cff49ec321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92066801-acc8-41a2-84cc-b591eb01f0d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fffc8e-a364-40a0-9d46-84cff49ec321",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "bdcd6a16-edcd-468d-8920-c723a1403186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "39e5373b-37d2-4bd7-9da3-7a2c60e20109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdcd6a16-edcd-468d-8920-c723a1403186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd20d3f-de73-4af0-911b-adc0f33f2ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdcd6a16-edcd-468d-8920-c723a1403186",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "5a3982a0-d60a-4ec4-9f11-4d2c7921d25c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "db0b078f-c22d-4536-b4ac-6696edb3d01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3982a0-d60a-4ec4-9f11-4d2c7921d25c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e051bd8-dba3-4893-adbe-9935dd0cfb2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3982a0-d60a-4ec4-9f11-4d2c7921d25c",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "65f9c749-9bde-4e36-b5e6-ecb80b432f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "7b8d2c8c-3f5f-4037-8b79-5a4e111b78f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f9c749-9bde-4e36-b5e6-ecb80b432f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badb21b6-3830-4cec-a4d6-faee2d1666a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f9c749-9bde-4e36-b5e6-ecb80b432f99",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "3d0463e4-8aad-4f89-b4ff-210ae2a9d861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "2903c335-b269-4b67-9137-93ef17a0a321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0463e4-8aad-4f89-b4ff-210ae2a9d861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2f950c-10c8-4f66-8e97-fb5b4d559eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0463e4-8aad-4f89-b4ff-210ae2a9d861",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "2fb83988-c84c-4821-88b7-bd9e94041ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "7cfdf385-df71-40a4-87b5-c2a30ddfab75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb83988-c84c-4821-88b7-bd9e94041ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09172b39-a9a0-467e-b967-f40261082792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb83988-c84c-4821-88b7-bd9e94041ab2",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "1b77cf43-3481-46cc-8095-487a8cbc0377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "5fb8090c-3859-406d-8909-32eb81fd438a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b77cf43-3481-46cc-8095-487a8cbc0377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c69ca8f6-e13b-479d-b797-3ececf4ebca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b77cf43-3481-46cc-8095-487a8cbc0377",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "e2a470ee-f79d-491c-853e-14083dc8e3fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "73e1b9cf-c7dc-4e43-aaff-6a3e5baac2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2a470ee-f79d-491c-853e-14083dc8e3fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed107073-11de-4bfe-a877-670bfffa14f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2a470ee-f79d-491c-853e-14083dc8e3fd",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "1b1debe0-43de-41f4-9fe7-1e972172dfd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "40bf4c1e-2d9f-4c5d-a26f-0ff7a891b2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1debe0-43de-41f4-9fe7-1e972172dfd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e59b8a-ab94-42b9-a6da-bfef122dd2a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1debe0-43de-41f4-9fe7-1e972172dfd3",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "1e859660-7220-4595-a55c-e7cac436f3a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "d2c6dde4-8d7e-43cd-bea1-1e6065a62923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e859660-7220-4595-a55c-e7cac436f3a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c099219a-ea13-414a-b3b3-72ef1cb99863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e859660-7220-4595-a55c-e7cac436f3a9",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "cad8987c-a0da-43fb-bb86-d99c26ba8f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "a3adeed1-9490-40c5-b10d-74038ba493d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad8987c-a0da-43fb-bb86-d99c26ba8f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6e43c3-aeaf-4de4-9c9f-3f87f285ef76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad8987c-a0da-43fb-bb86-d99c26ba8f16",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "22f25e02-39cf-42f9-a367-29e0d401e8d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "69744219-1b0d-4089-91d7-aa63302affd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f25e02-39cf-42f9-a367-29e0d401e8d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc07b904-4e03-4e5f-be6a-2890e48fc052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f25e02-39cf-42f9-a367-29e0d401e8d3",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "efe4edc9-c04d-4079-a0a0-4858e32471b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "0af87257-05de-466c-abed-572844418534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe4edc9-c04d-4079-a0a0-4858e32471b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d7e967-a3a6-483d-b584-05fa58e98433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe4edc9-c04d-4079-a0a0-4858e32471b7",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "47be600c-2637-4afd-a1bb-61c27f0a56d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "27a78a83-e1f6-483a-a264-dbef73a940f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47be600c-2637-4afd-a1bb-61c27f0a56d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d689eace-97b9-43c3-af4d-8361a69e6df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47be600c-2637-4afd-a1bb-61c27f0a56d9",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "01c0eb15-ef84-47dd-a8be-4670362f59c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "c8f2883a-5fe8-49e4-88ae-8c56a03d6095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c0eb15-ef84-47dd-a8be-4670362f59c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe89daba-dc35-4c9e-b647-e6e8903bb128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c0eb15-ef84-47dd-a8be-4670362f59c2",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "22848950-adbb-4a77-aa73-43a0379b3df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "654732e6-0082-4868-b8e1-cb1540632a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22848950-adbb-4a77-aa73-43a0379b3df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68c8999-42d6-470d-ab0d-8cb8097789c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22848950-adbb-4a77-aa73-43a0379b3df9",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "28a34597-bfe0-4568-b2b3-3aa715be8aed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "ee30dafd-9e55-4c6a-bd16-aa191ff7595a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a34597-bfe0-4568-b2b3-3aa715be8aed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76dda3e1-7848-4781-b5a2-d12309269e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a34597-bfe0-4568-b2b3-3aa715be8aed",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "a2fcb7d4-1f65-4ffa-af04-7ba881a5167e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "2c4e8ee3-faf9-423b-ade7-dff1a2476f51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2fcb7d4-1f65-4ffa-af04-7ba881a5167e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d27fc6-db1a-4fe0-aad4-7339a6c01de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2fcb7d4-1f65-4ffa-af04-7ba881a5167e",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "1748e233-4de9-4ace-9704-04ff8b322070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "e0f89348-ca9e-4599-acf7-b6b00d0ac0a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1748e233-4de9-4ace-9704-04ff8b322070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5b45c0-e81f-48c3-a0a5-2b174cd5c900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1748e233-4de9-4ace-9704-04ff8b322070",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "eb50d952-4ab3-45ad-bdbd-55aa3ced6ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "4cd145a1-ffc0-43ad-8124-7d638370feb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb50d952-4ab3-45ad-bdbd-55aa3ced6ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91585103-f977-490d-8eba-ec6ab2e21d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb50d952-4ab3-45ad-bdbd-55aa3ced6ba4",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "260ab59a-d0c4-4111-801e-a7e6c7b8c85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "fb4d9bcf-a3e8-4ce8-9b9e-cc38b685b3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260ab59a-d0c4-4111-801e-a7e6c7b8c85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775f8a09-6407-462a-89ce-99feffa5442b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260ab59a-d0c4-4111-801e-a7e6c7b8c85d",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "d4ec683b-0cfc-4b5b-b349-e6071d4a8674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "2697fd4b-b81d-4d36-8e72-d6111e02545b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ec683b-0cfc-4b5b-b349-e6071d4a8674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed69477-e8d0-4ed6-862c-b61753af1e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ec683b-0cfc-4b5b-b349-e6071d4a8674",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "ae117f0b-0ece-4bd9-93fe-4d14a85216ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "f3e6c812-8281-47f1-ba44-b2fa4e4ef4d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae117f0b-0ece-4bd9-93fe-4d14a85216ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e75a59-7e56-4e0c-9e91-52cb5fdc6e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae117f0b-0ece-4bd9-93fe-4d14a85216ad",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "ea634d3e-1b82-4bb0-9686-6dcc2d4e49f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "da50a3ef-e23d-47c1-a1a0-9ecf04a131e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea634d3e-1b82-4bb0-9686-6dcc2d4e49f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39913c97-b32d-4776-a74c-0c1688755e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea634d3e-1b82-4bb0-9686-6dcc2d4e49f3",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "56294464-9e6e-473f-8d34-67e3d2f8a4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "3f6452e3-35a7-4ced-ab7b-96669360df15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56294464-9e6e-473f-8d34-67e3d2f8a4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d0fd15-f0cc-4bed-8136-f1521a3a858e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56294464-9e6e-473f-8d34-67e3d2f8a4b4",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "c6c7efd0-5c71-4098-a79a-59867bf644e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "f4832040-4ede-462c-b9c9-522b127c9510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c7efd0-5c71-4098-a79a-59867bf644e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2adfb7-102c-4b01-b641-88b298d2f014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c7efd0-5c71-4098-a79a-59867bf644e5",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "79a081ab-2476-4a9c-8b04-f0e7d57c6fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "7c16ab84-2e06-429c-afc8-23bbc0eccb48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a081ab-2476-4a9c-8b04-f0e7d57c6fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c5785b-9ba3-4fdb-91b5-d96d713d1b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a081ab-2476-4a9c-8b04-f0e7d57c6fc6",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "a6c90df6-f894-47c9-bec1-1285d4b36d42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "0f01e0a0-19ab-4db2-8873-162bb00b9352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c90df6-f894-47c9-bec1-1285d4b36d42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a95606b-2260-46a4-8d65-3e4e3bfb667d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c90df6-f894-47c9-bec1-1285d4b36d42",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "4e8052f8-42fc-44f7-a386-7081f01b2936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "d7d10d7b-244b-40a6-9c79-38764eb6fc2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e8052f8-42fc-44f7-a386-7081f01b2936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09f0377-b880-4bd5-9706-3a480a831e6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e8052f8-42fc-44f7-a386-7081f01b2936",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        },
        {
            "id": "e3b5c3f0-8a29-4e3a-8537-d4455683f776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "compositeImage": {
                "id": "f47f0169-b069-4358-a0ed-abf96ff75dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b5c3f0-8a29-4e3a-8537-d4455683f776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "166bfd89-5f61-4a7c-84db-da3cbb50e645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b5c3f0-8a29-4e3a-8537-d4455683f776",
                    "LayerId": "533eafa5-cbb1-488f-ba06-ebfd831f6f31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "533eafa5-cbb1-488f-ba06-ebfd831f6f31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbbbfee9-6d76-4f81-b27c-451fcef5b35f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}