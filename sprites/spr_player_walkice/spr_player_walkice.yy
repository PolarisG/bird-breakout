{
    "id": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walkice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 4,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ed29322-8d79-4152-9227-531b6a9b7efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
            "compositeImage": {
                "id": "ab2d045a-7085-4887-8d11-3b638305ce38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed29322-8d79-4152-9227-531b6a9b7efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5db28d-fe91-43e3-8efc-307a65145f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed29322-8d79-4152-9227-531b6a9b7efc",
                    "LayerId": "a4a54f78-e971-4073-a4bd-f95cf01f2cf5"
                }
            ]
        },
        {
            "id": "d3b21ad5-2609-4fa5-ba9b-dd37c1227685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
            "compositeImage": {
                "id": "7c43824a-6d47-4983-ba52-fbbecf0c0e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b21ad5-2609-4fa5-ba9b-dd37c1227685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597543db-800e-4cd7-9c07-70f1ffb7904f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b21ad5-2609-4fa5-ba9b-dd37c1227685",
                    "LayerId": "a4a54f78-e971-4073-a4bd-f95cf01f2cf5"
                }
            ]
        },
        {
            "id": "f4250b5a-268d-4884-9552-8680681e4c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
            "compositeImage": {
                "id": "04583629-3ec2-4622-ab28-7156ec386b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4250b5a-268d-4884-9552-8680681e4c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74f9f00-2f23-41ff-ba75-3872cdcc1121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4250b5a-268d-4884-9552-8680681e4c12",
                    "LayerId": "a4a54f78-e971-4073-a4bd-f95cf01f2cf5"
                }
            ]
        },
        {
            "id": "03cc4806-ed80-481d-8a0c-7fa8e409b29e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
            "compositeImage": {
                "id": "2c53974b-6d32-4f1b-8a69-48de5f3031d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03cc4806-ed80-481d-8a0c-7fa8e409b29e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "646f19e3-dcd6-4e9d-a75c-c500d010f16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03cc4806-ed80-481d-8a0c-7fa8e409b29e",
                    "LayerId": "a4a54f78-e971-4073-a4bd-f95cf01f2cf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "a4a54f78-e971-4073-a4bd-f95cf01f2cf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89a7f15f-4f22-454b-a7e4-51166bedc2ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}