{
    "id": "77469c77-f827-4bc2-b7f0-bd233c494a05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_1up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "544dc1ce-163b-4aa8-b651-445c61537bf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "6cc366ca-edbe-4938-a297-f90f4edac780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "544dc1ce-163b-4aa8-b651-445c61537bf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4761650-1f5a-4d4d-8b32-e40b9eeae8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "544dc1ce-163b-4aa8-b651-445c61537bf8",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "0ed43d6d-93cc-42cd-8f6a-d7d1347a1a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "3907489b-0415-4de4-8cfc-9f458f388b6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ed43d6d-93cc-42cd-8f6a-d7d1347a1a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b99c2b1-0362-4e6a-be97-71d0c84dae11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ed43d6d-93cc-42cd-8f6a-d7d1347a1a38",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "4cf94af2-062e-4973-a5fc-5df61dae5a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "e9f32faa-0f34-48d5-b76c-e7875fa2c308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf94af2-062e-4973-a5fc-5df61dae5a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566834d8-e129-4bde-9f45-73fd006df13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf94af2-062e-4973-a5fc-5df61dae5a90",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "cd6b3d90-2200-40d7-879b-cd03354bb811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "aa92677b-d77f-46c3-adfc-0273c441f78d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd6b3d90-2200-40d7-879b-cd03354bb811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97634f7-74c4-46ee-a6ad-289414059198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd6b3d90-2200-40d7-879b-cd03354bb811",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "41b01841-6710-420c-9e4a-032c66c84489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "007532fd-540e-4643-8e42-898991a2a96b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41b01841-6710-420c-9e4a-032c66c84489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc9fd46-37c4-4d21-bcec-e159a26d8503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41b01841-6710-420c-9e4a-032c66c84489",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "90e775da-ad40-4000-be77-1cd48bb0e3d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "127d0f53-6fed-43e5-a0a8-d3b362409a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e775da-ad40-4000-be77-1cd48bb0e3d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4151fb9-c3b6-4b61-8b9f-8b2f20278f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e775da-ad40-4000-be77-1cd48bb0e3d7",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "aa52af50-4803-4859-93ef-02c2f9d1eb82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "cd2aff62-a429-4d92-9d24-91834928a882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa52af50-4803-4859-93ef-02c2f9d1eb82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed5e957a-9b90-4b8f-9059-4987180585e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa52af50-4803-4859-93ef-02c2f9d1eb82",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "968368f0-8033-4752-9381-b88e035cf965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "23a4e3ad-adea-467b-a7e9-233fda687e72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968368f0-8033-4752-9381-b88e035cf965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689e54c0-20c3-4278-84b4-51d8d3e8e208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968368f0-8033-4752-9381-b88e035cf965",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "77e480f6-9e96-4ee6-bdf6-6f3ee6cdf7f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "bef35f38-d2eb-4f8d-a15b-78c9c2f8d758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e480f6-9e96-4ee6-bdf6-6f3ee6cdf7f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c6e9a06-1d22-45d4-a614-bacd56a15968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e480f6-9e96-4ee6-bdf6-6f3ee6cdf7f8",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "2b5703ae-6b46-495d-a521-632517b69d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "7bdadfcd-d7c0-4318-baed-e651bcae951f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5703ae-6b46-495d-a521-632517b69d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6869492f-f6c3-40ac-b8fe-0eb57f20e7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5703ae-6b46-495d-a521-632517b69d4c",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "cd855f53-db0a-4e30-9bdc-ba9dbc3ffa24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "1d237fcd-d68d-4efd-a6e5-20819f6f967a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd855f53-db0a-4e30-9bdc-ba9dbc3ffa24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e59b710-db86-4da8-ad7a-fa53d300f59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd855f53-db0a-4e30-9bdc-ba9dbc3ffa24",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "2f8427f0-5fea-4434-9e3d-acf942fcc4c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "7054da7c-29f8-4ce1-9695-6b91365ffc43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f8427f0-5fea-4434-9e3d-acf942fcc4c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5b1a22-49f8-4215-8023-6674dd3a46f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f8427f0-5fea-4434-9e3d-acf942fcc4c0",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "f71b19a5-5e29-4057-8b6d-9086d63a36f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "76421550-3c07-449d-81f8-04accd2764e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71b19a5-5e29-4057-8b6d-9086d63a36f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c908dc0-fbfa-48e1-9dc8-d8ccb74e9386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71b19a5-5e29-4057-8b6d-9086d63a36f3",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "6cea4502-dd59-4aba-aff3-1d7505c383c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "5a76473b-9684-4084-bfbf-b16265518b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cea4502-dd59-4aba-aff3-1d7505c383c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1400ccf9-3497-43d8-a8f1-7a2418b8b5de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cea4502-dd59-4aba-aff3-1d7505c383c8",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "292f59d7-ea13-4a68-ac72-0a07e62f12fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "a9f694b3-c09a-4e2a-bd09-246e718038cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "292f59d7-ea13-4a68-ac72-0a07e62f12fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd18eb4-192a-40ec-932c-a3a072ee8a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "292f59d7-ea13-4a68-ac72-0a07e62f12fa",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "f2c030ec-ef3f-485b-95a6-2f4ee51c57ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "1470bea1-4af0-42b8-9820-bc97c6e2a64a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c030ec-ef3f-485b-95a6-2f4ee51c57ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520d6d9f-5598-4f0b-9803-25e59abb5933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c030ec-ef3f-485b-95a6-2f4ee51c57ed",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "c3c164dd-14b3-4751-a0ce-f303be1131c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "a1210199-d87b-457d-8c49-3103a150ff24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c164dd-14b3-4751-a0ce-f303be1131c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "776c6672-5120-404a-b315-efc813f17da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c164dd-14b3-4751-a0ce-f303be1131c2",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "256882c8-0be1-4657-99d5-4ef2725fe01b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "fef022ca-504a-4f1d-888a-6d6bd697e2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256882c8-0be1-4657-99d5-4ef2725fe01b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4289f9e8-cbe0-498f-94da-34b7ef7d10e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256882c8-0be1-4657-99d5-4ef2725fe01b",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "089f7c9b-c0ff-4381-b7b5-d484c62c7e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "ae7186fc-c896-4805-8d8c-9ecd93bfc26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "089f7c9b-c0ff-4381-b7b5-d484c62c7e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5f717b-a141-4baa-9739-b2a28b3b6eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "089f7c9b-c0ff-4381-b7b5-d484c62c7e5b",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "07be2148-0252-48e0-9ed4-4c6f2e5716a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "a1c5cce9-f12d-4e25-b803-aaa4d392a78e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07be2148-0252-48e0-9ed4-4c6f2e5716a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc588f9b-3db1-4aa3-a321-f97a6143a8ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07be2148-0252-48e0-9ed4-4c6f2e5716a0",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "b06737ed-ec83-45f5-909e-788ed79cd594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "774b21fe-1429-4da1-8580-d0a2bc279590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06737ed-ec83-45f5-909e-788ed79cd594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb8b7d6-a58e-4a79-9340-7e3e99328457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06737ed-ec83-45f5-909e-788ed79cd594",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "f21ebb92-d90d-42ca-943e-a36a421bc16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "0bb2e238-f1c9-4bf0-a728-c882372a1600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f21ebb92-d90d-42ca-943e-a36a421bc16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774107f6-7d58-45cc-9c44-0b2213cb17a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f21ebb92-d90d-42ca-943e-a36a421bc16a",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "92392a99-1c0a-4963-b04b-97540b585b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "34165bfa-d1d6-4167-8430-ed58974175d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92392a99-1c0a-4963-b04b-97540b585b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d3e89c-ef37-4461-87be-46f09573a55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92392a99-1c0a-4963-b04b-97540b585b5c",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "d456a3a0-861e-4e67-b9c2-9333de621a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "775c76a8-7430-4789-ad21-b670583a083d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d456a3a0-861e-4e67-b9c2-9333de621a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec4e361-e4b2-4d89-8d29-6abdc7e26025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d456a3a0-861e-4e67-b9c2-9333de621a27",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "64b80b76-a1f1-4c66-8c82-1f1f0e04fb62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "db64fc4c-cf95-43b9-a6b9-bf6af97ecca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b80b76-a1f1-4c66-8c82-1f1f0e04fb62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb70993-987c-41ef-8d1c-fc1e42084bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b80b76-a1f1-4c66-8c82-1f1f0e04fb62",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "938741a4-caec-4be2-ba82-dd09bce475db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "8c54a6d2-f2d4-4acd-9498-0b871a6451c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "938741a4-caec-4be2-ba82-dd09bce475db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75b9eaf4-58d9-4237-8acd-f9d862e09107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "938741a4-caec-4be2-ba82-dd09bce475db",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "896307f3-9058-48a4-9332-447789805169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "4d38bd7a-6d04-411d-9a7e-9e3feec6d24c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896307f3-9058-48a4-9332-447789805169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d34d0f5-776a-4c20-8785-fde6e08cc978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896307f3-9058-48a4-9332-447789805169",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "021b892b-26ba-4dc6-b900-f165738a76ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "129a9bd2-1182-490f-bf53-d70dd6f833f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021b892b-26ba-4dc6-b900-f165738a76ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a55c8c3e-e2be-4cf1-bb36-50bb6d3c708d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021b892b-26ba-4dc6-b900-f165738a76ff",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "52beab4c-5c3d-4701-8197-ed97b63befd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "9eeb7a77-74e3-4854-99c9-1676b8d69d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52beab4c-5c3d-4701-8197-ed97b63befd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a3d7dd-9560-4c04-ad8c-badb71ef76ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52beab4c-5c3d-4701-8197-ed97b63befd6",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "f8cbe91a-1f79-45f3-a110-2a8cc3ce676a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "501dd008-b3f4-4362-89a0-2164a5bb4dda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8cbe91a-1f79-45f3-a110-2a8cc3ce676a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4837cd-4f65-4f13-bbcf-c3567a4acb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8cbe91a-1f79-45f3-a110-2a8cc3ce676a",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "d0c6c288-c2d2-42b7-9c88-33951c09062b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "ee1309f8-bc07-40da-a90c-22c7a57ce73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c6c288-c2d2-42b7-9c88-33951c09062b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a9074c-df22-4d86-afe1-4a7b2f70cac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c6c288-c2d2-42b7-9c88-33951c09062b",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "b76ede89-de72-4d8d-9438-59ca063a53c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "6719e423-8230-4da2-9ba5-c282fcdc26c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76ede89-de72-4d8d-9438-59ca063a53c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4a2e79-9d4e-4c6c-94ca-93f727d306ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76ede89-de72-4d8d-9438-59ca063a53c8",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "de70a6da-4072-4b57-8d43-8ead7763e13e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "614748f8-7f0e-458b-a229-0faab3fd0891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de70a6da-4072-4b57-8d43-8ead7763e13e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3da1599-b99e-4329-857a-a28f391f1b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de70a6da-4072-4b57-8d43-8ead7763e13e",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "eda17419-dec8-4f94-9e33-8ad2bce1631d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "7f2ffa96-f184-4424-9de6-6f074f783f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda17419-dec8-4f94-9e33-8ad2bce1631d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e47a24f-f584-4122-8fa6-ab1c71ffccf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda17419-dec8-4f94-9e33-8ad2bce1631d",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "e340652b-66bd-4cc4-96f3-714533d1584c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "27e63bf6-0d5c-4c6a-af7a-0a72489c8114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e340652b-66bd-4cc4-96f3-714533d1584c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "203e25fb-f2fe-426c-a01a-1244f7e277b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e340652b-66bd-4cc4-96f3-714533d1584c",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "6c1ed9f6-ddc8-4d1d-9b8e-e59975eeebaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "415c26f0-c180-4276-8ec5-bad4a2ca9e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c1ed9f6-ddc8-4d1d-9b8e-e59975eeebaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb8702f-9643-49d7-8e5b-5c74d93ec3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c1ed9f6-ddc8-4d1d-9b8e-e59975eeebaa",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "70137e5b-d3a7-41d4-b4de-b345b4c89780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "ea98db18-b9ba-424b-874e-762a8911dbcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70137e5b-d3a7-41d4-b4de-b345b4c89780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e28f81d-66df-4298-b6ea-183a4df3d3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70137e5b-d3a7-41d4-b4de-b345b4c89780",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "27e3867d-3ddc-47cb-a1aa-eda6d35fb7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "8b4f8522-cdc6-441b-9b45-29c840186d8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27e3867d-3ddc-47cb-a1aa-eda6d35fb7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1acd95-c514-48ad-aed6-0cba0fb1fd3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27e3867d-3ddc-47cb-a1aa-eda6d35fb7ee",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "085a7909-1904-4ce4-960d-63e9ea29d8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "3f6e49ad-bc62-4fa8-b678-2142fd0cabb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085a7909-1904-4ce4-960d-63e9ea29d8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fc82c9-3710-4937-9108-8223d6bdd6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085a7909-1904-4ce4-960d-63e9ea29d8ad",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "bb6f779d-2f8b-4127-b0ae-9981a3405611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "5391bd27-c64e-49de-b82e-916282a86c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb6f779d-2f8b-4127-b0ae-9981a3405611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b8fa5d-837b-49a1-8b81-fa0a9c028d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb6f779d-2f8b-4127-b0ae-9981a3405611",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "d69b28b1-ca1a-4c68-9e62-fa49f8c3fda4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "662fa099-d4a2-44c3-8dc8-0679a18f82de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d69b28b1-ca1a-4c68-9e62-fa49f8c3fda4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dee6b8f-4a8c-46b8-b51d-d72f9c64c62e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69b28b1-ca1a-4c68-9e62-fa49f8c3fda4",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "6a89762c-72f6-4ab8-9bae-9e53613ad8f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "f1ca653b-7d3d-4ed7-9fbd-03d1fae9b81f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a89762c-72f6-4ab8-9bae-9e53613ad8f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe57d890-d4ab-4d7d-89e7-921049650c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a89762c-72f6-4ab8-9bae-9e53613ad8f7",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "19430254-a0c3-4ab3-a46c-89ee2af8dca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "443f2323-dd99-4aa5-bf62-475867273d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19430254-a0c3-4ab3-a46c-89ee2af8dca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c24bf07a-4333-4dba-ac7e-f745335abf90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19430254-a0c3-4ab3-a46c-89ee2af8dca8",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "4f09e359-5e30-4e37-977b-18d1edd99e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "8ba8e55f-02cf-4593-8601-2094299a6ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f09e359-5e30-4e37-977b-18d1edd99e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15fc3b1e-c708-4e73-af58-1a8d169ec312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f09e359-5e30-4e37-977b-18d1edd99e66",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "d167c992-0736-4f88-811e-2bc365dd76e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "5ea338ea-794c-4637-8c4e-6cb8fecbc985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d167c992-0736-4f88-811e-2bc365dd76e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6936feea-b197-4d59-b6fa-c9eca23fd7b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d167c992-0736-4f88-811e-2bc365dd76e9",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "0c3ddedc-8ca5-41d4-be1a-5e2c92fd2ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "a1572f73-736e-4152-be18-6ced2554b3f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3ddedc-8ca5-41d4-be1a-5e2c92fd2ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc362ec-bf81-4566-876d-dbcbaf2d0bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3ddedc-8ca5-41d4-be1a-5e2c92fd2ead",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "daec6bd5-852a-497d-8c86-c18fc07f990f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "ba9da49f-6b87-4463-9ea8-28139bc3c550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daec6bd5-852a-497d-8c86-c18fc07f990f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea872eb-8559-45a9-956a-62e2d2b68878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daec6bd5-852a-497d-8c86-c18fc07f990f",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        },
        {
            "id": "9ad99ef1-d01c-4c38-a7b3-411c3580059c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "compositeImage": {
                "id": "abd51427-cf23-48de-936d-41ca44dbcbd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad99ef1-d01c-4c38-a7b3-411c3580059c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34132ae2-8001-4579-8076-6cae30aca6a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad99ef1-d01c-4c38-a7b3-411c3580059c",
                    "LayerId": "4554a8b6-531b-4937-9041-e4a44edd3e92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4554a8b6-531b-4937-9041-e4a44edd3e92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}