{
    "id": "e7586574-906c-4029-a206-a2de5636bde7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf3b06d8-f5ac-4124-9ed4-3b20cd119513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7586574-906c-4029-a206-a2de5636bde7",
            "compositeImage": {
                "id": "825d16b7-8983-4faf-a82a-c0f9eed45202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3b06d8-f5ac-4124-9ed4-3b20cd119513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7b45d2-8b53-419a-af1a-119d2c2b3b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3b06d8-f5ac-4124-9ed4-3b20cd119513",
                    "LayerId": "c6d68235-1191-4bd1-af01-b807f444910a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "c6d68235-1191-4bd1-af01-b807f444910a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7586574-906c-4029-a206-a2de5636bde7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 5
}