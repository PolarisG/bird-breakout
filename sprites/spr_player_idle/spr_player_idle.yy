{
    "id": "fa004e7a-2fb5-4833-8480-9b2d2b032bcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ec363d1-ecd9-41e4-b14a-676f06314b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa004e7a-2fb5-4833-8480-9b2d2b032bcc",
            "compositeImage": {
                "id": "ff72b977-14b7-42fc-bb8e-647a087c49b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec363d1-ecd9-41e4-b14a-676f06314b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d2caf8-e7ed-4f62-bfe0-1690853a248d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec363d1-ecd9-41e4-b14a-676f06314b52",
                    "LayerId": "2977e123-5f75-40fb-99c5-5fdfc3a2d6a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2977e123-5f75-40fb-99c5-5fdfc3a2d6a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa004e7a-2fb5-4833-8480-9b2d2b032bcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}