{
    "id": "19cb4edd-9b21-4823-ba17-ed77574e2951",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_air",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1565ca9e-605a-4741-be08-e8a127cd6ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19cb4edd-9b21-4823-ba17-ed77574e2951",
            "compositeImage": {
                "id": "41dfffc9-d09c-4903-be34-b8c4cf422da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1565ca9e-605a-4741-be08-e8a127cd6ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c9aa0c-d1df-453e-b122-a585725a2021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1565ca9e-605a-4741-be08-e8a127cd6ef3",
                    "LayerId": "fbda8d0e-6921-400d-956c-e6ae5faeabf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fbda8d0e-6921-400d-956c-e6ae5faeabf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19cb4edd-9b21-4823-ba17-ed77574e2951",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}