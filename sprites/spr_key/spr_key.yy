{
    "id": "39968b05-4e68-4600-b22f-46e00197afeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 24,
    "bbox_right": 37,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e185f20-9345-40b1-829c-155d47c98548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39968b05-4e68-4600-b22f-46e00197afeb",
            "compositeImage": {
                "id": "719f96fe-88e2-4b75-9857-634d98fbfb82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e185f20-9345-40b1-829c-155d47c98548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf0e89c7-f43c-4085-a1ff-c83edf064a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e185f20-9345-40b1-829c-155d47c98548",
                    "LayerId": "879797cf-9aab-47eb-841d-f580a6abfd05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "879797cf-9aab-47eb-841d-f580a6abfd05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39968b05-4e68-4600-b22f-46e00197afeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}