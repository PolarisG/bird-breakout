{
    "id": "4f9d41de-5300-4ed2-9b99-4a9eebd717c0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_default",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "12bab429-6dc5-4f73-a197-acfd17a09604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a38b6106-7d40-457a-9613-d54c632f5734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 14,
                "y": 82
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a9285932-afee-41c3-89ca-a32749feb874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2175c6e6-a226-42ce-91b8-01bfdfaf28dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7f9cc11c-bebf-4276-be9e-039aa03af8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 212,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "803f05f6-496d-460d-b8e3-747e89914f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 196,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e8d1da00-79e3-4302-a7a9-b48d167c8f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7bbce1a7-3947-4d19-b82f-056ed760aa5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 174,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ec25b81f-7529-495f-8c91-90125a910e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 164,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f5be8aed-696a-444f-8421-16d8f69f091b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cff29575-0516-4c18-ab37-f53a51a4d44f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 22,
                "y": 82
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ab7e59c8-c177-4fc3-beb5-23b1c5fbb0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 140,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dcdf7b4f-f5a8-4911-9ecd-4c0e34a9d45f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 6,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "79ac590c-cc54-49d0-9b31-9a1b8e6ba94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 102,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "51d8d99c-e09f-4632-9bd6-b1ca6c96eea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 96,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "78a5f150-fcbb-4ef6-b4d7-c6442ddd4276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8f895f3e-c400-4626-b13f-6ec591e4ed53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d7d3559b-5648-4408-8e9a-677911b86086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ddaef7f8-e64a-448b-98b3-8eb30116c6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f64faa1e-7e3d-41f4-be66-3dfecf44da44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5f407df5-a654-4871-81bf-9a9c1e078938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "37f3010b-6dd4-43c5-adfd-2d1e6e48a872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 124,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e3b86d27-b9f6-4dab-a3cd-39ec70db3e4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 38,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2e66ab9d-2000-4d23-8028-9a815ba9fd7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 54,
                "y": 82
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "35530fa5-b30c-4863-baf3-b12d46643547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 70,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "599f1bc3-155a-46de-93f8-c2cbbdd1d8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 136,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6938b512-0df2-4267-a316-4fcb924e43fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 130,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "95027d19-92cd-4735-a706-1b68be4f7451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 6,
                "x": 122,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7a5f13c8-1819-42a3-a691-1d3edb98985a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 110,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bfe3e29f-bffb-4040-b3d4-71a77abe5e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 94,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5f1ead78-0cc5-44c9-bef4-f35da71adc9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 82,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a5be64fe-2dd8-4ec3-828d-9ace98fc396c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 102
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fe699b45-4123-4023-a494-9d4f6be5c9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "838282a8-0a87-40dc-a4ee-9086735316fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 102
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "82f5b83c-8e1a-41eb-bdd7-811777be0016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 102
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "19ce1e8b-2511-4fb1-8666-eba448717588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "132de707-1475-4f6f-a838-63168a9809b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7d6b5daa-f285-4aca-ab1c-e7f319a74c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b1925697-8a23-4967-a4fd-e5731a00542f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 82
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "08588eae-ec01-47cf-9c14-ebca69ad2245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "68c4c1ee-c82b-4905-a023-9a926f647307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 82
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c8bb9d06-8902-496a-820d-ccb215e84018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 148,
                "y": 82
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "feeb7606-1717-4b1f-bb7e-094d84eadc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 82
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dbe98119-8595-455d-b9d9-7b9222767482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 116,
                "y": 82
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b2ee6e48-36c2-4676-b77c-57956ea9f445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 102,
                "y": 82
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "69f5f77a-ac2a-4fd5-b4f0-af4710f2e77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 82
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "12b25d1b-accc-40cf-a076-c96b29bc01c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ea3f88ec-9e2e-436c-b339-f29a08ec0e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 152,
                "y": 102
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1c04c8ce-a570-443e-bab2-2b1e9d04211f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 212,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a11f1a4e-9d26-4999-853b-a1490e3eb781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 42
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "169bbbc2-cdde-42a0-acae-8413df0d4d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 48,
                "y": 22
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3f8fa6cc-f1fc-4132-aca7-8c9dbe922a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 32,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bb0460d5-219d-4a1e-8b43-ed92b6b96926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3baf3da1-885e-4fb2-9955-3b65f6b5fea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b0dc4932-4c5f-436a-b42e-4ce41e79c84c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c238ba0b-8a2b-459e-be77-361a26a46c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a70d6cfb-4288-4090-aa17-f510817b8e47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7609c150-1955-45dd-9535-4e6a949aa748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "eadc9344-a9ba-416a-bde9-feeae3b85b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e0bd9a0c-0db1-4700-8c1c-a762f1c3b12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2c661597-da6a-4fdb-91ac-f83520a54e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3d87faec-ea52-4a44-86d6-16184526d025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d2942d48-eae0-48b7-b2ce-afc0eadfadf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a2f21a23-98d6-42ae-8e87-a4f6ff1f2b21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "48a92650-62c7-4415-afbc-bf602cd1b04b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "78b9f6a9-9575-47e9-a05c-efaa2c94b00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "249ee174-4480-4b35-9ba7-ec45fc03a4a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "aaa4ace3-7ebc-4e79-85a2-95dea29b42f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e10ff170-c617-43f8-8137-d9989ecd9b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1dd1217d-0bdd-47e5-99cb-2bbccad0e9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d8ca54b7-24c4-455e-96cd-5c4a6b13fbdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b7c9cab0-7283-4b1e-b996-610354abd3ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7179cec6-0ab2-4de0-8b43-ffbb1e0c3211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 90,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "807988a2-49d2-49d4-a929-8859cd7d72f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4879d2d6-9e5c-4b80-92f4-45876d559791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 172,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4ddede11-cb5b-4054-9124-5c46b5fd22db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 156,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "93566a3f-7cf4-4342-bdcc-ae14675f8386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 142,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "95a46e8e-be4f-4966-91ce-50ce4fe1abdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 126,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d3f581aa-5d02-4b19-be0b-eef17820b3d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 110,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "52774415-76fc-4a89-8f3f-94c027eed1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 94,
                "y": 42
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9abdd697-2bb9-4f45-b6c9-b301b814ff5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 78,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8b0d33f9-e18e-4058-b7f3-817d4ff7f3c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 62,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c2a44011-2df1-4fd7-98c5-f0422376369a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 48,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1a78da9d-0094-45c9-9a2a-bea08916545e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 32,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9c545c9d-637b-4bdb-9545-04d9dd3a0d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a83c52be-6895-45f6-9739-865398fe42cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3cdb1a85-3d98-4581-ac90-cbd3a2e56f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 226,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "77d3bb8f-7ea7-440d-a411-f7dc755b5c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e5a035bc-8461-43bd-8363-ee94e814ba9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "227de4a8-606d-4ec7-9500-1ad64519787c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1c649f53-89d2-4ef2-b29c-322de865a4db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "33b45ec3-2531-4da2-97af-20c294dcf1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "eb33f870-6467-4b1c-835e-0097cf31fb97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 146,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "72ef3dae-570a-49c4-b1f1-321f7ecfd326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 136,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9ef5f335-08a4-4409-8c74-eb2fab320d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "a1023a64-e3fe-48dd-878d-73ed5edca991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 18,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b730e4dd-e6c2-42a8-9f8b-07fc322f4780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 168,
                "y": 102
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}