{
    "id": "4f9d41de-5300-4ed2-9b99-4a9eebd717c0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_default",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Press Start 2P",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "96f303dd-75ea-43d3-aa69-c0f0ddb77dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "11de290a-6082-4d13-8d02-ed8185460b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 8,
                "x": 242,
                "y": 106
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0a3a6a70-34b7-4a09-820b-5658d3f21439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 227,
                "y": 106
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4cc26eb7-1f4e-45ce-b5f2-6bc06e80d9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 207,
                "y": 106
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "07543c70-fa90-46a9-ab20-223050284feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 187,
                "y": 106
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2ea3bb97-33f5-435b-8776-d553a705e735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 167,
                "y": 106
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7d4e472a-6d07-49b6-9cb0-291f44ca734c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 147,
                "y": 106
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "460ff8c5-5f7a-4851-ad03-7e986311e46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 6,
                "x": 139,
                "y": 106
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f3fa026b-ab37-4f4f-bdc4-fd59941b6a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 126,
                "y": 106
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "60dd1448-a8d1-4593-b544-a94f863cef2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 10,
                "x": 114,
                "y": 106
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f246fadf-136e-427f-8202-a8e3d8741ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9d6c9bfb-8793-47a9-adff-0607fb5b9211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 97,
                "y": 106
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b606b97f-c1fa-4ba3-9040-d05102ca004e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 8,
                "x": 67,
                "y": 106
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eb0e8306-c1e6-4d2c-8a9f-e1b8b17ac8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 50,
                "y": 106
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ae4c41a6-b9c3-46e7-bf8d-1614f6462b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 6,
                "x": 42,
                "y": 106
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "54c320f0-b5b5-4a0a-b233-c63b09bb5689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 106
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "69170761-adf9-419d-8170-a91abbbc3c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b933f55c-8f74-4e5d-80c8-d2a0114bce39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 229,
                "y": 80
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1d0e707e-824e-4f2a-873f-8054c15c88f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 209,
                "y": 80
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1de60827-5f2a-4cc1-a7f5-b7ac271b6c78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 189,
                "y": 80
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5005903d-fb56-4462-a551-29d0ab6bc19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 169,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5e77d16f-1c59-4f9f-a181-0bb29ae87c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 77,
                "y": 106
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7ed0cba7-ef95-4d95-a238-94d876018bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 132
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a6168df1-c02e-4932-ab72-9a48bc259e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 132
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a3467099-f294-45a1-aa0b-07ca7b6a97fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 132
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1783abfd-73a1-43fb-a300-dd8c37f9257d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 210,
                "y": 158
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6a3506e2-3b43-4d14-bb44-6ddf652e5925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 6,
                "x": 202,
                "y": 158
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "548aaf90-1820-4704-925d-48e22ff6b58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 8,
                "x": 192,
                "y": 158
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "72193642-783c-4ecf-a4c0-d8ae29bfcc6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 177,
                "y": 158
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a76be33e-2fd8-474a-b667-49f5bdb9072d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 157,
                "y": 158
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "48c02698-fece-4b83-885c-caae18be08ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 142,
                "y": 158
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "adf1d9e6-ed5c-40e1-b86b-0b7f2ca1f379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 158
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8efe21b8-d864-4979-a419-6c993fc2ba1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 158
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "010bfe4a-6d87-4a3d-bf20-c12f6d5b8f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 158
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "743b146f-fe30-40c6-abcd-c4a97e8052f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 158
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1c54b7a1-e5d3-4589-b9be-301c9ebaba61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 158
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c15ae1f5-7f2c-48d8-8abc-2201da9e8c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 158
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e3b7cab8-9bd5-4e25-bd94-0f12cf8e1bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d60d684c-cf98-4078-ac6b-03c51d397dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 216,
                "y": 132
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4cb3d19b-fb98-42a3-8bde-693cd46f9587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 196,
                "y": 132
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "857f1938-6570-4815-a4fe-82571e9d743d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 176,
                "y": 132
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d6817237-f263-4736-baa6-8dee88688b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 159,
                "y": 132
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d792e257-d417-4014-ae0d-d7fe2c591ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 139,
                "y": 132
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "03edc64e-84e4-48c8-aed7-d090da4fa7bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 119,
                "y": 132
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3063e497-69e8-4cb2-9f64-9c0845ff2487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 102,
                "y": 132
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "32a1d4ca-137b-4b49-a432-2b8700c71c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 132
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "98c970c3-11e1-466a-8381-04079d31bdfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 149,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6b3358f9-9de4-4afa-8b77-c33f64c5d7d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 230,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "89eea9ae-1890-4917-b882-cd0893ed41c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 129,
                "y": 80
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "883b31b1-35ad-485d-a395-8016c9864745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 94,
                "y": 80
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "27449503-2d24-4160-8f2a-1798ff8bc02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 119,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dadf7235-2377-40bc-87ce-068ce4bc4ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 99,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f203be73-236b-4db2-962f-70f8a8af9da8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 82,
                "y": 28
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f87ace18-e8a4-4261-8ad2-cbe680cd7121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 28
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "629ca6fd-bfaa-4fac-95b1-a6783eb265e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 28
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b76da541-2cda-49df-b4d2-5850732153a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 28
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1d5a94b9-ed4a-4eac-a2b0-818e074eb770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "39b755d7-df30-426a-807f-9218d93a9a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "09c3ca7d-39a6-4c90-ad85-915bad94e1a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9e73aac8-2c04-4383-a957-a23060609741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 139,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "917e35a6-e7fc-41c3-a5f0-6352c3a5bbd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6ef4858d-a802-46ca-a235-22770ae78066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 10,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d88eb4f9-0ebf-4b2a-ad8d-dd1664a22105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c29ae0c5-5379-44e5-93ec-1fc1e1e8af63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6a46ebbf-e8a0-418c-9898-151466f17050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 8,
                "shift": 21,
                "w": 5,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2ace9ea6-daaa-40c5-b3b8-4398bdc21fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a09be0c4-478d-49f6-871c-427469376558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c3d3073f-a73e-4af1-a81a-4eb9164ec744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "bc29a2c5-aca6-42b5-a6f5-8465cc0e54a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "96d3338e-2f03-48ce-beb1-01ac9720e04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3a7ba678-2cc3-4ebe-acdc-3cdff6a29970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d03e33ca-76fd-4e1c-a02c-c7aad0a44d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 152,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f6e00654-bae4-47a6-b8ec-c72af2070150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 172,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7dce3bb0-bad9-4b5c-9b44-36ac41099322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 192,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "46a7ef23-a17b-4798-8227-7dae3ff51c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 13,
                "x": 79,
                "y": 80
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b07c8cf2-ec65-4b37-9db9-4b05bceb4bfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 59,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f906b691-f3ce-4874-929d-119e1f40a4d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 42,
                "y": 80
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "15ccf842-363f-4720-9784-97e3ff98e36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 80
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6ba68623-627e-4536-aac4-9ff50b39c0a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f8f2b89d-738e-4cd7-9e6e-a59828e3af64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 226,
                "y": 54
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0e8c1cde-9cbc-4173-a993-0bcbbf2f4ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 206,
                "y": 54
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "801d9c3d-669a-438f-81b3-4775458102b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 186,
                "y": 54
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eda53edb-e979-4038-aca8-74e0a91d4f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 169,
                "y": 54
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "cc1b83cd-281f-493d-94bc-975d7e5a4adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 149,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8ee19680-1c41-4a20-b071-2791a2c694a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 132,
                "y": 54
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "17f671be-3405-43a1-8d9d-3feb9c998f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 112,
                "y": 54
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9ebc6dd9-7a6f-4fdf-ba62-3d7a59698e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 95,
                "y": 54
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "06f6222c-80c9-41ad-9180-f655d1e2e17b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 75,
                "y": 54
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1eaeb6d3-fd50-49d0-b3c4-9b9ff1ae04d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 55,
                "y": 54
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9967d307-0954-46d4-ba55-bf4b26da4125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 35,
                "y": 54
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6c3c80ed-1719-4704-a1cb-073e9b7dda57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 15,
                "y": 54
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c3c711ec-faa1-4ed2-bbb1-6e9c7105b3ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c1713e7f-7716-4b8a-a520-0c6d023f6ae3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 8,
                "shift": 21,
                "w": 5,
                "x": 241,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "050de62e-9ce4-4694-ac02-fd1d00d0bfa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 3,
                "shift": 21,
                "w": 10,
                "x": 229,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8a6422bc-1c34-46b5-b462-5739597efb5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 209,
                "y": 28
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "70bfae0d-35ef-49c9-84c8-0174be0e9189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 24,
                "offset": 2,
                "shift": 21,
                "w": 13,
                "x": 114,
                "y": 80
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1f39be42-3b1f-473b-89a1-bedd02f57b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 2,
                "y": 184
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}