{
    "id": "1bdce21c-fb55-4c02-ab7b-15849f773892",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_runes",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bird",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "42778781-c410-47ab-a01a-3b4fb900e55b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "78d7509b-d68b-430e-9434-a84d86352517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 102,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "35dba24a-5664-4c69-ae83-5a8b1c2ba364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 91,
                "y": 114
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b5e13d83-5fa3-474c-806e-00faced1829f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 75,
                "y": 114
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "06d6090c-e6d2-491b-be82-87be14a42c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 62,
                "y": 114
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3546e22c-9f3d-456f-9672-764c090b3398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 45,
                "y": 114
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "46966d00-4257-48e0-84b1-5ec64231b3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 28,
                "y": 114
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "aa2e7aac-d828-411e-b6f9-e91ca3b444a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 22,
                "y": 114
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f9e61377-0960-4040-8ad6-049ee3aac65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 12,
                "y": 114
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "39bf3f0d-8dff-4dee-828e-d2d6b6df7c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c9680bf2-d238-497e-ad98-89f2ef3c03fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 123,
                "y": 114
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a09a909d-9e53-4bc3-af7d-b012a8b58260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 234,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "24c85bef-24d5-4224-b90a-a65392c702e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 204,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "36ba2092-247c-41ad-8964-15320e17dc11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 193,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fb00cf2e-9590-43ab-8f95-7b6ce26cc9b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 172,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9daa2d4-8344-4de8-82d5-d1be58b0a809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 160,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4be60b72-3944-4c80-9d11-24e9b210d803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 146,
                "y": 86
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8fde1b25-e799-454e-81fe-74c4962a3c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 137,
                "y": 86
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e9ebb2ff-e629-4ce2-909e-a4262f6af724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 123,
                "y": 86
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1b0c8644-e961-4448-a258-364005aae46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 109,
                "y": 86
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d2161d61-2a69-4301-ba6f-77db47d13920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 93,
                "y": 86
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d9179c4e-0d84-4a6c-85ba-3d2e7fe394db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 221,
                "y": 86
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a19f042b-45dd-492a-8eed-48ebadeafbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 137,
                "y": 114
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6d8307db-3d65-41f6-823f-7b862fd4bbe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 151,
                "y": 114
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b3d8a1e3-742e-451f-97aa-8185069afb96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 165,
                "y": 114
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e8116f5b-35c4-4afd-b999-bff34af30896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 43,
                "y": 170
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8513a8a6-9d99-4f4b-9250-e3992adf795a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 37,
                "y": 170
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2f7f60bd-a94c-4321-904f-a45a46b296f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 30,
                "y": 170
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0521bcec-0b4c-4017-b4d1-41c078855e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 170
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0633196d-393f-46dd-9800-4821f0ac7e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2a0970c0-cc56-4e51-b397-2e827f58ba7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 228,
                "y": 142
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "aa3d1c29-a7ac-42df-9e57-3f51c4789504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 207,
                "y": 142
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e7079b74-1ac4-48f0-b28d-d0c57cb052ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 190,
                "y": 142
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5ec62d16-d2d3-46ce-a505-3c06b9540f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 167,
                "y": 142
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c9672b33-acf9-4d2b-98cc-46a366141070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 148,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "09f0b544-61a4-46d7-aa6a-d3384a6689d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 129,
                "y": 142
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "88425657-7a78-4125-b8e8-829604b404f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 107,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bdb32666-b6b9-439d-930d-3dae18b091fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 90,
                "y": 142
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7fc95679-e398-42de-b310-14a13e5636ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 69,
                "y": 142
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f3ed9cf8-5b97-4a22-812d-11fe10e26d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 6,
                "shift": 20,
                "w": 12,
                "x": 55,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3313d475-236f-4d62-a29e-8c7430d27ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 39,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ad4ba770-8fd0-4f29-b4b8-95781782b9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 23,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0439c5c5-7ba8-49e3-818d-5627a91ab541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c217d66b-7de5-4ccf-88e9-39eea3a3d03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 221,
                "y": 114
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c33667a5-48b9-4b16-95be-8cc15d57f857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 200,
                "y": 114
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "49502d46-444f-4288-91f8-67bb90759166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 179,
                "y": 114
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fabc1f61-2f18-49d9-aeba-07232fc9593a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 76,
                "y": 86
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2543feda-8783-4fa6-a10f-be8d963e2c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 58,
                "y": 86
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d4290059-c2a5-476e-8fb2-061eca7f086b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 40,
                "y": 86
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d54893f7-d32d-4b5c-9b64-f59f6d3c7d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 120,
                "y": 30
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf90a2a4-4f0e-4da5-9df6-a9104f62d9f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "17e00f2f-317f-436d-82e8-2ad31634e5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 75,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ee6bfdbe-4fcf-4b0b-bc09-921e5d9ea5b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 62,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c920f39c-e771-4599-9db3-dba9d88c56a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 41,
                "y": 30
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5c67a62a-be05-4af1-9ace-512fcd55faf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 24,
                "y": 30
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "727a05c5-2c3d-4f3d-8042-024683d86dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "427fb38f-8eed-474e-9fea-b9d75fce9750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ce575d53-4f29-45b1-bc30-89440ae0af10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "58e45599-c55b-4674-a00f-5c424ec9edb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "aff48661-9c4d-4944-9eb0-0b735f9ed0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 4,
                "shift": 14,
                "w": 8,
                "x": 110,
                "y": 30
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "56c4725b-4a26-4528-9b03-5ae8c55f4369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cb039f10-597e-466f-abea-499995acc4b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "52d52c63-5784-467e-a9e6-94f9e5febb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2bdf247b-6ade-42d7-bf40-10c2362a5d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ebe6b93e-9bd3-4ea9-9107-4c2bd50b0d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fd12479a-d846-4c14-a0fa-f0f3b9b04d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "981b3823-24c6-4280-a7ea-1820c1d715c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "901889fc-4649-42b7-89e8-164ab252fe43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "876245ab-fd67-473c-b264-0f3f6bd9a172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "47aa8192-b0a5-4b08-adc6-0a0d5de308fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ceaee494-97df-4ebd-b584-b129f94b91dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e5f5bdc7-c0d0-4c36-93b1-f75b58681998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 6,
                "shift": 20,
                "w": 12,
                "x": 138,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1836d5f0-14ba-4f69-a1e5-9583c69d8241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 62,
                "y": 58
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5c826d77-5788-4fed-ae46-7c2294eafe2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 152,
                "y": 30
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2b33ec22-12de-412a-89c8-3b3a622c913d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "58e8b2b4-9305-48b6-9d7e-57bd5913988f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 222,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2d537c1d-24f8-44e6-9632-a1c839692102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 201,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "79383240-205e-410d-bb52-9e686c01b2b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 180,
                "y": 58
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dd5e37f4-3721-44b9-978e-a575e92472a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 163,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c5cb5a34-d17a-458c-be5d-54e35aee62a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 145,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3d1b1d68-70da-4e26-bbb6-64fef50ebb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 127,
                "y": 58
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2a52b812-e024-404a-a8db-aac248b0d5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 109,
                "y": 58
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "deb42d3b-5910-4c44-84ae-25519b73e198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 91,
                "y": 58
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "761811fe-0cb0-45c8-b168-bda716974144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 23,
                "y": 86
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6e308a4f-f6d5-4e1a-9873-e42d43962b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 78,
                "y": 58
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "24fc9daf-8cf3-4420-a15d-56b8826ccae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 41,
                "y": 58
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d4e69439-72e2-4342-9914-fa04fb39bf6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 24,
                "y": 58
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7f32f4b9-36ce-4fbb-9519-dfd3c8a03d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "489df4f5-9008-48ad-a1f7-b45b8a4fbe62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 235,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "25be7400-95ab-4aa0-bf41-5f54eeff9620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 215,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c6f89187-44e7-4a42-8613-c9807cd24b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 197,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0cd214b7-8f62-44cc-9c5c-20b610ae09b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 185,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d6a45863-608c-4e1c-9674-b3ba15fb1f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 180,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9cdb8a4f-ade5-4b63-819c-0aed5c9cd58d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bba43454-b152-4a22-8257-1d7a654b6129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 57,
                "y": 170
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "be32e392-859e-4737-bb35-f0c1d1138391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 26,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 71,
                "y": 170
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000d\\u000a0123456789 .,<>\"'&!?\\u000d\\u000athe quick brown fox jumps over the lazy dog\\u000d\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000d\\u000aDefault character: ▯ (9647)\\u000d\\u000aBirdRunes",
    "size": 18,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}