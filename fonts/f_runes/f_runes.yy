{
    "id": "1bdce21c-fb55-4c02-ab7b-15849f773892",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_runes",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bird",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e9a13965-b73f-4777-ba8a-7ed57d34163a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "50afb8c9-28ae-4275-b383-4718b22cb1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 366,
                "y": 76
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bd7f5930-28ec-4903-b539-d21d9c223baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 353,
                "y": 76
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "de7fb26b-57f5-4ae5-8705-e144593c0d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 332,
                "y": 76
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6f88e8ac-42db-4dab-b69a-82034e3a8726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 315,
                "y": 76
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "810c0e34-19c8-44ba-bd7c-9e5b56ef78ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 293,
                "y": 76
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ec11b710-98cb-4be5-8c0b-d44b7e1c5448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 271,
                "y": 76
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "364b198e-25bb-4bf6-8651-d2975a81a752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 264,
                "y": 76
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d6d65999-4cc1-4618-9ddf-5f9acd141be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 251,
                "y": 76
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e669fe84-5871-4eed-9c28-c9ae25ab05ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 238,
                "y": 76
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c0ca7df6-eb10-421c-bda3-4ac8fea02037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 392,
                "y": 76
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b99e2bd8-d063-41e4-b4cb-f146dea7f944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 221,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6ef7a314-65b3-4fff-8aeb-665cfcf76e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 182,
                "y": 76
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "943737e1-b04f-4cbf-a295-ff8b03ae2ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 169,
                "y": 76
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "992a9c99-8d09-4a13-b466-913aff7e24b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 143,
                "y": 76
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "42b61a14-a303-4a02-8f09-7b800a9aaccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 128,
                "y": 76
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0f452937-bdf1-485d-87f5-be7a296f8f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 111,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3eff979e-61a3-40be-8fe0-051b6019cc68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 9,
                "x": 100,
                "y": 76
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "72ccb692-e38d-43aa-9ec3-bab9eac08d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 83,
                "y": 76
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1102f5e5-5593-414d-b4e2-4b69d4a63412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 66,
                "y": 76
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b5b6dd3c-353f-43c7-81e4-87cd1c86b25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 46,
                "y": 76
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "eeb0d526-a476-4791-92a3-60fc2256b7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 204,
                "y": 76
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6eac8795-7736-4e85-87f8-fbde56afbe6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 410,
                "y": 76
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9edfd80b-d082-48fe-a3cb-c5222a9f30e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 427,
                "y": 76
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b8a11e60-9474-496b-ba10-b0ceaa4c2a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 444,
                "y": 76
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a173e74a-3927-4c14-b2c4-ca0106bcfe37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 405,
                "y": 113
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ed2e1a88-4730-490b-abf6-8f2ca7856b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 398,
                "y": 113
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4729cf33-caf5-4dce-8f71-42dfb1e161c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 390,
                "y": 113
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7098f6b9-7298-4ffb-bd10-563110378815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 373,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1a6b494b-7959-41f7-8b45-29df500468cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 356,
                "y": 113
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "06a2fa16-a01b-4516-ae50-305959e9c964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 339,
                "y": 113
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "218d2173-e605-4b8c-899b-7e000938dce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 313,
                "y": 113
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0c244681-bd85-4ccb-a8ac-b10b29f81f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 292,
                "y": 113
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e28e71d8-9441-4db3-9197-8bc397b11bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 30,
                "w": 28,
                "x": 262,
                "y": 113
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f3eeb1bb-ea74-4559-897a-91c8af4ba5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 238,
                "y": 113
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a5f4f117-b8cd-471e-a923-aae76d69b715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 214,
                "y": 113
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0864fe8b-544f-42bb-a659-52dee32da675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 0,
                "shift": 28,
                "w": 26,
                "x": 186,
                "y": 113
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "73299ca4-f4f6-40a8-99c7-26fe1d6d58d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 164,
                "y": 113
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8c8e3651-8ff1-4c61-acfc-296840bfc15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 138,
                "y": 113
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "71053346-c05d-435d-a1dd-f1b020b78093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 8,
                "shift": 26,
                "w": 16,
                "x": 120,
                "y": 113
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a8ee2955-20db-4cdc-b45e-4efad72cc269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 100,
                "y": 113
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9050fe49-9f68-4fcb-b3ff-eb9db526857f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 80,
                "y": 113
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8acba9dc-1809-4870-b5ca-baada0396b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 54,
                "y": 113
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "54621f89-5e0f-4827-887e-4127f9662856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 28,
                "y": 113
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "38ba5a87-ae3d-454e-a9e5-cbfd48cf8fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1bfe7535-79d8-48a4-b460-a81c3c931a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 461,
                "y": 76
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51b9e093-d3ae-4d79-adb0-5f3cc4560051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 24,
                "y": 76
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c4c24d94-77c1-467c-8c68-0faf8c7a5940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0f84c8e0-bb7f-4d24-972b-db4e3115afd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 473,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "687e470f-ce9c-42ee-a01f-1b02fd3643e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "05d12e2c-f0c8-49f6-a6f9-e0893d41f906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "da31b288-1201-48e7-8919-b2aff17eaea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 397,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "31758163-6d54-40e8-9933-441f11dd3e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 4,
                "shift": 20,
                "w": 14,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "96519ae0-6aa8-4a66-888b-2c3b72afa034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "de3b40c2-bda5-4c93-b574-c092e56c54a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "953dd304-7eb6-45fb-b071-eedbe4f2afa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1b181066-44ff-4ff8-854c-5e5cbcb8dca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8c32b0ba-edc0-4388-9504-34db25a9df1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "24bad2d5-bdbb-4afb-9002-e6877af066b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "31b50343-75c5-44b0-95f2-41b156e17ef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 6,
                "shift": 19,
                "w": 9,
                "x": 441,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f9da5667-9a5b-45fc-9f75-7dfcb3ab997b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8c214e09-6a07-4a7e-93fe-3ddf50643191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 9,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ae106ba7-67a8-4818-a263-e42153735f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ccdedddc-9437-4af0-8bfd-9f32e811478e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bfb1c444-5037-4a61-8571-7ef1c503cc6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "14418a0a-03be-4258-a90d-46c28c6a4e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 0,
                "shift": 30,
                "w": 28,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fae33cef-627a-4ef9-9784-0d317bef306c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "35b264de-61ea-4a15-9f15-08b0cdb5b55f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d5dcc096-17f2-42f9-a6e6-45b65c40bc1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 0,
                "shift": 28,
                "w": 26,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "617316b2-05df-4dd4-a984-7619b43c27d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "50558a00-1960-4975-bf53-dc93f375c645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "80ababe3-f6d6-4fe2-9712-1fa97c612140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 8,
                "shift": 26,
                "w": 16,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3bf45611-17bc-4217-b18b-789c2f39ff4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 201,
                "y": 39
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8de7d125-446b-48d7-8c9f-75f6f421fbb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6d4f66de-1d11-4ad6-93bf-a5cd27c94034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 425,
                "y": 39
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9014a280-1ff8-46d8-86db-8789dba32add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 399,
                "y": 39
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6cf6990b-18cc-4cb3-9b62-2093e4b70f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 373,
                "y": 39
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "965b6018-f7ea-4033-b9b3-51a48dfddf31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 347,
                "y": 39
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e924ae52-0218-4491-bd88-9271bdaa3cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 325,
                "y": 39
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e98157f5-2f54-4e1f-96f8-9805e3b3c1c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 303,
                "y": 39
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e1d03e8-262e-4107-a1d0-fd9bcff3ae04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 281,
                "y": 39
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c9070451-325d-4fe1-9348-6f129e6e8e6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 259,
                "y": 39
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "73cbd86c-ab11-4f36-8c60-39dbdbb30cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 237,
                "y": 39
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2649526b-9e53-4bd1-b680-f89a8c012d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 451,
                "y": 39
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "32049744-b25a-4cec-883b-48ae7ff019ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 4,
                "shift": 20,
                "w": 14,
                "x": 221,
                "y": 39
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e3e3682c-5248-47e1-9a15-d48fdc0c4cdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 175,
                "y": 39
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f99389f1-24c7-4c16-85e4-68104e5c2d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 153,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ed78396d-da33-4ed7-b9b6-eeaf0f98c690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 125,
                "y": 39
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "581e1b36-8a31-4c56-bfa0-57291ac3d0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 18,
                "x": 105,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "61a68824-9d5d-4498-bc75-9f544b38681d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 79,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5c96f1df-898a-4a73-b322-b3b57fe374c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 6,
                "shift": 28,
                "w": 20,
                "x": 57,
                "y": 39
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a8d8dde8-1856-485a-9c42-03aded733d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 42,
                "y": 39
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6a29bd93-894f-4749-9827-a3f1fab177c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 37,
                "y": 39
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e9fb7500-062a-474f-a0d3-7303523e933b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 22,
                "y": 39
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f87058c9-e168-42df-a546-85b110b224ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 422,
                "y": 113
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1c9f403b-8471-4cda-aebf-0f8588f54422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 35,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 439,
                "y": 113
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)\\u000aBirdRunes",
    "size": 24,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}