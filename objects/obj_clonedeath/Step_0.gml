/// @DnDAction : YoYo Games.Rooms.Get_Current_Room
/// @DnDVersion : 1
/// @DnDHash : 3CC646DF
/// @DnDArgument : "var" "currentroom"
currentroom = room;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 11AF66A2
/// @DnDArgument : "var" "currentroom"
/// @DnDArgument : "value" "rm_boss1stage1"
if(currentroom == rm_boss1stage1)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 6260F7AA
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "xpos" "576"
	/// @DnDArgument : "ypos" "448"
	/// @DnDArgument : "objectid" "obj_key"
	/// @DnDSaveInfo : "objectid" "16941c96-be99-4220-bbee-9e214aa47826"
	instance_create_layer(576, 448, "Instances", obj_key);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 745D928C
	/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	with(obj_player) {
	controllock = true;
	
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7C3B1399
	/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "expr" "obj_key"
	/// @DnDArgument : "var" "camerafollow"
	with(obj_camera) {
	camerafollow = obj_key;
	
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7EFC8BFE
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "var" "alarmset"
	/// @DnDArgument : "value" "false"
	if(alarmset == false)
	{
		/// @DnDAction : YoYo Games.Instances.Set_Alarm
		/// @DnDVersion : 1
		/// @DnDHash : 2A8535EA
		/// @DnDParent : 7EFC8BFE
		/// @DnDArgument : "steps" "100"
		/// @DnDArgument : "alarm" "2"
		alarm_set(2, 100);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 151409FB
		/// @DnDParent : 7EFC8BFE
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "alarmset"
		alarmset = true;
	}

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 249740AE
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "steps" "-1"
	/// @DnDArgument : "steps_relative" "1"
	/// @DnDArgument : "alarm" "2"
	alarm_set(2, -1 + alarm_get(2));

	/// @DnDAction : YoYo Games.Instances.Get_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 6E95B4CA
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "var" "delay"
	/// @DnDArgument : "alarm" "2"
	delay = alarm_get(2);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 75FE0EF6
	/// @DnDParent : 11AF66A2
	/// @DnDArgument : "var" "delay"
	/// @DnDArgument : "op" "3"
	if(delay <= 0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6BC93872
		/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
		/// @DnDParent : 75FE0EF6
		/// @DnDArgument : "expr" "obj_player"
		/// @DnDArgument : "var" "camerafollow"
		with(obj_camera) {
		camerafollow = obj_player;
		
		}
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 113DD036
		/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
		/// @DnDParent : 75FE0EF6
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "var" "controllock"
		with(obj_player) {
		controllock = false;
		
		}
	
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 454265EE
		/// @DnDParent : 75FE0EF6
		instance_destroy();
	}
}