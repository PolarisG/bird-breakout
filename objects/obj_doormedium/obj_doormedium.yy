{
    "id": "8a4ae1a3-7bd2-4cf8-b3bf-8add297fe03b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_doormedium",
    "eventList": [
        {
            "id": "d1424046-0069-45c7-851e-8d6b4ea9a820",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d64475a8-3db2-43b7-9702-39be88c9e600",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8a4ae1a3-7bd2-4cf8-b3bf-8add297fe03b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b77218a9-a681-4762-8642-4dcc3841131c",
    "visible": true
}