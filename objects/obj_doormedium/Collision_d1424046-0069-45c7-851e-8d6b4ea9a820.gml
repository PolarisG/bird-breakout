/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 1ACEDB74
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
with(obj_player) {
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7BC3286E
	/// @DnDParent : 1ACEDB74
	/// @DnDArgument : "var" "haskey"
	/// @DnDArgument : "value" "true"
	if(haskey == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 4931F0C2
		/// @DnDParent : 7BC3286E
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "haskey"
		haskey += false;
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 162A86FF
		/// @DnDApplyTo : 8a4ae1a3-7bd2-4cf8-b3bf-8add297fe03b
		/// @DnDParent : 7BC3286E
		with(obj_doormedium) {
			/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
			/// @DnDVersion : 1
			/// @DnDHash : 0C641673
			/// @DnDParent : 162A86FF
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			position_destroy(x + 0, y + 0);
		}
	}
}