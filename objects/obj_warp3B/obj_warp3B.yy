{
    "id": "b5ba6e0d-1aed-48cf-9491-ad6127b87446",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp3B",
    "eventList": [
        {
            "id": "f27fe616-0ffe-4ed7-98b5-2f051ba3c2c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5ba6e0d-1aed-48cf-9491-ad6127b87446"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
    "visible": true
}