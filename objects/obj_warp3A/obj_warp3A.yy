{
    "id": "ec1d40ff-1deb-42db-8ca2-98f48336639a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp3A",
    "eventList": [
        {
            "id": "596dc1cc-41e7-4993-a3c8-07a28b9311a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec1d40ff-1deb-42db-8ca2-98f48336639a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d97cb63-2f58-45e0-aaa1-fbc35368f3ce",
    "visible": true
}