{
    "id": "63ee7a46-7ad3-4bd5-9445-1111f5a732d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp1A",
    "eventList": [
        {
            "id": "658cc007-3624-4c4a-88e2-9b7f62cc5a4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63ee7a46-7ad3-4bd5-9445-1111f5a732d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
    "visible": true
}