/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 648D7923
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_mouse"
/// @DnDSaveInfo : "object" "bd650bbc-d75c-4851-bb39-ca023a96434a"
var l648D7923_0 = instance_place(x + 0, y + 0, obj_mouse);
if ((l648D7923_0 > 0))
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alignment
	/// @DnDVersion : 1.1
	/// @DnDHash : 75C9006C
	/// @DnDParent : 648D7923
	/// @DnDArgument : "halign" "fa_center"
	/// @DnDArgument : "valign" "fa_middle"
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);

	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 65F72BBD
	/// @DnDParent : 648D7923
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 6B698F0A
	/// @DnDParent : 648D7923
	/// @DnDArgument : "color" "$FF00FFFF"
	draw_set_colour($FF00FFFF & $ffffff);
	var l6B698F0A_0=($FF00FFFF >> 24);
	draw_set_alpha(l6B698F0A_0 / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 22B6EB86
	/// @DnDParent : 648D7923
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" ""New Game""
	draw_text(x + 0, y + 0, string("") + string("New Game"));
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 29D07A6B
else
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alignment
	/// @DnDVersion : 1.1
	/// @DnDHash : 3F2D5A58
	/// @DnDParent : 29D07A6B
	/// @DnDArgument : "halign" "fa_center"
	/// @DnDArgument : "valign" "fa_middle"
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);

	/// @DnDAction : YoYo Games.Drawing.Set_Font
	/// @DnDVersion : 1
	/// @DnDHash : 6B99ADB9
	/// @DnDParent : 29D07A6B
	/// @DnDArgument : "font" "f_default"
	/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
	draw_set_font(f_default);

	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 49041CE7
	/// @DnDParent : 29D07A6B
	draw_set_colour($FFFFFFFF & $ffffff);
	var l49041CE7_0=($FFFFFFFF >> 24);
	draw_set_alpha(l49041CE7_0 / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 354DC98D
	/// @DnDParent : 29D07A6B
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" ""New Game""
	draw_text(x + 0, y + 0, string("") + string("New Game"));
}