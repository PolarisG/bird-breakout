{
    "id": "a5125cf4-a6c5-44f8-b75e-3a8ea861831b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startgame",
    "eventList": [
        {
            "id": "50b32722-049c-44c2-971d-9d38bd80507b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a5125cf4-a6c5-44f8-b75e-3a8ea861831b"
        },
        {
            "id": "3a06a488-0e76-41b1-adf8-f307c8de8fec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a5125cf4-a6c5-44f8-b75e-3a8ea861831b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38efe206-356a-4043-a174-fbf0f90922d5",
    "visible": true
}