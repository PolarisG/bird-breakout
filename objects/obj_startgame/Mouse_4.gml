/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 6C9C1BF4
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_mouse"
/// @DnDSaveInfo : "object" "bd650bbc-d75c-4851-bb39-ca023a96434a"
var l6C9C1BF4_0 = instance_place(x + 0, y + 0, obj_mouse);
if ((l6C9C1BF4_0 > 0))
{
	/// @DnDAction : YoYo Games.Game.Save_Game
	/// @DnDVersion : 1
	/// @DnDHash : 08D65AA3
	/// @DnDParent : 6C9C1BF4
	game_save("save.dat");

	/// @DnDAction : YoYo Games.Rooms.Next_Room
	/// @DnDVersion : 1
	/// @DnDHash : 43261855
	/// @DnDParent : 6C9C1BF4
	room_goto_next();
}