/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 5A13169C
/// @DnDArgument : "x" "-450"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "-250"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Current Level: ""
/// @DnDArgument : "var" "global.currentlevel"
draw_text(x + -450, y + -250, string("Current Level: ") + string(global.currentlevel));