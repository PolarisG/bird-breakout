{
    "id": "83a30f64-90f4-4106-aa82-4138babb53ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_1up",
    "eventList": [
        {
            "id": "7bc64a6d-90c3-48be-b7aa-90aad6530d54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d64475a8-3db2-43b7-9702-39be88c9e600",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "83a30f64-90f4-4106-aa82-4138babb53ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77469c77-f827-4bc2-b7f0-bd233c494a05",
    "visible": true
}