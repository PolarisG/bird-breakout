{
    "id": "f3803e12-e4b9-4584-be67-7c7b855c3ae0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadowmonsterLV1",
    "eventList": [
        {
            "id": "1cb38d39-66b2-4f5b-ad17-6017c26bf417",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8d33e9c5-4529-45cb-91b9-0b206f882847",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3803e12-e4b9-4584-be67-7c7b855c3ae0"
        },
        {
            "id": "229eca96-58a6-4ed4-8da7-a1bf86082dfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3803e12-e4b9-4584-be67-7c7b855c3ae0"
        },
        {
            "id": "165eab58-5664-4e6c-becc-5eceb55ba3da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3803e12-e4b9-4584-be67-7c7b855c3ae0"
        },
        {
            "id": "0b7cf97f-bf69-4806-941f-a5a9dd36aec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3803e12-e4b9-4584-be67-7c7b855c3ae0"
        },
        {
            "id": "30eaf2fb-c84a-4a34-9586-2b22110386ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3803e12-e4b9-4584-be67-7c7b855c3ae0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9ede6001-2d32-451c-b8bd-fa206b15b41e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "facing",
            "varType": 0
        },
        {
            "id": "82017732-78a7-4b31-b2bf-71e1ecb20287",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "spd",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "894350fa-c44b-4595-98de-e375abfd6e52",
    "visible": true
}