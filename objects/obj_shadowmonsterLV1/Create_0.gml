/// @DnDAction : YoYo Games.Instance Variables.Set_Health
/// @DnDVersion : 1
/// @DnDHash : 5B4F2CBF
/// @DnDArgument : "health" "100"

__dnd_health = real(100);

/// @DnDAction : YoYo Games.Random.Choose
/// @DnDVersion : 1
/// @DnDHash : 35DDB0F4
/// @DnDArgument : "var" "facing"
/// @DnDArgument : "option" "1, -1"
facing = choose(1, -1);