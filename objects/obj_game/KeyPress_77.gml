/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 76F77D70
/// @DnDArgument : "var" "mute"
/// @DnDArgument : "value" "false"
if(mute == false)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 08A28950
	/// @DnDParent : 76F77D70
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "mute"
	mute = true;

	/// @DnDAction : YoYo Games.Audio.Stop_All_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 66B16AA5
	/// @DnDParent : 76F77D70
	audio_stop_all();

	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 078B5A34
	/// @DnDParent : 76F77D70
	exit;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 132B2632
/// @DnDArgument : "var" "mute"
/// @DnDArgument : "value" "true"
if(mute == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 704BE1B0
	/// @DnDParent : 132B2632
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "mute"
	mute = false;

	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 0AA814D2
	/// @DnDParent : 132B2632
	exit;
}