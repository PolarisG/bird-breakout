/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5B9E9BE4
/// @DnDArgument : "code" "view_x=camera_get_view_x(view_camera[0]);$(13_10)view_y=camera_get_view_y(view_camera[0]);$(13_10)$(13_10)if (mouse_wheel_down())$(13_10){$(13_10)	if (size_w<960)$(13_10)	{$(13_10)		size_w+=32;$(13_10)		size_h+=18;$(13_10)		camera_set_view_size(view_camera[0],size_w,size_h);$(13_10)		camera_set_view_border(view_camera[0],size_w,size_h);$(13_10)	}$(13_10)}$(13_10)else if (mouse_wheel_up())$(13_10){$(13_10)	if (size_w>320)$(13_10)	{$(13_10)		size_w-=32;$(13_10)		size_h-=18;$(13_10)		camera_set_view_size(view_camera[0],size_w,size_h);$(13_10)		camera_set_view_border(view_camera[0],size_w,size_h);$(13_10)	}$(13_10)}$(13_10)$(13_10)if (keyboard_check_pressed(vk_add))$(13_10){$(13_10)		var monitor_size_w=display_get_width();$(13_10)		var monitor_size_h=display_get_height();$(13_10)		if (window_size_w==960)$(13_10)		{$(13_10)			window_size_w=1280;$(13_10)			window_size_h=720;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		else if (window_size_w==1280)$(13_10)		{$(13_10)			window_size_w=1408;$(13_10)			window_size_h=792;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		else if (window_size_w==1408)$(13_10)		{$(13_10)			window_size_w=1920;$(13_10)			window_size_h=1080;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);$(13_10)}$(13_10)else if (keyboard_check_pressed(vk_subtract))$(13_10){$(13_10)		var monitor_size_w=display_get_width();$(13_10)		var monitor_size_h=display_get_height();$(13_10)		if (window_size_w==1920)$(13_10)		{$(13_10)			window_size_w=1408;$(13_10)			window_size_h=792;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		else if (window_size_w==1408)$(13_10)		{$(13_10)			window_size_w=1280;$(13_10)			window_size_h=720;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		else if (window_size_w==1280)$(13_10)		{$(13_10)			window_size_w=960;$(13_10)			window_size_h=540;$(13_10)			window_set_size(window_size_w,window_size_h);$(13_10)		}$(13_10)		window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);$(13_10)}$(13_10)$(13_10)gpu_set_tex_filter(false);"
view_x=camera_get_view_x(view_camera[0]);
view_y=camera_get_view_y(view_camera[0]);

if (mouse_wheel_down())
{
	if (size_w<960)
	{
		size_w+=32;
		size_h+=18;
		camera_set_view_size(view_camera[0],size_w,size_h);
		camera_set_view_border(view_camera[0],size_w,size_h);
	}
}
else if (mouse_wheel_up())
{
	if (size_w>320)
	{
		size_w-=32;
		size_h-=18;
		camera_set_view_size(view_camera[0],size_w,size_h);
		camera_set_view_border(view_camera[0],size_w,size_h);
	}
}

if (keyboard_check_pressed(vk_add))
{
		var monitor_size_w=display_get_width();
		var monitor_size_h=display_get_height();
		if (window_size_w==960)
		{
			window_size_w=1280;
			window_size_h=720;
			window_set_size(window_size_w,window_size_h);
		}
		else if (window_size_w==1280)
		{
			window_size_w=1408;
			window_size_h=792;
			window_set_size(window_size_w,window_size_h);
		}
		else if (window_size_w==1408)
		{
			window_size_w=1920;
			window_size_h=1080;
			window_set_size(window_size_w,window_size_h);
		}
		window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);
}
else if (keyboard_check_pressed(vk_subtract))
{
		var monitor_size_w=display_get_width();
		var monitor_size_h=display_get_height();
		if (window_size_w==1920)
		{
			window_size_w=1408;
			window_size_h=792;
			window_set_size(window_size_w,window_size_h);
		}
		else if (window_size_w==1408)
		{
			window_size_w=1280;
			window_size_h=720;
			window_set_size(window_size_w,window_size_h);
		}
		else if (window_size_w==1280)
		{
			window_size_w=960;
			window_size_h=540;
			window_set_size(window_size_w,window_size_h);
		}
		window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);
}

gpu_set_tex_filter(false);

/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
/// @DnDVersion : 1
/// @DnDHash : 61646798
/// @DnDArgument : "key" "ord("1")"
var l61646798_0;
l61646798_0 = keyboard_check_pressed(ord("1"));
if (l61646798_0)
{
	/// @DnDAction : YoYo Games.Audio.Stop_All_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 49DE022E
	/// @DnDParent : 61646798
	audio_stop_all();

	/// @DnDAction : YoYo Games.Rooms.Next_Room
	/// @DnDVersion : 1
	/// @DnDHash : 260AC8A5
	/// @DnDParent : 61646798
	room_goto_next();

	/// @DnDAction : YoYo Games.Game.Save_Game
	/// @DnDVersion : 1
	/// @DnDHash : 67C6AE6D
	/// @DnDParent : 61646798
	game_save("save.dat");
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7F8ACCE0
/// @DnDArgument : "var" "mute"
/// @DnDArgument : "value" "false"
if(mute == false)
{
	/// @DnDAction : YoYo Games.Audio.If_Audio_Playing
	/// @DnDVersion : 1
	/// @DnDHash : 73155BD4
	/// @DnDParent : 7F8ACCE0
	/// @DnDArgument : "soundid" "currentmusic"
	/// @DnDArgument : "not" "1"
	var l73155BD4_0 = currentmusic;
	if (!audio_is_playing(l73155BD4_0))
	{
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 68149E78
		/// @DnDParent : 73155BD4
		/// @DnDArgument : "soundid" "currentmusic"
		/// @DnDArgument : "loop" "1"
		audio_play_sound(currentmusic, 0, 1);
	}
}