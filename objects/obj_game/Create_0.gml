/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 67CE98A3
/// @DnDArgument : "code" "view_x=camera_get_view_x(view_camera[0]);$(13_10)view_y=camera_get_view_y(view_camera[0]);$(13_10)$(13_10)size_w=960;$(13_10)size_h=540;$(13_10)$(13_10)window_size_w=1280;$(13_10)window_size_h=720;$(13_10)window_set_size(window_size_w,window_size_h);$(13_10)$(13_10)var monitor_size_w=display_get_width();$(13_10)var monitor_size_h=display_get_height();$(13_10)$(13_10)window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);"
view_x=camera_get_view_x(view_camera[0]);
view_y=camera_get_view_y(view_camera[0]);

size_w=960;
size_h=540;

window_size_w=1280;
window_size_h=720;
window_set_size(window_size_w,window_size_h);

var monitor_size_w=display_get_width();
var monitor_size_h=display_get_height();

window_set_position(monitor_size_w/2-window_size_w/2,monitor_size_h/2-window_size_h/2);

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 14502EDB
/// @DnDArgument : "font" "f_default"
/// @DnDSaveInfo : "font" "4f9d41de-5300-4ed2-9b99-4a9eebd717c0"
draw_set_font(f_default);

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 20D15C59
draw_set_colour($FFFFFFFF & $ffffff);
var l20D15C59_0=($FFFFFFFF >> 24);
draw_set_alpha(l20D15C59_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 17F8B93B
draw_set_halign(fa_left);
draw_set_valign(fa_top);