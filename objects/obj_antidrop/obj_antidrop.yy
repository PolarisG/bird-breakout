{
    "id": "ac75e150-f465-4bfb-9d27-d70475bec6d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_antidrop",
    "eventList": [
        {
            "id": "41727c2e-7681-4bb6-8cf0-c47358c35382",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d64475a8-3db2-43b7-9702-39be88c9e600",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ac75e150-f465-4bfb-9d27-d70475bec6d0"
        },
        {
            "id": "2ab9415f-50d0-493b-be66-46230962c9f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd2ddf97-b455-445f-9a98-315d6a98aa55",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ac75e150-f465-4bfb-9d27-d70475bec6d0"
        },
        {
            "id": "3a8dbd20-b1ed-4e38-bc85-449cef6b391a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ac75e150-f465-4bfb-9d27-d70475bec6d0"
        },
        {
            "id": "127d9193-706d-4526-8f9c-36a9b59f62ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ee00b552-243c-4d86-b081-918e4485ac14",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ac75e150-f465-4bfb-9d27-d70475bec6d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03541a19-4818-46ba-858e-55e027503ee2",
    "visible": true
}