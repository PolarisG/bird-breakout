{
    "id": "5cb6398c-b4d8-4f3d-b750-8374fd964230",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp2B",
    "eventList": [
        {
            "id": "4ba0ffbc-10c9-4bff-94fd-ba38eb8e0ca3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cb6398c-b4d8-4f3d-b750-8374fd964230"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
    "visible": true
}