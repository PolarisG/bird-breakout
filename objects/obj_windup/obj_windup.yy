{
    "id": "cfca85e2-e9b3-46bc-9902-b6a142e350bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_windup",
    "eventList": [
        {
            "id": "191a5ded-117a-4864-b69c-ed06d29dee4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfca85e2-e9b3-46bc-9902-b6a142e350bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07edc611-8b88-42db-8f0c-f31600937ef8",
    "visible": true
}