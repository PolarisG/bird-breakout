/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 7362A203
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
with(obj_player) {
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 63E07236
	/// @DnDParent : 7362A203
	/// @DnDArgument : "var" "haskey"
	/// @DnDArgument : "value" "true"
	if(haskey == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 2DE5A2F6
		/// @DnDParent : 63E07236
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "haskey"
		haskey += false;
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 2166C2FB
		/// @DnDApplyTo : 278b2cfc-112b-4c7d-8ff2-220f09c01e22
		/// @DnDParent : 63E07236
		with(obj_doorlarge) {
			/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
			/// @DnDVersion : 1
			/// @DnDHash : 649A87C0
			/// @DnDParent : 2166C2FB
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			position_destroy(x + 0, y + 0);
		}
	}
}