{
    "id": "278b2cfc-112b-4c7d-8ff2-220f09c01e22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_doorlarge",
    "eventList": [
        {
            "id": "1d12af71-f983-41c8-b7e0-d5e852e9af07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d64475a8-3db2-43b7-9702-39be88c9e600",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "278b2cfc-112b-4c7d-8ff2-220f09c01e22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ec238ce-2353-4f9d-afce-b85c9b63e5d0",
    "visible": true
}