{
    "id": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laserright",
    "eventList": [
        {
            "id": "fe7d94d9-79e1-4b57-bb6e-0103f7316db2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "8981386d-e286-46b8-8f7c-167ea495fc16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cb25211f-46da-4813-a595-dee799f30ed6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "6e442570-6f0c-44ec-97a0-eb612205af81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd2ddf97-b455-445f-9a98-315d6a98aa55",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "e77fafa5-b58d-4564-ba90-c4d117bfa80f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a3b19797-d99e-49e8-b8f7-d6f55aa1f741",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "f3b72760-e12d-48ab-9f53-30759b822219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "72d7e06f-1c7e-4bb0-8fae-fc317c069f8b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "444eb84b-c216-4f4c-8305-c934ad394f79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a50322a5-7105-4d68-b807-8002db2ece13",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "ea3c0c1b-8ba0-4c98-8224-24b53b6a44ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1499d865-72a6-46b9-864f-53a5648d5525",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "de81aa17-54bb-4dbf-909a-74372ab80bf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        },
        {
            "id": "ed3d8ff2-1dbd-4d21-a8d7-9009f578d99e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f3803e12-e4b9-4584-be67-7c7b855c3ae0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "e7586574-906c-4029-a206-a2de5636bde7",
    "visible": true
}