/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
/// @DnDVersion : 1
/// @DnDHash : 1793EE9A
/// @DnDArgument : "key" "vk_enter"
var l1793EE9A_0;
l1793EE9A_0 = keyboard_check_pressed(vk_enter);
if (l1793EE9A_0)
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 047B4A25
	/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
	/// @DnDParent : 1793EE9A
	with(obj_camera) {
		/// @DnDAction : YoYo Games.Instances.Create_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 5A8F020B
		/// @DnDParent : 047B4A25
		/// @DnDArgument : "xpos_relative" "1"
		/// @DnDArgument : "ypos" "175"
		/// @DnDArgument : "ypos_relative" "1"
		/// @DnDArgument : "objectid" "obj_textbox"
		/// @DnDArgument : "layer" ""text""
		/// @DnDSaveInfo : "objectid" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
		instance_create_layer(x + 0, y + 175, "text", obj_textbox);
	}

	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 4416F6B1
	/// @DnDApplyTo : 337c3fd2-446d-4312-9657-0c0a5f79ffb8
	/// @DnDParent : 1793EE9A
	with(obj_textbox) {
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 590E42B6
		/// @DnDParent : 4416F6B1
		/// @DnDArgument : "script" "S1sign()"
		script_execute(S1sign());
	}
}