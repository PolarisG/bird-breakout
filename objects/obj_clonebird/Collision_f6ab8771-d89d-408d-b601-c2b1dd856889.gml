/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
/// @DnDVersion : 1
/// @DnDHash : 4239D32F
/// @DnDArgument : "key" "vk_enter"
var l4239D32F_0;
l4239D32F_0 = keyboard_check_pressed(vk_enter);
if (l4239D32F_0)
{
	/// @DnDAction : YoYo Games.Rooms.Previous_Room
	/// @DnDVersion : 1
	/// @DnDHash : 62EFAB2C
	/// @DnDParent : 4239D32F
	room_goto_previous();

	/// @DnDAction : YoYo Games.Game.Save_Game
	/// @DnDVersion : 1
	/// @DnDHash : 65D1EDBE
	/// @DnDParent : 4239D32F
	game_save("save.dat");

	/// @DnDAction : YoYo Games.Audio.Stop_All_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 7F6A315E
	/// @DnDParent : 4239D32F
	audio_stop_all();
}