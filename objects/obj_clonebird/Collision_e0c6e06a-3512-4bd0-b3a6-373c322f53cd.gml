/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
/// @DnDVersion : 1
/// @DnDHash : 02BB959F
/// @DnDArgument : "key" "vk_enter"
var l02BB959F_0;
l02BB959F_0 = keyboard_check_pressed(vk_enter);
if (l02BB959F_0)
{
	/// @DnDAction : YoYo Games.Movement.Jump_To_Point
	/// @DnDVersion : 1
	/// @DnDHash : 19A14683
	/// @DnDParent : 02BB959F
	/// @DnDArgument : "x" "global.warp1Ax"
	/// @DnDArgument : "y" "global.warp1Ay"
	x = global.warp1Ax;
	y = global.warp1Ay;
}