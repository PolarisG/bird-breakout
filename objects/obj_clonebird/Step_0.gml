/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 27616A40
/// @DnDArgument : "script" "get_inputreverse"
script_execute(get_inputreverse);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 4E7C9BCD
/// @DnDArgument : "script" "calc_movement"
/// @DnDSaveInfo : "script" "23dc3d95-c4ea-47db-ad45-6ccce96e7dda"
script_execute(calc_movement);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 188BDF27
/// @DnDArgument : "script" "check_ground"
/// @DnDSaveInfo : "script" "6a95ae44-1da1-442f-95e4-327bf61ae0ec"
script_execute(check_ground);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 6862A45D
/// @DnDArgument : "script" "check_jump"
/// @DnDSaveInfo : "script" "90505c14-9b82-435b-b6dc-a207c78132fb"
script_execute(check_jump);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 41556E10
/// @DnDArgument : "script" "collision"
/// @DnDSaveInfo : "script" "ef1430cd-f646-45d8-9987-760c1cee0636"
script_execute(collision);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 1E94BA94
/// @DnDArgument : "script" "anim"
/// @DnDSaveInfo : "script" "cc17b1af-94ed-4d38-9ecd-7d314cc5d73d"
script_execute(anim);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 4D1C221C
/// @DnDArgument : "script" "wind_physics"
/// @DnDSaveInfo : "script" "ad8f1a21-0cb3-4dc6-bcb0-4cf6743ccf5c"
script_execute(wind_physics);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 3BC61B8A
/// @DnDArgument : "script" "ice_physics"
/// @DnDSaveInfo : "script" "2efbfed2-1185-4351-984c-102fee185526"
script_execute(ice_physics);

/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 15EF2F9A
/// @DnDArgument : "script" "trap"
/// @DnDSaveInfo : "script" "ad27d53d-2aa9-4008-96fa-d1f43429a95c"
script_execute(trap);