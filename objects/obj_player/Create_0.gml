/// @DnDAction : YoYo Games.Instances.If_Instance_Exists
/// @DnDVersion : 1
/// @DnDHash : 1557A824
/// @DnDArgument : "obj" "obj_textbox"
/// @DnDArgument : "not" "1"
/// @DnDSaveInfo : "obj" "337c3fd2-446d-4312-9657-0c0a5f79ffb8"
var l1557A824_0 = false;
l1557A824_0 = instance_exists(obj_textbox);
if(!l1557A824_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 715C01FC
	/// @DnDParent : 1557A824
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "controllock"
	controllock = false;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 6A32CBFA
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 286FF5AB
	/// @DnDParent : 6A32CBFA
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	controllock = true;
}