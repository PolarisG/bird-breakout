/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4FACF717
/// @DnDArgument : "var" "crouching"
/// @DnDArgument : "value" "false"
if(crouching == false)
{
	/// @DnDAction : YoYo Games.Game.Load_Game
	/// @DnDVersion : 1
	/// @DnDHash : 26E5BF3F
	/// @DnDParent : 4FACF717
	game_load("save.dat");

	/// @DnDAction : YoYo Games.Audio.Stop_All_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 78FE53EF
	/// @DnDParent : 4FACF717
	audio_stop_all();
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 437845C8
else
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 547706CB
	/// @DnDApplyTo : f3803e12-e4b9-4584-be67-7c7b855c3ae0
	/// @DnDParent : 437845C8
	with(obj_shadowmonsterLV1) {
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 7575AF9B
		/// @DnDParent : 547706CB
		instance_destroy();
	}
}