/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
/// @DnDVersion : 1
/// @DnDHash : 3B2C9F7C
/// @DnDArgument : "key" "vk_enter"
var l3B2C9F7C_0;
l3B2C9F7C_0 = keyboard_check_pressed(vk_enter);
if (l3B2C9F7C_0)
{
	/// @DnDAction : YoYo Games.Movement.Jump_To_Point
	/// @DnDVersion : 1
	/// @DnDHash : 0915BC1D
	/// @DnDParent : 3B2C9F7C
	/// @DnDArgument : "x" "global.warp3Ax"
	/// @DnDArgument : "y" "global.warp3Ay"
	x = global.warp3Ax;
	y = global.warp3Ay;
}