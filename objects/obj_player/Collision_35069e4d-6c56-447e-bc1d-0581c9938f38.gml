/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 17FEE433
/// @DnDApplyTo : fdcd5805-1221-43f1-8a73-e7cf69914c26
with(obj_lasergun) instance_destroy();

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 541D3FDA
/// @DnDArgument : "expr" "true"
/// @DnDArgument : "var" "hasgun"
hasgun = true;