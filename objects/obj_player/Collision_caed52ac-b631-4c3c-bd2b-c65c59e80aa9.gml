/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 282AA81C
/// @DnDApplyTo : b8f2740a-f816-4e35-bfa4-5741586c667f
with(obj_cutscene1) {
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 7D32FAFB
	/// @DnDApplyTo : d740379a-87ae-432a-96b5-b959c65ff526
	/// @DnDParent : 282AA81C
	with(obj_camera) {
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 790F08FC
		/// @DnDParent : 7D32FAFB
		/// @DnDArgument : "expr" "obj_clonebird"
		/// @DnDArgument : "var" "camerafollow"
		camerafollow = obj_clonebird;
	}

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2299130C
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "var" "alarmset"
	/// @DnDArgument : "value" "false"
	if(alarmset == false)
	{
		/// @DnDAction : YoYo Games.Instances.Set_Alarm
		/// @DnDVersion : 1
		/// @DnDHash : 69794CB6
		/// @DnDParent : 2299130C
		/// @DnDArgument : "steps" "100"
		/// @DnDArgument : "alarm" "1"
		alarm_set(1, 100);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 003FDB05
		/// @DnDParent : 2299130C
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "alarmset"
		alarmset = true;
	}

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 78000908
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "steps" "-1"
	/// @DnDArgument : "steps_relative" "1"
	/// @DnDArgument : "alarm" "1"
	alarm_set(1, -1 + alarm_get(1));

	/// @DnDAction : YoYo Games.Instances.Get_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 3A790EA2
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "var" "delay"
	/// @DnDArgument : "alarm" "1"
	delay = alarm_get(1);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6B46BBC2
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "var" "delay"
	/// @DnDArgument : "op" "3"
	if(delay <= 0)
	{
		/// @DnDAction : YoYo Games.Common.Execute_Script
		/// @DnDVersion : 1.1
		/// @DnDHash : 5500A438
		/// @DnDParent : 6B46BBC2
		/// @DnDArgument : "script" "currentcutscene"
		script_execute(currentcutscene);
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2E9B3545
	/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	with(obj_player) {
	controllock = true;
	
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 57AE4083
	/// @DnDApplyTo : 793b8f5a-3f85-44df-ae17-b67cc569a275
	/// @DnDParent : 282AA81C
	/// @DnDArgument : "expr" "true"
	/// @DnDArgument : "var" "controllock"
	with(obj_clonebird) {
	controllock = true;
	
	}
}