/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1852AD9C
/// @DnDArgument : "var" "crouching"
/// @DnDArgument : "value" "false"
if(crouching == false)
{
	/// @DnDAction : YoYo Games.Game.Load_Game
	/// @DnDVersion : 1
	/// @DnDHash : 26E5BF3F
	/// @DnDParent : 1852AD9C
	game_load("save.dat");

	/// @DnDAction : YoYo Games.Audio.Stop_All_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 78FE53EF
	/// @DnDParent : 1852AD9C
	audio_stop_all();
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 489EA1C0
else
{
	/// @DnDAction : YoYo Games.Common.Apply_To
	/// @DnDVersion : 1
	/// @DnDHash : 4091797E
	/// @DnDApplyTo : 1aff1c99-4fac-4377-8b40-0e5f34814b65
	/// @DnDParent : 489EA1C0
	with(obj_shadowmonsterLV2) {
		/// @DnDAction : YoYo Games.Instances.Destroy_Instance
		/// @DnDVersion : 1
		/// @DnDHash : 358B6298
		/// @DnDParent : 4091797E
		instance_destroy();
	}
}