{
    "id": "a68c2aef-d684-4a10-a571-b25981e7417e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "a3c7dbe4-6f75-4b07-a705-e2c1de682b82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d64475a8-3db2-43b7-9702-39be88c9e600",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a68c2aef-d684-4a10-a571-b25981e7417e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2aab302a-4f92-43d2-b231-6f71e481303c",
    "visible": true
}