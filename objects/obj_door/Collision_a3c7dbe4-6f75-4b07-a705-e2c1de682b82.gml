/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 03713C6B
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
with(obj_player) {
	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6509A3A8
	/// @DnDParent : 03713C6B
	/// @DnDArgument : "var" "haskey"
	/// @DnDArgument : "value" "true"
	if(haskey == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 635E2B97
		/// @DnDParent : 6509A3A8
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "expr_relative" "1"
		/// @DnDArgument : "var" "haskey"
		haskey += false;
	
		/// @DnDAction : YoYo Games.Common.Apply_To
		/// @DnDVersion : 1
		/// @DnDHash : 7A2DF267
		/// @DnDApplyTo : a68c2aef-d684-4a10-a571-b25981e7417e
		/// @DnDParent : 6509A3A8
		with(obj_door) {
			/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
			/// @DnDVersion : 1
			/// @DnDHash : 2A6E64F2
			/// @DnDParent : 7A2DF267
			/// @DnDArgument : "xpos_relative" "1"
			/// @DnDArgument : "ypos_relative" "1"
			position_destroy(x + 0, y + 0);
		}
	}
}