{
    "id": "bd650bbc-d75c-4851-bb39-ca023a96434a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mouse",
    "eventList": [
        {
            "id": "1f976fe7-b59b-4a48-b760-516af19783c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd650bbc-d75c-4851-bb39-ca023a96434a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "67821102-3be3-42e1-9904-d4c0bb0b2989",
    "visible": true
}