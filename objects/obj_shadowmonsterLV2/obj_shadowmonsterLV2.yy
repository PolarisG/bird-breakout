{
    "id": "1aff1c99-4fac-4377-8b40-0e5f34814b65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadowmonsterLV2",
    "eventList": [
        {
            "id": "b4967fd2-2c58-4bd1-a5ff-30f00c5c92cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8d33e9c5-4529-45cb-91b9-0b206f882847",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1aff1c99-4fac-4377-8b40-0e5f34814b65"
        },
        {
            "id": "8257b99e-798e-4127-ac49-ed836c2456d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e61b491d-555a-4cc3-9c63-0bfb0cf5b0b6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1aff1c99-4fac-4377-8b40-0e5f34814b65"
        },
        {
            "id": "dc0cadbe-1d82-4a24-967a-4a1779ba5ea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1aff1c99-4fac-4377-8b40-0e5f34814b65"
        },
        {
            "id": "4a20de5c-dbf2-4064-9feb-31d9594341d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1aff1c99-4fac-4377-8b40-0e5f34814b65"
        },
        {
            "id": "27b0f642-1d81-4544-b9f9-d35471ddef53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1aff1c99-4fac-4377-8b40-0e5f34814b65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "65a79c81-244a-4112-8c01-75878ab8d9c0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "facing",
            "varType": 0
        },
        {
            "id": "85f176d9-b8ba-4169-b3f5-f6a0898ecf6b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "spd",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "894350fa-c44b-4595-98de-e375abfd6e52",
    "visible": true
}