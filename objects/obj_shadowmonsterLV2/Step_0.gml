/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 628D5D65
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_patrolpoint"
/// @DnDSaveInfo : "object" "456b4ff6-f5a5-4881-9119-4c1257a694bb"
var l628D5D65_0 = instance_place(x + 0, y + 0, obj_patrolpoint);
if ((l628D5D65_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4BE4BDB0
	/// @DnDParent : 628D5D65
	/// @DnDArgument : "expr" "-facing"
	/// @DnDArgument : "var" "facing"
	facing = -facing;
}

/// @DnDAction : YoYo Games.Collisions.If_Object_At
/// @DnDVersion : 1.1
/// @DnDHash : 511D9D29
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "object" "obj_solid"
/// @DnDSaveInfo : "object" "cd2ddf97-b455-445f-9a98-315d6a98aa55"
var l511D9D29_0 = instance_place(x + 0, y + 0, obj_solid);
if ((l511D9D29_0 > 0))
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1F91AC76
	/// @DnDParent : 511D9D29
	/// @DnDArgument : "expr" "-facing"
	/// @DnDArgument : "var" "facing"
	facing = -facing;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5CBFE212
/// @DnDArgument : "var" "facing"
/// @DnDArgument : "value" "-1"
if(facing == -1)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 60114B28
	/// @DnDParent : 5CBFE212
	/// @DnDArgument : "expr" "-spd"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "x"
	x += -spd;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 27467A56
else
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4E1D3345
	/// @DnDParent : 27467A56
	/// @DnDArgument : "expr" "spd"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "x"
	x += spd;
}

/// @DnDAction : YoYo Games.Instance Variables.If_Health
/// @DnDVersion : 1
/// @DnDHash : 4297F0BE
/// @DnDArgument : "op" "3"
if(!variable_instance_exists(id, "__dnd_health")) __dnd_health = 0;
if(__dnd_health <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7AB31805
	/// @DnDParent : 4297F0BE
	instance_destroy();
}