{
    "id": "8d33e9c5-4529-45cb-91b9-0b206f882847",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laserleft",
    "eventList": [
        {
            "id": "70eac9e3-b369-47cf-ac0d-7196ad0eb7ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "f1a24dbf-0dd9-4aa3-aba4-6bd919fbdfc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cb25211f-46da-4813-a595-dee799f30ed6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "5c8516ff-a6f1-4d64-8f80-1f0106100efc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd2ddf97-b455-445f-9a98-315d6a98aa55",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "16c3bb25-f202-450c-a64a-83f6a81a1e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a3b19797-d99e-49e8-b8f7-d6f55aa1f741",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "8708f42a-6d4d-4e00-b20f-7943377a86c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "72d7e06f-1c7e-4bb0-8fae-fc317c069f8b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "2dd5ac2c-4082-4117-9230-bd3416f938ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a50322a5-7105-4d68-b807-8002db2ece13",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "cf026d81-c979-4066-ac20-4a0c34452cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1499d865-72a6-46b9-864f-53a5648d5525",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "69a61608-8db4-407f-9ffe-2ba17f3436e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        },
        {
            "id": "28496717-b7a5-4f29-83b4-b6dc77618a55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f3803e12-e4b9-4584-be67-7c7b855c3ae0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d33e9c5-4529-45cb-91b9-0b206f882847"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "e7586574-906c-4029-a206-a2de5636bde7",
    "visible": true
}