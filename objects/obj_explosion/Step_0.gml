/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 7DB713C6
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "explosioncountdown"
explosioncountdown += -1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 575470CB
/// @DnDArgument : "var" "explosioncountdown"
/// @DnDArgument : "op" "3"
if(explosioncountdown <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 355DE09D
	/// @DnDParent : 575470CB
	instance_destroy();
}