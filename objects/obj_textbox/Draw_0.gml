/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 447A46D1
draw_self();

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 1A8DB10A
/// @DnDArgument : "x" "-375"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "-75"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" "speaker"
/// @DnDArgument : "var" "textstring"
draw_text(x + -375, y + -75, string(speaker) + string(textstring));