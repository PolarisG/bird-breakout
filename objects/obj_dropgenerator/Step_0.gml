/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 25730FC7
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "dropcountdown"
dropcountdown += -1;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 45D7EA29
/// @DnDArgument : "var" "dropcountdown"
/// @DnDArgument : "op" "3"
if(dropcountdown <= 0)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 4AE09B1D
	/// @DnDParent : 45D7EA29
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_antidrop"
	/// @DnDSaveInfo : "objectid" "ac75e150-f465-4bfb-9d27-d70475bec6d0"
	instance_create_layer(x + 0, y + 0, "Instances", obj_antidrop);

	/// @DnDAction : YoYo Games.Random.Get_Random_Number
	/// @DnDVersion : 1
	/// @DnDHash : 2EE9CF49
	/// @DnDParent : 45D7EA29
	/// @DnDArgument : "var" "dropcountdown"
	/// @DnDArgument : "type" "1"
	/// @DnDArgument : "min" "120"
	/// @DnDArgument : "max" "240"
	dropcountdown = floor(random_range(120, 240 + 1));
}