{
    "id": "2d590e08-d96b-4e63-ad50-f751b573da24",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dropgenerator",
    "eventList": [
        {
            "id": "f664ee1a-de3c-41bb-a157-474ffc394ec4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d590e08-d96b-4e63-ad50-f751b573da24"
        },
        {
            "id": "11aa3025-278d-4980-9367-1f9206ae25fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d590e08-d96b-4e63-ad50-f751b573da24"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "22b1487d-3327-41d9-9499-fc5b0283162c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "dropcountdown",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "eac3dc59-0274-462b-8635-c3526609a0a1",
    "visible": false
}