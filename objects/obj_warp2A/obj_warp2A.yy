{
    "id": "a62f6be9-f3d2-4ef2-85ea-15927bed889b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp2A",
    "eventList": [
        {
            "id": "7714bc99-069d-40d1-8d2e-fe2aad5016d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a62f6be9-f3d2-4ef2-85ea-15927bed889b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f04e3a2c-6df2-4843-9d23-3aa1fb767159",
    "visible": true
}