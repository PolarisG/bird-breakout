{
    "id": "69319f7d-9bcf-4404-b4cb-617e3d2fddf1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_continue",
    "eventList": [
        {
            "id": "183f918f-5c40-4640-b0f9-77558781ad12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "69319f7d-9bcf-4404-b4cb-617e3d2fddf1"
        },
        {
            "id": "5591353d-7f9d-4279-ae93-fd6ee5accd0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "69319f7d-9bcf-4404-b4cb-617e3d2fddf1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38efe206-356a-4043-a174-fbf0f90922d5",
    "visible": true
}