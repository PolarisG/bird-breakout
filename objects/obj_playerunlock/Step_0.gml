/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 4598BC9B
/// @DnDApplyTo : 793b8f5a-3f85-44df-ae17-b67cc569a275
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "controllock"
with(obj_clonebird) {
controllock = false;

}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 59586AFC
/// @DnDApplyTo : d64475a8-3db2-43b7-9702-39be88c9e600
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "controllock"
with(obj_player) {
controllock = false;

}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 13B811CD
instance_destroy();