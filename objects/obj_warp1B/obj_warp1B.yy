{
    "id": "7afc3bb9-b9e4-4cf3-a2d1-fbcdaf9a1eac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_warp1B",
    "eventList": [
        {
            "id": "f96c66ee-f780-4717-9b75-111284f572b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7afc3bb9-b9e4-4cf3-a2d1-fbcdaf9a1eac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "7c24b0ed-afa2-46c1-ab73-8217f670d3c2",
    "visible": true
}